<?php

namespace IiMedias\ProjectBundle\Model;

use IiMedias\ProjectBundle\Model\Base\ProjectIssue as BaseProjectIssue;

/**
 * Skeleton subclass for representing a row from the 'project_issue_proiss' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ProjectIssue extends BaseProjectIssue
{
    protected $newComment;

    public function getNewComment()
    {
        return $this->newComment;
    }

    public function setNewComment($newComment)
    {
        $this->newComment = $newComment;
        return $this;
    }

    public function __toString()
    {
        return '#' . $this->getId() . ': ' . $this->getSubject();
    }

    public function calculateParentAndChildrenCompletedPercentage()
    {
        $this
            ->calculateChildrenCompletedPercentage($this)
            ->calculateParentCompletedPercentage($this)
        ;
        return $this;
    }

    protected function calculateParentCompletedPercentage($currentIssue)
    {
        if (!is_null($currentIssue->getParentId())) {
            $this
                ->calculateChildrenCompletedPercentage($currentIssue->getProjectIssueParent())
                ->calculateParentCompletedPercentage($currentIssue->getProjectIssueParent())
            ;
        }
        return $this;
    }

    protected function calculateChildrenCompletedPercentage($currentIssue)
    {
        $childrenIssues = ProjectIssueQuery::create()
            ->filterByProjectIssueParent($currentIssue)
            ->find()
        ;
        if ($childrenIssues->count() > 0) {
            $totalPercent = 0;
            foreach ($childrenIssues as $childIssue) {
                $totalPercent += $childIssue->getChildrenCompletedPercentage();
            }
            $totalPercent = ceil($totalPercent / $childrenIssues->count());
            $currentIssue
                ->setChildrenCompletedPercentage($totalPercent)
                ->save()
            ;
        }
        else {
            $currentIssue
                ->setChildrenCompletedPercentage($this->getCompletedPercentage())
                ->save()
            ;
        }

        return $this;
    }
}

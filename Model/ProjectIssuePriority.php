<?php

namespace IiMedias\ProjectBundle\Model;

use IiMedias\ProjectBundle\Model\Base\ProjectIssuePriority as BaseProjectIssuePriority;

/**
 * Skeleton subclass for representing a row from the 'project_issue_priority_prispr' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ProjectIssuePriority extends BaseProjectIssuePriority
{
    public function  __toString()
    {
        return $this->getName();
    }
}

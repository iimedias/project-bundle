<?php

namespace IiMedias\ProjectBundle\Model;

use IiMedias\ProjectBundle\Model\Base\Project as BaseProject;

/**
 * Skeleton subclass for representing a row from the 'project_project_prproj' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Project extends BaseProject
{

}

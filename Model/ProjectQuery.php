<?php

namespace IiMedias\ProjectBundle\Model;

use IiMedias\ProjectBundle\Model\Base\ProjectQuery as BaseProjectQuery;
use Propel\Runtime\ActiveQuery\Criteria;

/**
 * Skeleton subclass for performing query and update operations on the 'project_project_prproj' table.
 *
 * @package IiMedias\ProjectBundle\Model
 * @author Sébastien "sebii" Bloino <sebii@sebiiheckel.fr>
 * @version 1.0.0
 */
class ProjectQuery extends BaseProjectQuery {
    /**
     * Récupération de la liste des projets pour un utilisateur
     *
     * @access public
     * @since 1.0.0 20/07/2016 Création -- sebii
     * @param mixed $user
     * @return Propel\Runtime\Collection\ObjectCollection
     */
    public static function getAllForUser($user)
    {
        $projects = self::create()
            //->filterByIsPublic(true)
            ->orderById(Criteria::ASC)
            ->find();
        return $projects;
    }

    /**
     * Récupération d'un projet via son identifiant slug
     *
     * @access public
     * @since 1.0.0 26/07/2016 Création -- sebii
     * @param string $slug
     * @return mixed
     */
    public function getOneBySlug($slug)
    {
        $project = self::create()
            ->filterBySlug($slug)
            ->findOne();
        return $project;
    }
}

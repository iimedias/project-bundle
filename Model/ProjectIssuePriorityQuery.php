<?php

namespace IiMedias\ProjectBundle\Model;

use IiMedias\ProjectBundle\Model\Base\ProjectIssuePriorityQuery as BaseProjectIssuePriorityQuery;
use Propel\Runtime\ActiveQuery\Criteria;

/**
 * Skeleton subclass for performing query and update operations on the 'project_issue_priority_prispr' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ProjectIssuePriorityQuery extends BaseProjectIssuePriorityQuery
{
    public static function getCriteriaForProjectIssueTypeForm()
    {
        $priorities = self::create()
            ->orderByOrder(Criteria::ASC)
        ;

        return $priorities;
    }
}

<?php

namespace IiMedias\ProjectBundle\Model;

use IiMedias\ProjectBundle\Model\Base\ProjectIssueTypeQuery as BaseProjectIssueTypeQuery;
use Propel\Runtime\ActiveQuery\Criteria;

/**
 * Skeleton subclass for performing query and update operations on the 'project_issue_type_pristy' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ProjectIssueTypeQuery extends BaseProjectIssueTypeQuery
{
    public static function getCriteriaForProjectIssueTypeForm()
    {
        $types = self::create()
            ->orderByOrder(Criteria::ASC)
        ;

        return $types;
    }
}

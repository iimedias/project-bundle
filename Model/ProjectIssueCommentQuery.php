<?php

namespace IiMedias\ProjectBundle\Model;

use IiMedias\ProjectBundle\Model\Base\ProjectIssueCommentQuery as BaseProjectIssueCommentQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'project_issue_comment_prisco' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ProjectIssueCommentQuery extends BaseProjectIssueCommentQuery
{

}

<?php

namespace IiMedias\ProjectBundle\Model;

use IiMedias\ProjectBundle\Model\Base\ProjectIssueType as BaseProjectIssueType;

/**
 * Skeleton subclass for representing a row from the 'project_issue_type_pristy' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ProjectIssueType extends BaseProjectIssueType
{
    public function  __toString()
    {
        return $this->getName();
    }
}

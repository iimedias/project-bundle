<?php

namespace IiMedias\ProjectBundle\Model\Map;

use IiMedias\ProjectBundle\Model\ProjectIssueStatus;
use IiMedias\ProjectBundle\Model\ProjectIssueStatusQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'project_issue_status_prisst' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ProjectIssueStatusTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.ProjectBundle.Model.Map.ProjectIssueStatusTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'project_issue_status_prisst';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\ProjectBundle\\Model\\ProjectIssueStatus';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.ProjectBundle.Model.ProjectIssueStatus';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 7;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 7;

    /**
     * the column name for the prisst_id field
     */
    const COL_PRISST_ID = 'project_issue_status_prisst.prisst_id';

    /**
     * the column name for the prisst_name field
     */
    const COL_PRISST_NAME = 'project_issue_status_prisst.prisst_name';

    /**
     * the column name for the prisst_order field
     */
    const COL_PRISST_ORDER = 'project_issue_status_prisst.prisst_order';

    /**
     * the column name for the prisst_created_by_user_id field
     */
    const COL_PRISST_CREATED_BY_USER_ID = 'project_issue_status_prisst.prisst_created_by_user_id';

    /**
     * the column name for the prisst_updated_by_user_id field
     */
    const COL_PRISST_UPDATED_BY_USER_ID = 'project_issue_status_prisst.prisst_updated_by_user_id';

    /**
     * the column name for the prisst_created_at field
     */
    const COL_PRISST_CREATED_AT = 'project_issue_status_prisst.prisst_created_at';

    /**
     * the column name for the prisst_updated_at field
     */
    const COL_PRISST_UPDATED_AT = 'project_issue_status_prisst.prisst_updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Name', 'Order', 'CreatedByUserId', 'UpdatedByUserId', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'name', 'order', 'createdByUserId', 'updatedByUserId', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(ProjectIssueStatusTableMap::COL_PRISST_ID, ProjectIssueStatusTableMap::COL_PRISST_NAME, ProjectIssueStatusTableMap::COL_PRISST_ORDER, ProjectIssueStatusTableMap::COL_PRISST_CREATED_BY_USER_ID, ProjectIssueStatusTableMap::COL_PRISST_UPDATED_BY_USER_ID, ProjectIssueStatusTableMap::COL_PRISST_CREATED_AT, ProjectIssueStatusTableMap::COL_PRISST_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('prisst_id', 'prisst_name', 'prisst_order', 'prisst_created_by_user_id', 'prisst_updated_by_user_id', 'prisst_created_at', 'prisst_updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Name' => 1, 'Order' => 2, 'CreatedByUserId' => 3, 'UpdatedByUserId' => 4, 'CreatedAt' => 5, 'UpdatedAt' => 6, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'name' => 1, 'order' => 2, 'createdByUserId' => 3, 'updatedByUserId' => 4, 'createdAt' => 5, 'updatedAt' => 6, ),
        self::TYPE_COLNAME       => array(ProjectIssueStatusTableMap::COL_PRISST_ID => 0, ProjectIssueStatusTableMap::COL_PRISST_NAME => 1, ProjectIssueStatusTableMap::COL_PRISST_ORDER => 2, ProjectIssueStatusTableMap::COL_PRISST_CREATED_BY_USER_ID => 3, ProjectIssueStatusTableMap::COL_PRISST_UPDATED_BY_USER_ID => 4, ProjectIssueStatusTableMap::COL_PRISST_CREATED_AT => 5, ProjectIssueStatusTableMap::COL_PRISST_UPDATED_AT => 6, ),
        self::TYPE_FIELDNAME     => array('prisst_id' => 0, 'prisst_name' => 1, 'prisst_order' => 2, 'prisst_created_by_user_id' => 3, 'prisst_updated_by_user_id' => 4, 'prisst_created_at' => 5, 'prisst_updated_at' => 6, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('project_issue_status_prisst');
        $this->setPhpName('ProjectIssueStatus');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\ProjectBundle\\Model\\ProjectIssueStatus');
        $this->setPackage('src.IiMedias.ProjectBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('prisst_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('prisst_name', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('prisst_order', 'Order', 'INTEGER', true, null, null);
        $this->addForeignKey('prisst_created_by_user_id', 'CreatedByUserId', 'INTEGER', 'user_user_usrusr', 'usrusr_id', false, null, null);
        $this->addForeignKey('prisst_updated_by_user_id', 'UpdatedByUserId', 'INTEGER', 'user_user_usrusr', 'usrusr_id', false, null, null);
        $this->addColumn('prisst_created_at', 'CreatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('prisst_updated_at', 'UpdatedAt', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CreatedByUser', '\\IiMedias\\AdminBundle\\Model\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':prisst_created_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('UpdatedByUser', '\\IiMedias\\AdminBundle\\Model\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':prisst_updated_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('ProjectIssue', '\\IiMedias\\ProjectBundle\\Model\\ProjectIssue', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':prissu_prisst_id',
    1 => ':prisst_id',
  ),
), 'CASCADE', 'CASCADE', 'ProjectIssues', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'prisst_created_at', 'update_column' => 'prisst_updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to project_issue_status_prisst     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        ProjectIssueTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ProjectIssueStatusTableMap::CLASS_DEFAULT : ProjectIssueStatusTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ProjectIssueStatus object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ProjectIssueStatusTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ProjectIssueStatusTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ProjectIssueStatusTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ProjectIssueStatusTableMap::OM_CLASS;
            /** @var ProjectIssueStatus $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ProjectIssueStatusTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ProjectIssueStatusTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ProjectIssueStatusTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ProjectIssueStatus $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ProjectIssueStatusTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ProjectIssueStatusTableMap::COL_PRISST_ID);
            $criteria->addSelectColumn(ProjectIssueStatusTableMap::COL_PRISST_NAME);
            $criteria->addSelectColumn(ProjectIssueStatusTableMap::COL_PRISST_ORDER);
            $criteria->addSelectColumn(ProjectIssueStatusTableMap::COL_PRISST_CREATED_BY_USER_ID);
            $criteria->addSelectColumn(ProjectIssueStatusTableMap::COL_PRISST_UPDATED_BY_USER_ID);
            $criteria->addSelectColumn(ProjectIssueStatusTableMap::COL_PRISST_CREATED_AT);
            $criteria->addSelectColumn(ProjectIssueStatusTableMap::COL_PRISST_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.prisst_id');
            $criteria->addSelectColumn($alias . '.prisst_name');
            $criteria->addSelectColumn($alias . '.prisst_order');
            $criteria->addSelectColumn($alias . '.prisst_created_by_user_id');
            $criteria->addSelectColumn($alias . '.prisst_updated_by_user_id');
            $criteria->addSelectColumn($alias . '.prisst_created_at');
            $criteria->addSelectColumn($alias . '.prisst_updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ProjectIssueStatusTableMap::DATABASE_NAME)->getTable(ProjectIssueStatusTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ProjectIssueStatusTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ProjectIssueStatusTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ProjectIssueStatusTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ProjectIssueStatus or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ProjectIssueStatus object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProjectIssueStatusTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\ProjectBundle\Model\ProjectIssueStatus) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ProjectIssueStatusTableMap::DATABASE_NAME);
            $criteria->add(ProjectIssueStatusTableMap::COL_PRISST_ID, (array) $values, Criteria::IN);
        }

        $query = ProjectIssueStatusQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ProjectIssueStatusTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ProjectIssueStatusTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the project_issue_status_prisst table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ProjectIssueStatusQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ProjectIssueStatus or Criteria object.
     *
     * @param mixed               $criteria Criteria or ProjectIssueStatus object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProjectIssueStatusTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ProjectIssueStatus object
        }

        if ($criteria->containsKey(ProjectIssueStatusTableMap::COL_PRISST_ID) && $criteria->keyContainsValue(ProjectIssueStatusTableMap::COL_PRISST_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ProjectIssueStatusTableMap::COL_PRISST_ID.')');
        }


        // Set the correct dbName
        $query = ProjectIssueStatusQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ProjectIssueStatusTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ProjectIssueStatusTableMap::buildTableMap();

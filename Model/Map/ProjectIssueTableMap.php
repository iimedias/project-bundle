<?php

namespace IiMedias\ProjectBundle\Model\Map;

use IiMedias\ProjectBundle\Model\ProjectIssue;
use IiMedias\ProjectBundle\Model\ProjectIssueQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'project_issue_prissu' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ProjectIssueTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.ProjectBundle.Model.Map.ProjectIssueTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'project_issue_prissu';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\ProjectBundle\\Model\\ProjectIssue';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.ProjectBundle.Model.ProjectIssue';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 18;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 18;

    /**
     * the column name for the prissu_id field
     */
    const COL_PRISSU_ID = 'project_issue_prissu.prissu_id';

    /**
     * the column name for the prissu_prproj_id field
     */
    const COL_PRISSU_PRPROJ_ID = 'project_issue_prissu.prissu_prproj_id';

    /**
     * the column name for the prissu_parent_id field
     */
    const COL_PRISSU_PARENT_ID = 'project_issue_prissu.prissu_parent_id';

    /**
     * the column name for the prissu_pristy_id field
     */
    const COL_PRISSU_PRISTY_ID = 'project_issue_prissu.prissu_pristy_id';

    /**
     * the column name for the prissu_subject field
     */
    const COL_PRISSU_SUBJECT = 'project_issue_prissu.prissu_subject';

    /**
     * the column name for the prissu_description field
     */
    const COL_PRISSU_DESCRIPTION = 'project_issue_prissu.prissu_description';

    /**
     * the column name for the prissu_prisst_id field
     */
    const COL_PRISSU_PRISST_ID = 'project_issue_prissu.prissu_prisst_id';

    /**
     * the column name for the prissu_prispr_id field
     */
    const COL_PRISSU_PRISPR_ID = 'project_issue_prissu.prissu_prispr_id';

    /**
     * the column name for the prissu_assigned_user_id field
     */
    const COL_PRISSU_ASSIGNED_USER_ID = 'project_issue_prissu.prissu_assigned_user_id';

    /**
     * the column name for the prissu_start_at field
     */
    const COL_PRISSU_START_AT = 'project_issue_prissu.prissu_start_at';

    /**
     * the column name for the prissu_end_at field
     */
    const COL_PRISSU_END_AT = 'project_issue_prissu.prissu_end_at';

    /**
     * the column name for the prissu_estimated_time field
     */
    const COL_PRISSU_ESTIMATED_TIME = 'project_issue_prissu.prissu_estimated_time';

    /**
     * the column name for the prissu_completed_percentage field
     */
    const COL_PRISSU_COMPLETED_PERCENTAGE = 'project_issue_prissu.prissu_completed_percentage';

    /**
     * the column name for the prissu_children_completed_percentage field
     */
    const COL_PRISSU_CHILDREN_COMPLETED_PERCENTAGE = 'project_issue_prissu.prissu_children_completed_percentage';

    /**
     * the column name for the prissu_created_by_user_id field
     */
    const COL_PRISSU_CREATED_BY_USER_ID = 'project_issue_prissu.prissu_created_by_user_id';

    /**
     * the column name for the prissu_updated_by_user_id field
     */
    const COL_PRISSU_UPDATED_BY_USER_ID = 'project_issue_prissu.prissu_updated_by_user_id';

    /**
     * the column name for the prissu_created_at field
     */
    const COL_PRISSU_CREATED_AT = 'project_issue_prissu.prissu_created_at';

    /**
     * the column name for the prissu_updated_at field
     */
    const COL_PRISSU_UPDATED_AT = 'project_issue_prissu.prissu_updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'ProjectId', 'ParentId', 'TypeId', 'Subject', 'Description', 'StatusId', 'PriorityId', 'AssignedUserId', 'StartAt', 'EndAt', 'EstimatedTime', 'CompletedPercentage', 'ChildrenCompletedPercentage', 'CreatedByUserId', 'UpdatedByUserId', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'projectId', 'parentId', 'typeId', 'subject', 'description', 'statusId', 'priorityId', 'assignedUserId', 'startAt', 'endAt', 'estimatedTime', 'completedPercentage', 'childrenCompletedPercentage', 'createdByUserId', 'updatedByUserId', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(ProjectIssueTableMap::COL_PRISSU_ID, ProjectIssueTableMap::COL_PRISSU_PRPROJ_ID, ProjectIssueTableMap::COL_PRISSU_PARENT_ID, ProjectIssueTableMap::COL_PRISSU_PRISTY_ID, ProjectIssueTableMap::COL_PRISSU_SUBJECT, ProjectIssueTableMap::COL_PRISSU_DESCRIPTION, ProjectIssueTableMap::COL_PRISSU_PRISST_ID, ProjectIssueTableMap::COL_PRISSU_PRISPR_ID, ProjectIssueTableMap::COL_PRISSU_ASSIGNED_USER_ID, ProjectIssueTableMap::COL_PRISSU_START_AT, ProjectIssueTableMap::COL_PRISSU_END_AT, ProjectIssueTableMap::COL_PRISSU_ESTIMATED_TIME, ProjectIssueTableMap::COL_PRISSU_COMPLETED_PERCENTAGE, ProjectIssueTableMap::COL_PRISSU_CHILDREN_COMPLETED_PERCENTAGE, ProjectIssueTableMap::COL_PRISSU_CREATED_BY_USER_ID, ProjectIssueTableMap::COL_PRISSU_UPDATED_BY_USER_ID, ProjectIssueTableMap::COL_PRISSU_CREATED_AT, ProjectIssueTableMap::COL_PRISSU_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('prissu_id', 'prissu_prproj_id', 'prissu_parent_id', 'prissu_pristy_id', 'prissu_subject', 'prissu_description', 'prissu_prisst_id', 'prissu_prispr_id', 'prissu_assigned_user_id', 'prissu_start_at', 'prissu_end_at', 'prissu_estimated_time', 'prissu_completed_percentage', 'prissu_children_completed_percentage', 'prissu_created_by_user_id', 'prissu_updated_by_user_id', 'prissu_created_at', 'prissu_updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'ProjectId' => 1, 'ParentId' => 2, 'TypeId' => 3, 'Subject' => 4, 'Description' => 5, 'StatusId' => 6, 'PriorityId' => 7, 'AssignedUserId' => 8, 'StartAt' => 9, 'EndAt' => 10, 'EstimatedTime' => 11, 'CompletedPercentage' => 12, 'ChildrenCompletedPercentage' => 13, 'CreatedByUserId' => 14, 'UpdatedByUserId' => 15, 'CreatedAt' => 16, 'UpdatedAt' => 17, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'projectId' => 1, 'parentId' => 2, 'typeId' => 3, 'subject' => 4, 'description' => 5, 'statusId' => 6, 'priorityId' => 7, 'assignedUserId' => 8, 'startAt' => 9, 'endAt' => 10, 'estimatedTime' => 11, 'completedPercentage' => 12, 'childrenCompletedPercentage' => 13, 'createdByUserId' => 14, 'updatedByUserId' => 15, 'createdAt' => 16, 'updatedAt' => 17, ),
        self::TYPE_COLNAME       => array(ProjectIssueTableMap::COL_PRISSU_ID => 0, ProjectIssueTableMap::COL_PRISSU_PRPROJ_ID => 1, ProjectIssueTableMap::COL_PRISSU_PARENT_ID => 2, ProjectIssueTableMap::COL_PRISSU_PRISTY_ID => 3, ProjectIssueTableMap::COL_PRISSU_SUBJECT => 4, ProjectIssueTableMap::COL_PRISSU_DESCRIPTION => 5, ProjectIssueTableMap::COL_PRISSU_PRISST_ID => 6, ProjectIssueTableMap::COL_PRISSU_PRISPR_ID => 7, ProjectIssueTableMap::COL_PRISSU_ASSIGNED_USER_ID => 8, ProjectIssueTableMap::COL_PRISSU_START_AT => 9, ProjectIssueTableMap::COL_PRISSU_END_AT => 10, ProjectIssueTableMap::COL_PRISSU_ESTIMATED_TIME => 11, ProjectIssueTableMap::COL_PRISSU_COMPLETED_PERCENTAGE => 12, ProjectIssueTableMap::COL_PRISSU_CHILDREN_COMPLETED_PERCENTAGE => 13, ProjectIssueTableMap::COL_PRISSU_CREATED_BY_USER_ID => 14, ProjectIssueTableMap::COL_PRISSU_UPDATED_BY_USER_ID => 15, ProjectIssueTableMap::COL_PRISSU_CREATED_AT => 16, ProjectIssueTableMap::COL_PRISSU_UPDATED_AT => 17, ),
        self::TYPE_FIELDNAME     => array('prissu_id' => 0, 'prissu_prproj_id' => 1, 'prissu_parent_id' => 2, 'prissu_pristy_id' => 3, 'prissu_subject' => 4, 'prissu_description' => 5, 'prissu_prisst_id' => 6, 'prissu_prispr_id' => 7, 'prissu_assigned_user_id' => 8, 'prissu_start_at' => 9, 'prissu_end_at' => 10, 'prissu_estimated_time' => 11, 'prissu_completed_percentage' => 12, 'prissu_children_completed_percentage' => 13, 'prissu_created_by_user_id' => 14, 'prissu_updated_by_user_id' => 15, 'prissu_created_at' => 16, 'prissu_updated_at' => 17, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('project_issue_prissu');
        $this->setPhpName('ProjectIssue');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\ProjectBundle\\Model\\ProjectIssue');
        $this->setPackage('src.IiMedias.ProjectBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('prissu_id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('prissu_prproj_id', 'ProjectId', 'INTEGER', 'project_project_prproj', 'prproj_id', true, null, null);
        $this->addForeignKey('prissu_parent_id', 'ParentId', 'INTEGER', 'project_issue_prissu', 'prissu_id', false, null, null);
        $this->addForeignKey('prissu_pristy_id', 'TypeId', 'INTEGER', 'project_issue_type_pristy', 'pristy_id', true, null, null);
        $this->addColumn('prissu_subject', 'Subject', 'VARCHAR', true, 255, null);
        $this->addColumn('prissu_description', 'Description', 'LONGVARCHAR', false, null, null);
        $this->addForeignKey('prissu_prisst_id', 'StatusId', 'INTEGER', 'project_issue_status_prisst', 'prisst_id', true, null, null);
        $this->addForeignKey('prissu_prispr_id', 'PriorityId', 'INTEGER', 'project_issue_priority_prispr', 'prispr_id', true, null, null);
        $this->addForeignKey('prissu_assigned_user_id', 'AssignedUserId', 'INTEGER', 'user_user_usrusr', 'usrusr_id', false, null, null);
        $this->addColumn('prissu_start_at', 'StartAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('prissu_end_at', 'EndAt', 'TIMESTAMP', false, null, null);
        $this->addColumn('prissu_estimated_time', 'EstimatedTime', 'FLOAT', false, null, null);
        $this->addColumn('prissu_completed_percentage', 'CompletedPercentage', 'FLOAT', false, null, null);
        $this->addColumn('prissu_children_completed_percentage', 'ChildrenCompletedPercentage', 'FLOAT', false, null, null);
        $this->addForeignKey('prissu_created_by_user_id', 'CreatedByUserId', 'INTEGER', 'user_user_usrusr', 'usrusr_id', false, null, null);
        $this->addForeignKey('prissu_updated_by_user_id', 'UpdatedByUserId', 'INTEGER', 'user_user_usrusr', 'usrusr_id', false, null, null);
        $this->addColumn('prissu_created_at', 'CreatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('prissu_updated_at', 'UpdatedAt', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('Project', '\\IiMedias\\ProjectBundle\\Model\\Project', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':prissu_prproj_id',
    1 => ':prproj_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('ProjectIssueParent', '\\IiMedias\\ProjectBundle\\Model\\ProjectIssue', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':prissu_parent_id',
    1 => ':prissu_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('ProjectIssueType', '\\IiMedias\\ProjectBundle\\Model\\ProjectIssueType', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':prissu_pristy_id',
    1 => ':pristy_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('ProjectIssueStatus', '\\IiMedias\\ProjectBundle\\Model\\ProjectIssueStatus', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':prissu_prisst_id',
    1 => ':prisst_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('ProjectIssuePriority', '\\IiMedias\\ProjectBundle\\Model\\ProjectIssuePriority', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':prissu_prispr_id',
    1 => ':prispr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('AssignedUser', '\\IiMedias\\AdminBundle\\Model\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':prissu_assigned_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('CreatedByUser', '\\IiMedias\\AdminBundle\\Model\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':prissu_created_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('UpdatedByUser', '\\IiMedias\\AdminBundle\\Model\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':prissu_updated_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('ProjectIssueChild', '\\IiMedias\\ProjectBundle\\Model\\ProjectIssue', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':prissu_parent_id',
    1 => ':prissu_id',
  ),
), 'CASCADE', 'CASCADE', 'ProjectIssuechildren', false);
        $this->addRelation('ProjectIssueComment', '\\IiMedias\\ProjectBundle\\Model\\ProjectIssueComment', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':prisco_prissu_id',
    1 => ':prissu_id',
  ),
), 'CASCADE', 'CASCADE', 'ProjectIssueComments', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'prissu_created_at', 'update_column' => 'prissu_updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to project_issue_prissu     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        ProjectIssueTableMap::clearInstancePool();
        ProjectIssueCommentTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ProjectIssueTableMap::CLASS_DEFAULT : ProjectIssueTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ProjectIssue object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ProjectIssueTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ProjectIssueTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ProjectIssueTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ProjectIssueTableMap::OM_CLASS;
            /** @var ProjectIssue $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ProjectIssueTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ProjectIssueTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ProjectIssueTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ProjectIssue $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ProjectIssueTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ProjectIssueTableMap::COL_PRISSU_ID);
            $criteria->addSelectColumn(ProjectIssueTableMap::COL_PRISSU_PRPROJ_ID);
            $criteria->addSelectColumn(ProjectIssueTableMap::COL_PRISSU_PARENT_ID);
            $criteria->addSelectColumn(ProjectIssueTableMap::COL_PRISSU_PRISTY_ID);
            $criteria->addSelectColumn(ProjectIssueTableMap::COL_PRISSU_SUBJECT);
            $criteria->addSelectColumn(ProjectIssueTableMap::COL_PRISSU_DESCRIPTION);
            $criteria->addSelectColumn(ProjectIssueTableMap::COL_PRISSU_PRISST_ID);
            $criteria->addSelectColumn(ProjectIssueTableMap::COL_PRISSU_PRISPR_ID);
            $criteria->addSelectColumn(ProjectIssueTableMap::COL_PRISSU_ASSIGNED_USER_ID);
            $criteria->addSelectColumn(ProjectIssueTableMap::COL_PRISSU_START_AT);
            $criteria->addSelectColumn(ProjectIssueTableMap::COL_PRISSU_END_AT);
            $criteria->addSelectColumn(ProjectIssueTableMap::COL_PRISSU_ESTIMATED_TIME);
            $criteria->addSelectColumn(ProjectIssueTableMap::COL_PRISSU_COMPLETED_PERCENTAGE);
            $criteria->addSelectColumn(ProjectIssueTableMap::COL_PRISSU_CHILDREN_COMPLETED_PERCENTAGE);
            $criteria->addSelectColumn(ProjectIssueTableMap::COL_PRISSU_CREATED_BY_USER_ID);
            $criteria->addSelectColumn(ProjectIssueTableMap::COL_PRISSU_UPDATED_BY_USER_ID);
            $criteria->addSelectColumn(ProjectIssueTableMap::COL_PRISSU_CREATED_AT);
            $criteria->addSelectColumn(ProjectIssueTableMap::COL_PRISSU_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.prissu_id');
            $criteria->addSelectColumn($alias . '.prissu_prproj_id');
            $criteria->addSelectColumn($alias . '.prissu_parent_id');
            $criteria->addSelectColumn($alias . '.prissu_pristy_id');
            $criteria->addSelectColumn($alias . '.prissu_subject');
            $criteria->addSelectColumn($alias . '.prissu_description');
            $criteria->addSelectColumn($alias . '.prissu_prisst_id');
            $criteria->addSelectColumn($alias . '.prissu_prispr_id');
            $criteria->addSelectColumn($alias . '.prissu_assigned_user_id');
            $criteria->addSelectColumn($alias . '.prissu_start_at');
            $criteria->addSelectColumn($alias . '.prissu_end_at');
            $criteria->addSelectColumn($alias . '.prissu_estimated_time');
            $criteria->addSelectColumn($alias . '.prissu_completed_percentage');
            $criteria->addSelectColumn($alias . '.prissu_children_completed_percentage');
            $criteria->addSelectColumn($alias . '.prissu_created_by_user_id');
            $criteria->addSelectColumn($alias . '.prissu_updated_by_user_id');
            $criteria->addSelectColumn($alias . '.prissu_created_at');
            $criteria->addSelectColumn($alias . '.prissu_updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ProjectIssueTableMap::DATABASE_NAME)->getTable(ProjectIssueTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ProjectIssueTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ProjectIssueTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ProjectIssueTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ProjectIssue or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ProjectIssue object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProjectIssueTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\ProjectBundle\Model\ProjectIssue) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ProjectIssueTableMap::DATABASE_NAME);
            $criteria->add(ProjectIssueTableMap::COL_PRISSU_ID, (array) $values, Criteria::IN);
        }

        $query = ProjectIssueQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ProjectIssueTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ProjectIssueTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the project_issue_prissu table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ProjectIssueQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ProjectIssue or Criteria object.
     *
     * @param mixed               $criteria Criteria or ProjectIssue object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProjectIssueTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ProjectIssue object
        }

        if ($criteria->containsKey(ProjectIssueTableMap::COL_PRISSU_ID) && $criteria->keyContainsValue(ProjectIssueTableMap::COL_PRISSU_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ProjectIssueTableMap::COL_PRISSU_ID.')');
        }


        // Set the correct dbName
        $query = ProjectIssueQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ProjectIssueTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ProjectIssueTableMap::buildTableMap();

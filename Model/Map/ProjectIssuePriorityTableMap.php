<?php

namespace IiMedias\ProjectBundle\Model\Map;

use IiMedias\ProjectBundle\Model\ProjectIssuePriority;
use IiMedias\ProjectBundle\Model\ProjectIssuePriorityQuery;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\InstancePoolTrait;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\DataFetcher\DataFetcherInterface;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\RelationMap;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Map\TableMapTrait;


/**
 * This class defines the structure of the 'project_issue_priority_prispr' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 */
class ProjectIssuePriorityTableMap extends TableMap
{
    use InstancePoolTrait;
    use TableMapTrait;

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'src.IiMedias.ProjectBundle.Model.Map.ProjectIssuePriorityTableMap';

    /**
     * The default database name for this class
     */
    const DATABASE_NAME = 'default';

    /**
     * The table name for this class
     */
    const TABLE_NAME = 'project_issue_priority_prispr';

    /**
     * The related Propel class for this table
     */
    const OM_CLASS = '\\IiMedias\\ProjectBundle\\Model\\ProjectIssuePriority';

    /**
     * A class that can be returned by this tableMap
     */
    const CLASS_DEFAULT = 'src.IiMedias.ProjectBundle.Model.ProjectIssuePriority';

    /**
     * The total number of columns
     */
    const NUM_COLUMNS = 7;

    /**
     * The number of lazy-loaded columns
     */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /**
     * The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS)
     */
    const NUM_HYDRATE_COLUMNS = 7;

    /**
     * the column name for the prispr_id field
     */
    const COL_PRISPR_ID = 'project_issue_priority_prispr.prispr_id';

    /**
     * the column name for the prispr_name field
     */
    const COL_PRISPR_NAME = 'project_issue_priority_prispr.prispr_name';

    /**
     * the column name for the prispr_order field
     */
    const COL_PRISPR_ORDER = 'project_issue_priority_prispr.prispr_order';

    /**
     * the column name for the prispr_created_by_user_id field
     */
    const COL_PRISPR_CREATED_BY_USER_ID = 'project_issue_priority_prispr.prispr_created_by_user_id';

    /**
     * the column name for the prispr_updated_by_user_id field
     */
    const COL_PRISPR_UPDATED_BY_USER_ID = 'project_issue_priority_prispr.prispr_updated_by_user_id';

    /**
     * the column name for the prispr_created_at field
     */
    const COL_PRISPR_CREATED_AT = 'project_issue_priority_prispr.prispr_created_at';

    /**
     * the column name for the prispr_updated_at field
     */
    const COL_PRISPR_UPDATED_AT = 'project_issue_priority_prispr.prispr_updated_at';

    /**
     * The default string format for model objects of the related table
     */
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldNames[self::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        self::TYPE_PHPNAME       => array('Id', 'Name', 'Order', 'CreatedByUserId', 'UpdatedByUserId', 'CreatedAt', 'UpdatedAt', ),
        self::TYPE_CAMELNAME     => array('id', 'name', 'order', 'createdByUserId', 'updatedByUserId', 'createdAt', 'updatedAt', ),
        self::TYPE_COLNAME       => array(ProjectIssuePriorityTableMap::COL_PRISPR_ID, ProjectIssuePriorityTableMap::COL_PRISPR_NAME, ProjectIssuePriorityTableMap::COL_PRISPR_ORDER, ProjectIssuePriorityTableMap::COL_PRISPR_CREATED_BY_USER_ID, ProjectIssuePriorityTableMap::COL_PRISPR_UPDATED_BY_USER_ID, ProjectIssuePriorityTableMap::COL_PRISPR_CREATED_AT, ProjectIssuePriorityTableMap::COL_PRISPR_UPDATED_AT, ),
        self::TYPE_FIELDNAME     => array('prispr_id', 'prispr_name', 'prispr_order', 'prispr_created_by_user_id', 'prispr_updated_by_user_id', 'prispr_created_at', 'prispr_updated_at', ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. self::$fieldKeys[self::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        self::TYPE_PHPNAME       => array('Id' => 0, 'Name' => 1, 'Order' => 2, 'CreatedByUserId' => 3, 'UpdatedByUserId' => 4, 'CreatedAt' => 5, 'UpdatedAt' => 6, ),
        self::TYPE_CAMELNAME     => array('id' => 0, 'name' => 1, 'order' => 2, 'createdByUserId' => 3, 'updatedByUserId' => 4, 'createdAt' => 5, 'updatedAt' => 6, ),
        self::TYPE_COLNAME       => array(ProjectIssuePriorityTableMap::COL_PRISPR_ID => 0, ProjectIssuePriorityTableMap::COL_PRISPR_NAME => 1, ProjectIssuePriorityTableMap::COL_PRISPR_ORDER => 2, ProjectIssuePriorityTableMap::COL_PRISPR_CREATED_BY_USER_ID => 3, ProjectIssuePriorityTableMap::COL_PRISPR_UPDATED_BY_USER_ID => 4, ProjectIssuePriorityTableMap::COL_PRISPR_CREATED_AT => 5, ProjectIssuePriorityTableMap::COL_PRISPR_UPDATED_AT => 6, ),
        self::TYPE_FIELDNAME     => array('prispr_id' => 0, 'prispr_name' => 1, 'prispr_order' => 2, 'prispr_created_by_user_id' => 3, 'prispr_updated_by_user_id' => 4, 'prispr_created_at' => 5, 'prispr_updated_at' => 6, ),
        self::TYPE_NUM           => array(0, 1, 2, 3, 4, 5, 6, )
    );

    /**
     * Initialize the table attributes and columns
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('project_issue_priority_prispr');
        $this->setPhpName('ProjectIssuePriority');
        $this->setIdentifierQuoting(false);
        $this->setClassName('\\IiMedias\\ProjectBundle\\Model\\ProjectIssuePriority');
        $this->setPackage('src.IiMedias.ProjectBundle.Model');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('prispr_id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('prispr_name', 'Name', 'VARCHAR', true, 255, null);
        $this->addColumn('prispr_order', 'Order', 'INTEGER', true, null, null);
        $this->addForeignKey('prispr_created_by_user_id', 'CreatedByUserId', 'INTEGER', 'user_user_usrusr', 'usrusr_id', false, null, null);
        $this->addForeignKey('prispr_updated_by_user_id', 'UpdatedByUserId', 'INTEGER', 'user_user_usrusr', 'usrusr_id', false, null, null);
        $this->addColumn('prispr_created_at', 'CreatedAt', 'TIMESTAMP', true, null, null);
        $this->addColumn('prispr_updated_at', 'UpdatedAt', 'TIMESTAMP', true, null, null);
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('CreatedByUser', '\\IiMedias\\AdminBundle\\Model\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':prispr_created_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('UpdatedByUser', '\\IiMedias\\AdminBundle\\Model\\User', RelationMap::MANY_TO_ONE, array (
  0 =>
  array (
    0 => ':prispr_updated_by_user_id',
    1 => ':usrusr_id',
  ),
), 'CASCADE', 'CASCADE', null, false);
        $this->addRelation('ProjectIssue', '\\IiMedias\\ProjectBundle\\Model\\ProjectIssue', RelationMap::ONE_TO_MANY, array (
  0 =>
  array (
    0 => ':prissu_prispr_id',
    1 => ':prispr_id',
  ),
), 'CASCADE', 'CASCADE', 'ProjectIssues', false);
    } // buildRelations()

    /**
     *
     * Gets the list of behaviors registered for this table
     *
     * @return array Associative array (name => parameters) of behaviors
     */
    public function getBehaviors()
    {
        return array(
            'timestampable' => array('create_column' => 'prispr_created_at', 'update_column' => 'prispr_updated_at', 'disable_created_at' => 'false', 'disable_updated_at' => 'false', ),
        );
    } // getBehaviors()
    /**
     * Method to invalidate the instance pool of all tables related to project_issue_priority_prispr     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
        // Invalidate objects in related instance pools,
        // since one or more of them may be deleted by ON DELETE CASCADE/SETNULL rule.
        ProjectIssueTableMap::clearInstancePool();
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return string The primary key hash of the row
     */
    public static function getPrimaryKeyHashFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        // If the PK cannot be derived from the row, return NULL.
        if ($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] === null) {
            return null;
        }

        return null === $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] || is_scalar($row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)]) || is_callable([$row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)], '__toString']) ? (string) $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)] : $row[TableMap::TYPE_NUM == $indexType ? 0 + $offset : static::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param array  $row       resultset row.
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM
     *
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        return (int) $row[
            $indexType == TableMap::TYPE_NUM
                ? 0 + $offset
                : self::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)
        ];
    }

    /**
     * The class that the tableMap will make instances of.
     *
     * If $withPrefix is true, the returned path
     * uses a dot-path notation which is translated into a path
     * relative to a location on the PHP include_path.
     * (e.g. path.to.MyClass -> 'path/to/MyClass.php')
     *
     * @param boolean $withPrefix Whether or not to return the path with the class name
     * @return string path.to.ClassName
     */
    public static function getOMClass($withPrefix = true)
    {
        return $withPrefix ? ProjectIssuePriorityTableMap::CLASS_DEFAULT : ProjectIssuePriorityTableMap::OM_CLASS;
    }

    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param array  $row       row returned by DataFetcher->fetch().
     * @param int    $offset    The 0-based offset for reading from the resultset row.
     * @param string $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                 One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                           TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     * @return array           (ProjectIssuePriority object, last column rank)
     */
    public static function populateObject($row, $offset = 0, $indexType = TableMap::TYPE_NUM)
    {
        $key = ProjectIssuePriorityTableMap::getPrimaryKeyHashFromRow($row, $offset, $indexType);
        if (null !== ($obj = ProjectIssuePriorityTableMap::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $offset, true); // rehydrate
            $col = $offset + ProjectIssuePriorityTableMap::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = ProjectIssuePriorityTableMap::OM_CLASS;
            /** @var ProjectIssuePriority $obj */
            $obj = new $cls();
            $col = $obj->hydrate($row, $offset, false, $indexType);
            ProjectIssuePriorityTableMap::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @param DataFetcherInterface $dataFetcher
     * @return array
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function populateObjects(DataFetcherInterface $dataFetcher)
    {
        $results = array();

        // set the class once to avoid overhead in the loop
        $cls = static::getOMClass(false);
        // populate the object(s)
        while ($row = $dataFetcher->fetch()) {
            $key = ProjectIssuePriorityTableMap::getPrimaryKeyHashFromRow($row, 0, $dataFetcher->getIndexType());
            if (null !== ($obj = ProjectIssuePriorityTableMap::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                /** @var ProjectIssuePriority $obj */
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                ProjectIssuePriorityTableMap::addInstanceToPool($obj, $key);
            } // if key exists
        }

        return $results;
    }
    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param Criteria $criteria object containing the columns to add.
     * @param string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(ProjectIssuePriorityTableMap::COL_PRISPR_ID);
            $criteria->addSelectColumn(ProjectIssuePriorityTableMap::COL_PRISPR_NAME);
            $criteria->addSelectColumn(ProjectIssuePriorityTableMap::COL_PRISPR_ORDER);
            $criteria->addSelectColumn(ProjectIssuePriorityTableMap::COL_PRISPR_CREATED_BY_USER_ID);
            $criteria->addSelectColumn(ProjectIssuePriorityTableMap::COL_PRISPR_UPDATED_BY_USER_ID);
            $criteria->addSelectColumn(ProjectIssuePriorityTableMap::COL_PRISPR_CREATED_AT);
            $criteria->addSelectColumn(ProjectIssuePriorityTableMap::COL_PRISPR_UPDATED_AT);
        } else {
            $criteria->addSelectColumn($alias . '.prispr_id');
            $criteria->addSelectColumn($alias . '.prispr_name');
            $criteria->addSelectColumn($alias . '.prispr_order');
            $criteria->addSelectColumn($alias . '.prispr_created_by_user_id');
            $criteria->addSelectColumn($alias . '.prispr_updated_by_user_id');
            $criteria->addSelectColumn($alias . '.prispr_created_at');
            $criteria->addSelectColumn($alias . '.prispr_updated_at');
        }
    }

    /**
     * Returns the TableMap related to this object.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getServiceContainer()->getDatabaseMap(ProjectIssuePriorityTableMap::DATABASE_NAME)->getTable(ProjectIssuePriorityTableMap::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this tableMap class.
     */
    public static function buildTableMap()
    {
        $dbMap = Propel::getServiceContainer()->getDatabaseMap(ProjectIssuePriorityTableMap::DATABASE_NAME);
        if (!$dbMap->hasTable(ProjectIssuePriorityTableMap::TABLE_NAME)) {
            $dbMap->addTableObject(new ProjectIssuePriorityTableMap());
        }
    }

    /**
     * Performs a DELETE on the database, given a ProjectIssuePriority or Criteria object OR a primary key value.
     *
     * @param mixed               $values Criteria or ProjectIssuePriority object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param  ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, ConnectionInterface $con = null)
     {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProjectIssuePriorityTableMap::DATABASE_NAME);
        }

        if ($values instanceof Criteria) {
            // rename for clarity
            $criteria = $values;
        } elseif ($values instanceof \IiMedias\ProjectBundle\Model\ProjectIssuePriority) { // it's a model object
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(ProjectIssuePriorityTableMap::DATABASE_NAME);
            $criteria->add(ProjectIssuePriorityTableMap::COL_PRISPR_ID, (array) $values, Criteria::IN);
        }

        $query = ProjectIssuePriorityQuery::create()->mergeWith($criteria);

        if ($values instanceof Criteria) {
            ProjectIssuePriorityTableMap::clearInstancePool();
        } elseif (!is_object($values)) { // it's a primary key, or an array of pks
            foreach ((array) $values as $singleval) {
                ProjectIssuePriorityTableMap::removeInstanceFromPool($singleval);
            }
        }

        return $query->delete($con);
    }

    /**
     * Deletes all rows from the project_issue_priority_prispr table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public static function doDeleteAll(ConnectionInterface $con = null)
    {
        return ProjectIssuePriorityQuery::create()->doDeleteAll($con);
    }

    /**
     * Performs an INSERT on the database, given a ProjectIssuePriority or Criteria object.
     *
     * @param mixed               $criteria Criteria or ProjectIssuePriority object containing data that is used to create the INSERT statement.
     * @param ConnectionInterface $con the ConnectionInterface connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public static function doInsert($criteria, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProjectIssuePriorityTableMap::DATABASE_NAME);
        }

        if ($criteria instanceof Criteria) {
            $criteria = clone $criteria; // rename for clarity
        } else {
            $criteria = $criteria->buildCriteria(); // build Criteria from ProjectIssuePriority object
        }

        if ($criteria->containsKey(ProjectIssuePriorityTableMap::COL_PRISPR_ID) && $criteria->keyContainsValue(ProjectIssuePriorityTableMap::COL_PRISPR_ID) ) {
            throw new PropelException('Cannot insert a value for auto-increment primary key ('.ProjectIssuePriorityTableMap::COL_PRISPR_ID.')');
        }


        // Set the correct dbName
        $query = ProjectIssuePriorityQuery::create()->mergeWith($criteria);

        // use transaction because $criteria could contain info
        // for more than one table (I guess, conceivably)
        return $con->transaction(function () use ($con, $query) {
            return $query->doInsert($con);
        });
    }

} // ProjectIssuePriorityTableMap
// This is the static code needed to register the TableMap for this table with the main Propel class.
//
ProjectIssuePriorityTableMap::buildTableMap();

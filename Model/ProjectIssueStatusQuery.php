<?php

namespace IiMedias\ProjectBundle\Model;

use IiMedias\ProjectBundle\Model\Base\ProjectIssueStatusQuery as BaseProjectIssueStatusQuery;
use Propel\Runtime\ActiveQuery\Criteria;

/**
 * Skeleton subclass for performing query and update operations on the 'project_issue_status_prisst' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ProjectIssueStatusQuery extends BaseProjectIssueStatusQuery
{
    public static function getCriteriaForProjectIssueTypeForm()
    {
        $statuses = self::create()
            ->orderByOrder(Criteria::ASC)
        ;

        return $statuses;
    }
}

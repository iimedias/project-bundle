<?php

namespace IiMedias\ProjectBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\AdminBundle\Model\User;
use IiMedias\ProjectBundle\Model\ProjectIssue as ChildProjectIssue;
use IiMedias\ProjectBundle\Model\ProjectIssueQuery as ChildProjectIssueQuery;
use IiMedias\ProjectBundle\Model\Map\ProjectIssueTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'project_issue_prissu' table.
 *
 *
 *
 * @method     ChildProjectIssueQuery orderById($order = Criteria::ASC) Order by the prissu_id column
 * @method     ChildProjectIssueQuery orderByProjectId($order = Criteria::ASC) Order by the prissu_prproj_id column
 * @method     ChildProjectIssueQuery orderByParentId($order = Criteria::ASC) Order by the prissu_parent_id column
 * @method     ChildProjectIssueQuery orderByTypeId($order = Criteria::ASC) Order by the prissu_pristy_id column
 * @method     ChildProjectIssueQuery orderBySubject($order = Criteria::ASC) Order by the prissu_subject column
 * @method     ChildProjectIssueQuery orderByDescription($order = Criteria::ASC) Order by the prissu_description column
 * @method     ChildProjectIssueQuery orderByStatusId($order = Criteria::ASC) Order by the prissu_prisst_id column
 * @method     ChildProjectIssueQuery orderByPriorityId($order = Criteria::ASC) Order by the prissu_prispr_id column
 * @method     ChildProjectIssueQuery orderByAssignedUserId($order = Criteria::ASC) Order by the prissu_assigned_user_id column
 * @method     ChildProjectIssueQuery orderByStartAt($order = Criteria::ASC) Order by the prissu_start_at column
 * @method     ChildProjectIssueQuery orderByEndAt($order = Criteria::ASC) Order by the prissu_end_at column
 * @method     ChildProjectIssueQuery orderByEstimatedTime($order = Criteria::ASC) Order by the prissu_estimated_time column
 * @method     ChildProjectIssueQuery orderByCompletedPercentage($order = Criteria::ASC) Order by the prissu_completed_percentage column
 * @method     ChildProjectIssueQuery orderByChildrenCompletedPercentage($order = Criteria::ASC) Order by the prissu_children_completed_percentage column
 * @method     ChildProjectIssueQuery orderByCreatedByUserId($order = Criteria::ASC) Order by the prissu_created_by_user_id column
 * @method     ChildProjectIssueQuery orderByUpdatedByUserId($order = Criteria::ASC) Order by the prissu_updated_by_user_id column
 * @method     ChildProjectIssueQuery orderByCreatedAt($order = Criteria::ASC) Order by the prissu_created_at column
 * @method     ChildProjectIssueQuery orderByUpdatedAt($order = Criteria::ASC) Order by the prissu_updated_at column
 *
 * @method     ChildProjectIssueQuery groupById() Group by the prissu_id column
 * @method     ChildProjectIssueQuery groupByProjectId() Group by the prissu_prproj_id column
 * @method     ChildProjectIssueQuery groupByParentId() Group by the prissu_parent_id column
 * @method     ChildProjectIssueQuery groupByTypeId() Group by the prissu_pristy_id column
 * @method     ChildProjectIssueQuery groupBySubject() Group by the prissu_subject column
 * @method     ChildProjectIssueQuery groupByDescription() Group by the prissu_description column
 * @method     ChildProjectIssueQuery groupByStatusId() Group by the prissu_prisst_id column
 * @method     ChildProjectIssueQuery groupByPriorityId() Group by the prissu_prispr_id column
 * @method     ChildProjectIssueQuery groupByAssignedUserId() Group by the prissu_assigned_user_id column
 * @method     ChildProjectIssueQuery groupByStartAt() Group by the prissu_start_at column
 * @method     ChildProjectIssueQuery groupByEndAt() Group by the prissu_end_at column
 * @method     ChildProjectIssueQuery groupByEstimatedTime() Group by the prissu_estimated_time column
 * @method     ChildProjectIssueQuery groupByCompletedPercentage() Group by the prissu_completed_percentage column
 * @method     ChildProjectIssueQuery groupByChildrenCompletedPercentage() Group by the prissu_children_completed_percentage column
 * @method     ChildProjectIssueQuery groupByCreatedByUserId() Group by the prissu_created_by_user_id column
 * @method     ChildProjectIssueQuery groupByUpdatedByUserId() Group by the prissu_updated_by_user_id column
 * @method     ChildProjectIssueQuery groupByCreatedAt() Group by the prissu_created_at column
 * @method     ChildProjectIssueQuery groupByUpdatedAt() Group by the prissu_updated_at column
 *
 * @method     ChildProjectIssueQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildProjectIssueQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildProjectIssueQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildProjectIssueQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildProjectIssueQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildProjectIssueQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildProjectIssueQuery leftJoinProject($relationAlias = null) Adds a LEFT JOIN clause to the query using the Project relation
 * @method     ChildProjectIssueQuery rightJoinProject($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Project relation
 * @method     ChildProjectIssueQuery innerJoinProject($relationAlias = null) Adds a INNER JOIN clause to the query using the Project relation
 *
 * @method     ChildProjectIssueQuery joinWithProject($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Project relation
 *
 * @method     ChildProjectIssueQuery leftJoinWithProject() Adds a LEFT JOIN clause and with to the query using the Project relation
 * @method     ChildProjectIssueQuery rightJoinWithProject() Adds a RIGHT JOIN clause and with to the query using the Project relation
 * @method     ChildProjectIssueQuery innerJoinWithProject() Adds a INNER JOIN clause and with to the query using the Project relation
 *
 * @method     ChildProjectIssueQuery leftJoinProjectIssueParent($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProjectIssueParent relation
 * @method     ChildProjectIssueQuery rightJoinProjectIssueParent($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProjectIssueParent relation
 * @method     ChildProjectIssueQuery innerJoinProjectIssueParent($relationAlias = null) Adds a INNER JOIN clause to the query using the ProjectIssueParent relation
 *
 * @method     ChildProjectIssueQuery joinWithProjectIssueParent($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProjectIssueParent relation
 *
 * @method     ChildProjectIssueQuery leftJoinWithProjectIssueParent() Adds a LEFT JOIN clause and with to the query using the ProjectIssueParent relation
 * @method     ChildProjectIssueQuery rightJoinWithProjectIssueParent() Adds a RIGHT JOIN clause and with to the query using the ProjectIssueParent relation
 * @method     ChildProjectIssueQuery innerJoinWithProjectIssueParent() Adds a INNER JOIN clause and with to the query using the ProjectIssueParent relation
 *
 * @method     ChildProjectIssueQuery leftJoinProjectIssueType($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProjectIssueType relation
 * @method     ChildProjectIssueQuery rightJoinProjectIssueType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProjectIssueType relation
 * @method     ChildProjectIssueQuery innerJoinProjectIssueType($relationAlias = null) Adds a INNER JOIN clause to the query using the ProjectIssueType relation
 *
 * @method     ChildProjectIssueQuery joinWithProjectIssueType($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProjectIssueType relation
 *
 * @method     ChildProjectIssueQuery leftJoinWithProjectIssueType() Adds a LEFT JOIN clause and with to the query using the ProjectIssueType relation
 * @method     ChildProjectIssueQuery rightJoinWithProjectIssueType() Adds a RIGHT JOIN clause and with to the query using the ProjectIssueType relation
 * @method     ChildProjectIssueQuery innerJoinWithProjectIssueType() Adds a INNER JOIN clause and with to the query using the ProjectIssueType relation
 *
 * @method     ChildProjectIssueQuery leftJoinProjectIssueStatus($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProjectIssueStatus relation
 * @method     ChildProjectIssueQuery rightJoinProjectIssueStatus($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProjectIssueStatus relation
 * @method     ChildProjectIssueQuery innerJoinProjectIssueStatus($relationAlias = null) Adds a INNER JOIN clause to the query using the ProjectIssueStatus relation
 *
 * @method     ChildProjectIssueQuery joinWithProjectIssueStatus($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProjectIssueStatus relation
 *
 * @method     ChildProjectIssueQuery leftJoinWithProjectIssueStatus() Adds a LEFT JOIN clause and with to the query using the ProjectIssueStatus relation
 * @method     ChildProjectIssueQuery rightJoinWithProjectIssueStatus() Adds a RIGHT JOIN clause and with to the query using the ProjectIssueStatus relation
 * @method     ChildProjectIssueQuery innerJoinWithProjectIssueStatus() Adds a INNER JOIN clause and with to the query using the ProjectIssueStatus relation
 *
 * @method     ChildProjectIssueQuery leftJoinProjectIssuePriority($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProjectIssuePriority relation
 * @method     ChildProjectIssueQuery rightJoinProjectIssuePriority($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProjectIssuePriority relation
 * @method     ChildProjectIssueQuery innerJoinProjectIssuePriority($relationAlias = null) Adds a INNER JOIN clause to the query using the ProjectIssuePriority relation
 *
 * @method     ChildProjectIssueQuery joinWithProjectIssuePriority($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProjectIssuePriority relation
 *
 * @method     ChildProjectIssueQuery leftJoinWithProjectIssuePriority() Adds a LEFT JOIN clause and with to the query using the ProjectIssuePriority relation
 * @method     ChildProjectIssueQuery rightJoinWithProjectIssuePriority() Adds a RIGHT JOIN clause and with to the query using the ProjectIssuePriority relation
 * @method     ChildProjectIssueQuery innerJoinWithProjectIssuePriority() Adds a INNER JOIN clause and with to the query using the ProjectIssuePriority relation
 *
 * @method     ChildProjectIssueQuery leftJoinAssignedUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the AssignedUser relation
 * @method     ChildProjectIssueQuery rightJoinAssignedUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the AssignedUser relation
 * @method     ChildProjectIssueQuery innerJoinAssignedUser($relationAlias = null) Adds a INNER JOIN clause to the query using the AssignedUser relation
 *
 * @method     ChildProjectIssueQuery joinWithAssignedUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the AssignedUser relation
 *
 * @method     ChildProjectIssueQuery leftJoinWithAssignedUser() Adds a LEFT JOIN clause and with to the query using the AssignedUser relation
 * @method     ChildProjectIssueQuery rightJoinWithAssignedUser() Adds a RIGHT JOIN clause and with to the query using the AssignedUser relation
 * @method     ChildProjectIssueQuery innerJoinWithAssignedUser() Adds a INNER JOIN clause and with to the query using the AssignedUser relation
 *
 * @method     ChildProjectIssueQuery leftJoinCreatedByUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the CreatedByUser relation
 * @method     ChildProjectIssueQuery rightJoinCreatedByUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CreatedByUser relation
 * @method     ChildProjectIssueQuery innerJoinCreatedByUser($relationAlias = null) Adds a INNER JOIN clause to the query using the CreatedByUser relation
 *
 * @method     ChildProjectIssueQuery joinWithCreatedByUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CreatedByUser relation
 *
 * @method     ChildProjectIssueQuery leftJoinWithCreatedByUser() Adds a LEFT JOIN clause and with to the query using the CreatedByUser relation
 * @method     ChildProjectIssueQuery rightJoinWithCreatedByUser() Adds a RIGHT JOIN clause and with to the query using the CreatedByUser relation
 * @method     ChildProjectIssueQuery innerJoinWithCreatedByUser() Adds a INNER JOIN clause and with to the query using the CreatedByUser relation
 *
 * @method     ChildProjectIssueQuery leftJoinUpdatedByUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the UpdatedByUser relation
 * @method     ChildProjectIssueQuery rightJoinUpdatedByUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UpdatedByUser relation
 * @method     ChildProjectIssueQuery innerJoinUpdatedByUser($relationAlias = null) Adds a INNER JOIN clause to the query using the UpdatedByUser relation
 *
 * @method     ChildProjectIssueQuery joinWithUpdatedByUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UpdatedByUser relation
 *
 * @method     ChildProjectIssueQuery leftJoinWithUpdatedByUser() Adds a LEFT JOIN clause and with to the query using the UpdatedByUser relation
 * @method     ChildProjectIssueQuery rightJoinWithUpdatedByUser() Adds a RIGHT JOIN clause and with to the query using the UpdatedByUser relation
 * @method     ChildProjectIssueQuery innerJoinWithUpdatedByUser() Adds a INNER JOIN clause and with to the query using the UpdatedByUser relation
 *
 * @method     ChildProjectIssueQuery leftJoinProjectIssueChild($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProjectIssueChild relation
 * @method     ChildProjectIssueQuery rightJoinProjectIssueChild($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProjectIssueChild relation
 * @method     ChildProjectIssueQuery innerJoinProjectIssueChild($relationAlias = null) Adds a INNER JOIN clause to the query using the ProjectIssueChild relation
 *
 * @method     ChildProjectIssueQuery joinWithProjectIssueChild($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProjectIssueChild relation
 *
 * @method     ChildProjectIssueQuery leftJoinWithProjectIssueChild() Adds a LEFT JOIN clause and with to the query using the ProjectIssueChild relation
 * @method     ChildProjectIssueQuery rightJoinWithProjectIssueChild() Adds a RIGHT JOIN clause and with to the query using the ProjectIssueChild relation
 * @method     ChildProjectIssueQuery innerJoinWithProjectIssueChild() Adds a INNER JOIN clause and with to the query using the ProjectIssueChild relation
 *
 * @method     ChildProjectIssueQuery leftJoinProjectIssueComment($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProjectIssueComment relation
 * @method     ChildProjectIssueQuery rightJoinProjectIssueComment($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProjectIssueComment relation
 * @method     ChildProjectIssueQuery innerJoinProjectIssueComment($relationAlias = null) Adds a INNER JOIN clause to the query using the ProjectIssueComment relation
 *
 * @method     ChildProjectIssueQuery joinWithProjectIssueComment($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProjectIssueComment relation
 *
 * @method     ChildProjectIssueQuery leftJoinWithProjectIssueComment() Adds a LEFT JOIN clause and with to the query using the ProjectIssueComment relation
 * @method     ChildProjectIssueQuery rightJoinWithProjectIssueComment() Adds a RIGHT JOIN clause and with to the query using the ProjectIssueComment relation
 * @method     ChildProjectIssueQuery innerJoinWithProjectIssueComment() Adds a INNER JOIN clause and with to the query using the ProjectIssueComment relation
 *
 * @method     \IiMedias\ProjectBundle\Model\ProjectQuery|\IiMedias\ProjectBundle\Model\ProjectIssueQuery|\IiMedias\ProjectBundle\Model\ProjectIssueTypeQuery|\IiMedias\ProjectBundle\Model\ProjectIssueStatusQuery|\IiMedias\ProjectBundle\Model\ProjectIssuePriorityQuery|\IiMedias\AdminBundle\Model\UserQuery|\IiMedias\ProjectBundle\Model\ProjectIssueCommentQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildProjectIssue findOne(ConnectionInterface $con = null) Return the first ChildProjectIssue matching the query
 * @method     ChildProjectIssue findOneOrCreate(ConnectionInterface $con = null) Return the first ChildProjectIssue matching the query, or a new ChildProjectIssue object populated from the query conditions when no match is found
 *
 * @method     ChildProjectIssue findOneById(int $prissu_id) Return the first ChildProjectIssue filtered by the prissu_id column
 * @method     ChildProjectIssue findOneByProjectId(int $prissu_prproj_id) Return the first ChildProjectIssue filtered by the prissu_prproj_id column
 * @method     ChildProjectIssue findOneByParentId(int $prissu_parent_id) Return the first ChildProjectIssue filtered by the prissu_parent_id column
 * @method     ChildProjectIssue findOneByTypeId(int $prissu_pristy_id) Return the first ChildProjectIssue filtered by the prissu_pristy_id column
 * @method     ChildProjectIssue findOneBySubject(string $prissu_subject) Return the first ChildProjectIssue filtered by the prissu_subject column
 * @method     ChildProjectIssue findOneByDescription(string $prissu_description) Return the first ChildProjectIssue filtered by the prissu_description column
 * @method     ChildProjectIssue findOneByStatusId(int $prissu_prisst_id) Return the first ChildProjectIssue filtered by the prissu_prisst_id column
 * @method     ChildProjectIssue findOneByPriorityId(int $prissu_prispr_id) Return the first ChildProjectIssue filtered by the prissu_prispr_id column
 * @method     ChildProjectIssue findOneByAssignedUserId(int $prissu_assigned_user_id) Return the first ChildProjectIssue filtered by the prissu_assigned_user_id column
 * @method     ChildProjectIssue findOneByStartAt(string $prissu_start_at) Return the first ChildProjectIssue filtered by the prissu_start_at column
 * @method     ChildProjectIssue findOneByEndAt(string $prissu_end_at) Return the first ChildProjectIssue filtered by the prissu_end_at column
 * @method     ChildProjectIssue findOneByEstimatedTime(double $prissu_estimated_time) Return the first ChildProjectIssue filtered by the prissu_estimated_time column
 * @method     ChildProjectIssue findOneByCompletedPercentage(double $prissu_completed_percentage) Return the first ChildProjectIssue filtered by the prissu_completed_percentage column
 * @method     ChildProjectIssue findOneByChildrenCompletedPercentage(double $prissu_children_completed_percentage) Return the first ChildProjectIssue filtered by the prissu_children_completed_percentage column
 * @method     ChildProjectIssue findOneByCreatedByUserId(int $prissu_created_by_user_id) Return the first ChildProjectIssue filtered by the prissu_created_by_user_id column
 * @method     ChildProjectIssue findOneByUpdatedByUserId(int $prissu_updated_by_user_id) Return the first ChildProjectIssue filtered by the prissu_updated_by_user_id column
 * @method     ChildProjectIssue findOneByCreatedAt(string $prissu_created_at) Return the first ChildProjectIssue filtered by the prissu_created_at column
 * @method     ChildProjectIssue findOneByUpdatedAt(string $prissu_updated_at) Return the first ChildProjectIssue filtered by the prissu_updated_at column *

 * @method     ChildProjectIssue requirePk($key, ConnectionInterface $con = null) Return the ChildProjectIssue by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssue requireOne(ConnectionInterface $con = null) Return the first ChildProjectIssue matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildProjectIssue requireOneById(int $prissu_id) Return the first ChildProjectIssue filtered by the prissu_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssue requireOneByProjectId(int $prissu_prproj_id) Return the first ChildProjectIssue filtered by the prissu_prproj_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssue requireOneByParentId(int $prissu_parent_id) Return the first ChildProjectIssue filtered by the prissu_parent_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssue requireOneByTypeId(int $prissu_pristy_id) Return the first ChildProjectIssue filtered by the prissu_pristy_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssue requireOneBySubject(string $prissu_subject) Return the first ChildProjectIssue filtered by the prissu_subject column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssue requireOneByDescription(string $prissu_description) Return the first ChildProjectIssue filtered by the prissu_description column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssue requireOneByStatusId(int $prissu_prisst_id) Return the first ChildProjectIssue filtered by the prissu_prisst_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssue requireOneByPriorityId(int $prissu_prispr_id) Return the first ChildProjectIssue filtered by the prissu_prispr_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssue requireOneByAssignedUserId(int $prissu_assigned_user_id) Return the first ChildProjectIssue filtered by the prissu_assigned_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssue requireOneByStartAt(string $prissu_start_at) Return the first ChildProjectIssue filtered by the prissu_start_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssue requireOneByEndAt(string $prissu_end_at) Return the first ChildProjectIssue filtered by the prissu_end_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssue requireOneByEstimatedTime(double $prissu_estimated_time) Return the first ChildProjectIssue filtered by the prissu_estimated_time column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssue requireOneByCompletedPercentage(double $prissu_completed_percentage) Return the first ChildProjectIssue filtered by the prissu_completed_percentage column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssue requireOneByChildrenCompletedPercentage(double $prissu_children_completed_percentage) Return the first ChildProjectIssue filtered by the prissu_children_completed_percentage column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssue requireOneByCreatedByUserId(int $prissu_created_by_user_id) Return the first ChildProjectIssue filtered by the prissu_created_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssue requireOneByUpdatedByUserId(int $prissu_updated_by_user_id) Return the first ChildProjectIssue filtered by the prissu_updated_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssue requireOneByCreatedAt(string $prissu_created_at) Return the first ChildProjectIssue filtered by the prissu_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssue requireOneByUpdatedAt(string $prissu_updated_at) Return the first ChildProjectIssue filtered by the prissu_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildProjectIssue[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildProjectIssue objects based on current ModelCriteria
 * @method     ChildProjectIssue[]|ObjectCollection findById(int $prissu_id) Return ChildProjectIssue objects filtered by the prissu_id column
 * @method     ChildProjectIssue[]|ObjectCollection findByProjectId(int $prissu_prproj_id) Return ChildProjectIssue objects filtered by the prissu_prproj_id column
 * @method     ChildProjectIssue[]|ObjectCollection findByParentId(int $prissu_parent_id) Return ChildProjectIssue objects filtered by the prissu_parent_id column
 * @method     ChildProjectIssue[]|ObjectCollection findByTypeId(int $prissu_pristy_id) Return ChildProjectIssue objects filtered by the prissu_pristy_id column
 * @method     ChildProjectIssue[]|ObjectCollection findBySubject(string $prissu_subject) Return ChildProjectIssue objects filtered by the prissu_subject column
 * @method     ChildProjectIssue[]|ObjectCollection findByDescription(string $prissu_description) Return ChildProjectIssue objects filtered by the prissu_description column
 * @method     ChildProjectIssue[]|ObjectCollection findByStatusId(int $prissu_prisst_id) Return ChildProjectIssue objects filtered by the prissu_prisst_id column
 * @method     ChildProjectIssue[]|ObjectCollection findByPriorityId(int $prissu_prispr_id) Return ChildProjectIssue objects filtered by the prissu_prispr_id column
 * @method     ChildProjectIssue[]|ObjectCollection findByAssignedUserId(int $prissu_assigned_user_id) Return ChildProjectIssue objects filtered by the prissu_assigned_user_id column
 * @method     ChildProjectIssue[]|ObjectCollection findByStartAt(string $prissu_start_at) Return ChildProjectIssue objects filtered by the prissu_start_at column
 * @method     ChildProjectIssue[]|ObjectCollection findByEndAt(string $prissu_end_at) Return ChildProjectIssue objects filtered by the prissu_end_at column
 * @method     ChildProjectIssue[]|ObjectCollection findByEstimatedTime(double $prissu_estimated_time) Return ChildProjectIssue objects filtered by the prissu_estimated_time column
 * @method     ChildProjectIssue[]|ObjectCollection findByCompletedPercentage(double $prissu_completed_percentage) Return ChildProjectIssue objects filtered by the prissu_completed_percentage column
 * @method     ChildProjectIssue[]|ObjectCollection findByChildrenCompletedPercentage(double $prissu_children_completed_percentage) Return ChildProjectIssue objects filtered by the prissu_children_completed_percentage column
 * @method     ChildProjectIssue[]|ObjectCollection findByCreatedByUserId(int $prissu_created_by_user_id) Return ChildProjectIssue objects filtered by the prissu_created_by_user_id column
 * @method     ChildProjectIssue[]|ObjectCollection findByUpdatedByUserId(int $prissu_updated_by_user_id) Return ChildProjectIssue objects filtered by the prissu_updated_by_user_id column
 * @method     ChildProjectIssue[]|ObjectCollection findByCreatedAt(string $prissu_created_at) Return ChildProjectIssue objects filtered by the prissu_created_at column
 * @method     ChildProjectIssue[]|ObjectCollection findByUpdatedAt(string $prissu_updated_at) Return ChildProjectIssue objects filtered by the prissu_updated_at column
 * @method     ChildProjectIssue[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ProjectIssueQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\ProjectBundle\Model\Base\ProjectIssueQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\ProjectBundle\\Model\\ProjectIssue', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildProjectIssueQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildProjectIssueQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildProjectIssueQuery) {
            return $criteria;
        }
        $query = new ChildProjectIssueQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildProjectIssue|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ProjectIssueTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ProjectIssueTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProjectIssue A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT prissu_id, prissu_prproj_id, prissu_parent_id, prissu_pristy_id, prissu_subject, prissu_description, prissu_prisst_id, prissu_prispr_id, prissu_assigned_user_id, prissu_start_at, prissu_end_at, prissu_estimated_time, prissu_completed_percentage, prissu_children_completed_percentage, prissu_created_by_user_id, prissu_updated_by_user_id, prissu_created_at, prissu_updated_at FROM project_issue_prissu WHERE prissu_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildProjectIssue $obj */
            $obj = new ChildProjectIssue();
            $obj->hydrate($row);
            ProjectIssueTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildProjectIssue|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the prissu_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE prissu_id = 1234
     * $query->filterById(array(12, 34)); // WHERE prissu_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE prissu_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_ID, $id, $comparison);
    }

    /**
     * Filter the query on the prissu_prproj_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectId(1234); // WHERE prissu_prproj_id = 1234
     * $query->filterByProjectId(array(12, 34)); // WHERE prissu_prproj_id IN (12, 34)
     * $query->filterByProjectId(array('min' => 12)); // WHERE prissu_prproj_id > 12
     * </code>
     *
     * @see       filterByProject()
     *
     * @param     mixed $projectId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByProjectId($projectId = null, $comparison = null)
    {
        if (is_array($projectId)) {
            $useMinMax = false;
            if (isset($projectId['min'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_PRPROJ_ID, $projectId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($projectId['max'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_PRPROJ_ID, $projectId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_PRPROJ_ID, $projectId, $comparison);
    }

    /**
     * Filter the query on the prissu_parent_id column
     *
     * Example usage:
     * <code>
     * $query->filterByParentId(1234); // WHERE prissu_parent_id = 1234
     * $query->filterByParentId(array(12, 34)); // WHERE prissu_parent_id IN (12, 34)
     * $query->filterByParentId(array('min' => 12)); // WHERE prissu_parent_id > 12
     * </code>
     *
     * @see       filterByProjectIssueParent()
     *
     * @param     mixed $parentId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByParentId($parentId = null, $comparison = null)
    {
        if (is_array($parentId)) {
            $useMinMax = false;
            if (isset($parentId['min'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_PARENT_ID, $parentId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($parentId['max'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_PARENT_ID, $parentId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_PARENT_ID, $parentId, $comparison);
    }

    /**
     * Filter the query on the prissu_pristy_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTypeId(1234); // WHERE prissu_pristy_id = 1234
     * $query->filterByTypeId(array(12, 34)); // WHERE prissu_pristy_id IN (12, 34)
     * $query->filterByTypeId(array('min' => 12)); // WHERE prissu_pristy_id > 12
     * </code>
     *
     * @see       filterByProjectIssueType()
     *
     * @param     mixed $typeId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByTypeId($typeId = null, $comparison = null)
    {
        if (is_array($typeId)) {
            $useMinMax = false;
            if (isset($typeId['min'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_PRISTY_ID, $typeId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($typeId['max'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_PRISTY_ID, $typeId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_PRISTY_ID, $typeId, $comparison);
    }

    /**
     * Filter the query on the prissu_subject column
     *
     * Example usage:
     * <code>
     * $query->filterBySubject('fooValue');   // WHERE prissu_subject = 'fooValue'
     * $query->filterBySubject('%fooValue%'); // WHERE prissu_subject LIKE '%fooValue%'
     * </code>
     *
     * @param     string $subject The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterBySubject($subject = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($subject)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_SUBJECT, $subject, $comparison);
    }

    /**
     * Filter the query on the prissu_description column
     *
     * Example usage:
     * <code>
     * $query->filterByDescription('fooValue');   // WHERE prissu_description = 'fooValue'
     * $query->filterByDescription('%fooValue%'); // WHERE prissu_description LIKE '%fooValue%'
     * </code>
     *
     * @param     string $description The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByDescription($description = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($description)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_DESCRIPTION, $description, $comparison);
    }

    /**
     * Filter the query on the prissu_prisst_id column
     *
     * Example usage:
     * <code>
     * $query->filterByStatusId(1234); // WHERE prissu_prisst_id = 1234
     * $query->filterByStatusId(array(12, 34)); // WHERE prissu_prisst_id IN (12, 34)
     * $query->filterByStatusId(array('min' => 12)); // WHERE prissu_prisst_id > 12
     * </code>
     *
     * @see       filterByProjectIssueStatus()
     *
     * @param     mixed $statusId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByStatusId($statusId = null, $comparison = null)
    {
        if (is_array($statusId)) {
            $useMinMax = false;
            if (isset($statusId['min'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_PRISST_ID, $statusId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($statusId['max'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_PRISST_ID, $statusId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_PRISST_ID, $statusId, $comparison);
    }

    /**
     * Filter the query on the prissu_prispr_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPriorityId(1234); // WHERE prissu_prispr_id = 1234
     * $query->filterByPriorityId(array(12, 34)); // WHERE prissu_prispr_id IN (12, 34)
     * $query->filterByPriorityId(array('min' => 12)); // WHERE prissu_prispr_id > 12
     * </code>
     *
     * @see       filterByProjectIssuePriority()
     *
     * @param     mixed $priorityId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByPriorityId($priorityId = null, $comparison = null)
    {
        if (is_array($priorityId)) {
            $useMinMax = false;
            if (isset($priorityId['min'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_PRISPR_ID, $priorityId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($priorityId['max'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_PRISPR_ID, $priorityId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_PRISPR_ID, $priorityId, $comparison);
    }

    /**
     * Filter the query on the prissu_assigned_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByAssignedUserId(1234); // WHERE prissu_assigned_user_id = 1234
     * $query->filterByAssignedUserId(array(12, 34)); // WHERE prissu_assigned_user_id IN (12, 34)
     * $query->filterByAssignedUserId(array('min' => 12)); // WHERE prissu_assigned_user_id > 12
     * </code>
     *
     * @see       filterByAssignedUser()
     *
     * @param     mixed $assignedUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByAssignedUserId($assignedUserId = null, $comparison = null)
    {
        if (is_array($assignedUserId)) {
            $useMinMax = false;
            if (isset($assignedUserId['min'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_ASSIGNED_USER_ID, $assignedUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($assignedUserId['max'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_ASSIGNED_USER_ID, $assignedUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_ASSIGNED_USER_ID, $assignedUserId, $comparison);
    }

    /**
     * Filter the query on the prissu_start_at column
     *
     * Example usage:
     * <code>
     * $query->filterByStartAt('2011-03-14'); // WHERE prissu_start_at = '2011-03-14'
     * $query->filterByStartAt('now'); // WHERE prissu_start_at = '2011-03-14'
     * $query->filterByStartAt(array('max' => 'yesterday')); // WHERE prissu_start_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $startAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByStartAt($startAt = null, $comparison = null)
    {
        if (is_array($startAt)) {
            $useMinMax = false;
            if (isset($startAt['min'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_START_AT, $startAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($startAt['max'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_START_AT, $startAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_START_AT, $startAt, $comparison);
    }

    /**
     * Filter the query on the prissu_end_at column
     *
     * Example usage:
     * <code>
     * $query->filterByEndAt('2011-03-14'); // WHERE prissu_end_at = '2011-03-14'
     * $query->filterByEndAt('now'); // WHERE prissu_end_at = '2011-03-14'
     * $query->filterByEndAt(array('max' => 'yesterday')); // WHERE prissu_end_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $endAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByEndAt($endAt = null, $comparison = null)
    {
        if (is_array($endAt)) {
            $useMinMax = false;
            if (isset($endAt['min'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_END_AT, $endAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($endAt['max'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_END_AT, $endAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_END_AT, $endAt, $comparison);
    }

    /**
     * Filter the query on the prissu_estimated_time column
     *
     * Example usage:
     * <code>
     * $query->filterByEstimatedTime(1234); // WHERE prissu_estimated_time = 1234
     * $query->filterByEstimatedTime(array(12, 34)); // WHERE prissu_estimated_time IN (12, 34)
     * $query->filterByEstimatedTime(array('min' => 12)); // WHERE prissu_estimated_time > 12
     * </code>
     *
     * @param     mixed $estimatedTime The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByEstimatedTime($estimatedTime = null, $comparison = null)
    {
        if (is_array($estimatedTime)) {
            $useMinMax = false;
            if (isset($estimatedTime['min'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_ESTIMATED_TIME, $estimatedTime['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($estimatedTime['max'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_ESTIMATED_TIME, $estimatedTime['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_ESTIMATED_TIME, $estimatedTime, $comparison);
    }

    /**
     * Filter the query on the prissu_completed_percentage column
     *
     * Example usage:
     * <code>
     * $query->filterByCompletedPercentage(1234); // WHERE prissu_completed_percentage = 1234
     * $query->filterByCompletedPercentage(array(12, 34)); // WHERE prissu_completed_percentage IN (12, 34)
     * $query->filterByCompletedPercentage(array('min' => 12)); // WHERE prissu_completed_percentage > 12
     * </code>
     *
     * @param     mixed $completedPercentage The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByCompletedPercentage($completedPercentage = null, $comparison = null)
    {
        if (is_array($completedPercentage)) {
            $useMinMax = false;
            if (isset($completedPercentage['min'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_COMPLETED_PERCENTAGE, $completedPercentage['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($completedPercentage['max'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_COMPLETED_PERCENTAGE, $completedPercentage['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_COMPLETED_PERCENTAGE, $completedPercentage, $comparison);
    }

    /**
     * Filter the query on the prissu_children_completed_percentage column
     *
     * Example usage:
     * <code>
     * $query->filterByChildrenCompletedPercentage(1234); // WHERE prissu_children_completed_percentage = 1234
     * $query->filterByChildrenCompletedPercentage(array(12, 34)); // WHERE prissu_children_completed_percentage IN (12, 34)
     * $query->filterByChildrenCompletedPercentage(array('min' => 12)); // WHERE prissu_children_completed_percentage > 12
     * </code>
     *
     * @param     mixed $childrenCompletedPercentage The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByChildrenCompletedPercentage($childrenCompletedPercentage = null, $comparison = null)
    {
        if (is_array($childrenCompletedPercentage)) {
            $useMinMax = false;
            if (isset($childrenCompletedPercentage['min'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_CHILDREN_COMPLETED_PERCENTAGE, $childrenCompletedPercentage['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($childrenCompletedPercentage['max'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_CHILDREN_COMPLETED_PERCENTAGE, $childrenCompletedPercentage['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_CHILDREN_COMPLETED_PERCENTAGE, $childrenCompletedPercentage, $comparison);
    }

    /**
     * Filter the query on the prissu_created_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedByUserId(1234); // WHERE prissu_created_by_user_id = 1234
     * $query->filterByCreatedByUserId(array(12, 34)); // WHERE prissu_created_by_user_id IN (12, 34)
     * $query->filterByCreatedByUserId(array('min' => 12)); // WHERE prissu_created_by_user_id > 12
     * </code>
     *
     * @see       filterByCreatedByUser()
     *
     * @param     mixed $createdByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByCreatedByUserId($createdByUserId = null, $comparison = null)
    {
        if (is_array($createdByUserId)) {
            $useMinMax = false;
            if (isset($createdByUserId['min'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_CREATED_BY_USER_ID, $createdByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdByUserId['max'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_CREATED_BY_USER_ID, $createdByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_CREATED_BY_USER_ID, $createdByUserId, $comparison);
    }

    /**
     * Filter the query on the prissu_updated_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedByUserId(1234); // WHERE prissu_updated_by_user_id = 1234
     * $query->filterByUpdatedByUserId(array(12, 34)); // WHERE prissu_updated_by_user_id IN (12, 34)
     * $query->filterByUpdatedByUserId(array('min' => 12)); // WHERE prissu_updated_by_user_id > 12
     * </code>
     *
     * @see       filterByUpdatedByUser()
     *
     * @param     mixed $updatedByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUserId($updatedByUserId = null, $comparison = null)
    {
        if (is_array($updatedByUserId)) {
            $useMinMax = false;
            if (isset($updatedByUserId['min'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_UPDATED_BY_USER_ID, $updatedByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedByUserId['max'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_UPDATED_BY_USER_ID, $updatedByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_UPDATED_BY_USER_ID, $updatedByUserId, $comparison);
    }

    /**
     * Filter the query on the prissu_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE prissu_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE prissu_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE prissu_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the prissu_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE prissu_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE prissu_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE prissu_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\ProjectBundle\Model\Project object
     *
     * @param \IiMedias\ProjectBundle\Model\Project|ObjectCollection $project The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByProject($project, $comparison = null)
    {
        if ($project instanceof \IiMedias\ProjectBundle\Model\Project) {
            return $this
                ->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_PRPROJ_ID, $project->getId(), $comparison);
        } elseif ($project instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_PRPROJ_ID, $project->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByProject() only accepts arguments of type \IiMedias\ProjectBundle\Model\Project or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Project relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function joinProject($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Project');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Project');
        }

        return $this;
    }

    /**
     * Use the Project relation Project object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\ProjectBundle\Model\ProjectQuery A secondary query class using the current class as primary query
     */
    public function useProjectQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProject($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Project', '\IiMedias\ProjectBundle\Model\ProjectQuery');
    }

    /**
     * Filter the query by a related \IiMedias\ProjectBundle\Model\ProjectIssue object
     *
     * @param \IiMedias\ProjectBundle\Model\ProjectIssue|ObjectCollection $projectIssue The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByProjectIssueParent($projectIssue, $comparison = null)
    {
        if ($projectIssue instanceof \IiMedias\ProjectBundle\Model\ProjectIssue) {
            return $this
                ->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_PARENT_ID, $projectIssue->getId(), $comparison);
        } elseif ($projectIssue instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_PARENT_ID, $projectIssue->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByProjectIssueParent() only accepts arguments of type \IiMedias\ProjectBundle\Model\ProjectIssue or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProjectIssueParent relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function joinProjectIssueParent($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProjectIssueParent');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProjectIssueParent');
        }

        return $this;
    }

    /**
     * Use the ProjectIssueParent relation ProjectIssue object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\ProjectBundle\Model\ProjectIssueQuery A secondary query class using the current class as primary query
     */
    public function useProjectIssueParentQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinProjectIssueParent($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProjectIssueParent', '\IiMedias\ProjectBundle\Model\ProjectIssueQuery');
    }

    /**
     * Filter the query by a related \IiMedias\ProjectBundle\Model\ProjectIssueType object
     *
     * @param \IiMedias\ProjectBundle\Model\ProjectIssueType|ObjectCollection $projectIssueType The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByProjectIssueType($projectIssueType, $comparison = null)
    {
        if ($projectIssueType instanceof \IiMedias\ProjectBundle\Model\ProjectIssueType) {
            return $this
                ->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_PRISTY_ID, $projectIssueType->getId(), $comparison);
        } elseif ($projectIssueType instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_PRISTY_ID, $projectIssueType->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByProjectIssueType() only accepts arguments of type \IiMedias\ProjectBundle\Model\ProjectIssueType or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProjectIssueType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function joinProjectIssueType($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProjectIssueType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProjectIssueType');
        }

        return $this;
    }

    /**
     * Use the ProjectIssueType relation ProjectIssueType object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\ProjectBundle\Model\ProjectIssueTypeQuery A secondary query class using the current class as primary query
     */
    public function useProjectIssueTypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProjectIssueType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProjectIssueType', '\IiMedias\ProjectBundle\Model\ProjectIssueTypeQuery');
    }

    /**
     * Filter the query by a related \IiMedias\ProjectBundle\Model\ProjectIssueStatus object
     *
     * @param \IiMedias\ProjectBundle\Model\ProjectIssueStatus|ObjectCollection $projectIssueStatus The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByProjectIssueStatus($projectIssueStatus, $comparison = null)
    {
        if ($projectIssueStatus instanceof \IiMedias\ProjectBundle\Model\ProjectIssueStatus) {
            return $this
                ->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_PRISST_ID, $projectIssueStatus->getId(), $comparison);
        } elseif ($projectIssueStatus instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_PRISST_ID, $projectIssueStatus->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByProjectIssueStatus() only accepts arguments of type \IiMedias\ProjectBundle\Model\ProjectIssueStatus or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProjectIssueStatus relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function joinProjectIssueStatus($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProjectIssueStatus');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProjectIssueStatus');
        }

        return $this;
    }

    /**
     * Use the ProjectIssueStatus relation ProjectIssueStatus object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\ProjectBundle\Model\ProjectIssueStatusQuery A secondary query class using the current class as primary query
     */
    public function useProjectIssueStatusQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProjectIssueStatus($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProjectIssueStatus', '\IiMedias\ProjectBundle\Model\ProjectIssueStatusQuery');
    }

    /**
     * Filter the query by a related \IiMedias\ProjectBundle\Model\ProjectIssuePriority object
     *
     * @param \IiMedias\ProjectBundle\Model\ProjectIssuePriority|ObjectCollection $projectIssuePriority The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByProjectIssuePriority($projectIssuePriority, $comparison = null)
    {
        if ($projectIssuePriority instanceof \IiMedias\ProjectBundle\Model\ProjectIssuePriority) {
            return $this
                ->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_PRISPR_ID, $projectIssuePriority->getId(), $comparison);
        } elseif ($projectIssuePriority instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_PRISPR_ID, $projectIssuePriority->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByProjectIssuePriority() only accepts arguments of type \IiMedias\ProjectBundle\Model\ProjectIssuePriority or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProjectIssuePriority relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function joinProjectIssuePriority($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProjectIssuePriority');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProjectIssuePriority');
        }

        return $this;
    }

    /**
     * Use the ProjectIssuePriority relation ProjectIssuePriority object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\ProjectBundle\Model\ProjectIssuePriorityQuery A secondary query class using the current class as primary query
     */
    public function useProjectIssuePriorityQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProjectIssuePriority($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProjectIssuePriority', '\IiMedias\ProjectBundle\Model\ProjectIssuePriorityQuery');
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\User object
     *
     * @param \IiMedias\AdminBundle\Model\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByAssignedUser($user, $comparison = null)
    {
        if ($user instanceof \IiMedias\AdminBundle\Model\User) {
            return $this
                ->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_ASSIGNED_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_ASSIGNED_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByAssignedUser() only accepts arguments of type \IiMedias\AdminBundle\Model\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the AssignedUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function joinAssignedUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('AssignedUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'AssignedUser');
        }

        return $this;
    }

    /**
     * Use the AssignedUser relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useAssignedUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinAssignedUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'AssignedUser', '\IiMedias\AdminBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\User object
     *
     * @param \IiMedias\AdminBundle\Model\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByCreatedByUser($user, $comparison = null)
    {
        if ($user instanceof \IiMedias\AdminBundle\Model\User) {
            return $this
                ->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_CREATED_BY_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_CREATED_BY_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCreatedByUser() only accepts arguments of type \IiMedias\AdminBundle\Model\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CreatedByUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function joinCreatedByUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CreatedByUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CreatedByUser');
        }

        return $this;
    }

    /**
     * Use the CreatedByUser relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useCreatedByUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCreatedByUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CreatedByUser', '\IiMedias\AdminBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\User object
     *
     * @param \IiMedias\AdminBundle\Model\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUser($user, $comparison = null)
    {
        if ($user instanceof \IiMedias\AdminBundle\Model\User) {
            return $this
                ->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_UPDATED_BY_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_UPDATED_BY_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUpdatedByUser() only accepts arguments of type \IiMedias\AdminBundle\Model\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UpdatedByUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function joinUpdatedByUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UpdatedByUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UpdatedByUser');
        }

        return $this;
    }

    /**
     * Use the UpdatedByUser relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useUpdatedByUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUpdatedByUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UpdatedByUser', '\IiMedias\AdminBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\ProjectBundle\Model\ProjectIssue object
     *
     * @param \IiMedias\ProjectBundle\Model\ProjectIssue|ObjectCollection $projectIssue the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByProjectIssueChild($projectIssue, $comparison = null)
    {
        if ($projectIssue instanceof \IiMedias\ProjectBundle\Model\ProjectIssue) {
            return $this
                ->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_ID, $projectIssue->getParentId(), $comparison);
        } elseif ($projectIssue instanceof ObjectCollection) {
            return $this
                ->useProjectIssueChildQuery()
                ->filterByPrimaryKeys($projectIssue->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProjectIssueChild() only accepts arguments of type \IiMedias\ProjectBundle\Model\ProjectIssue or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProjectIssueChild relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function joinProjectIssueChild($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProjectIssueChild');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProjectIssueChild');
        }

        return $this;
    }

    /**
     * Use the ProjectIssueChild relation ProjectIssue object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\ProjectBundle\Model\ProjectIssueQuery A secondary query class using the current class as primary query
     */
    public function useProjectIssueChildQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinProjectIssueChild($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProjectIssueChild', '\IiMedias\ProjectBundle\Model\ProjectIssueQuery');
    }

    /**
     * Filter the query by a related \IiMedias\ProjectBundle\Model\ProjectIssueComment object
     *
     * @param \IiMedias\ProjectBundle\Model\ProjectIssueComment|ObjectCollection $projectIssueComment the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProjectIssueQuery The current query, for fluid interface
     */
    public function filterByProjectIssueComment($projectIssueComment, $comparison = null)
    {
        if ($projectIssueComment instanceof \IiMedias\ProjectBundle\Model\ProjectIssueComment) {
            return $this
                ->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_ID, $projectIssueComment->getIssueId(), $comparison);
        } elseif ($projectIssueComment instanceof ObjectCollection) {
            return $this
                ->useProjectIssueCommentQuery()
                ->filterByPrimaryKeys($projectIssueComment->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProjectIssueComment() only accepts arguments of type \IiMedias\ProjectBundle\Model\ProjectIssueComment or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProjectIssueComment relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function joinProjectIssueComment($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProjectIssueComment');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProjectIssueComment');
        }

        return $this;
    }

    /**
     * Use the ProjectIssueComment relation ProjectIssueComment object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\ProjectBundle\Model\ProjectIssueCommentQuery A secondary query class using the current class as primary query
     */
    public function useProjectIssueCommentQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProjectIssueComment($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProjectIssueComment', '\IiMedias\ProjectBundle\Model\ProjectIssueCommentQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildProjectIssue $projectIssue Object to remove from the list of results
     *
     * @return $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function prune($projectIssue = null)
    {
        if ($projectIssue) {
            $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_ID, $projectIssue->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the project_issue_prissu table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProjectIssueTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ProjectIssueTableMap::clearInstancePool();
            ProjectIssueTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProjectIssueTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ProjectIssueTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ProjectIssueTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ProjectIssueTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(ProjectIssueTableMap::COL_PRISSU_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(ProjectIssueTableMap::COL_PRISSU_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(ProjectIssueTableMap::COL_PRISSU_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(ProjectIssueTableMap::COL_PRISSU_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildProjectIssueQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(ProjectIssueTableMap::COL_PRISSU_CREATED_AT);
    }

} // ProjectIssueQuery

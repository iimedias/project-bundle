<?php

namespace IiMedias\ProjectBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\AdminBundle\Model\User;
use IiMedias\ProjectBundle\Model\ProjectIssueComment as ChildProjectIssueComment;
use IiMedias\ProjectBundle\Model\ProjectIssueCommentQuery as ChildProjectIssueCommentQuery;
use IiMedias\ProjectBundle\Model\Map\ProjectIssueCommentTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'project_issue_comment_prisco' table.
 *
 *
 *
 * @method     ChildProjectIssueCommentQuery orderById($order = Criteria::ASC) Order by the prisco_id column
 * @method     ChildProjectIssueCommentQuery orderByIssueId($order = Criteria::ASC) Order by the prisco_prissu_id column
 * @method     ChildProjectIssueCommentQuery orderByComment($order = Criteria::ASC) Order by the prisco_comment column
 * @method     ChildProjectIssueCommentQuery orderByCreatedByUserId($order = Criteria::ASC) Order by the prisco_created_by_user_id column
 * @method     ChildProjectIssueCommentQuery orderByUpdatedByUserId($order = Criteria::ASC) Order by the prisco_updated_by_user_id column
 * @method     ChildProjectIssueCommentQuery orderByCreatedAt($order = Criteria::ASC) Order by the prisco_created_at column
 * @method     ChildProjectIssueCommentQuery orderByUpdatedAt($order = Criteria::ASC) Order by the prisco_updated_at column
 *
 * @method     ChildProjectIssueCommentQuery groupById() Group by the prisco_id column
 * @method     ChildProjectIssueCommentQuery groupByIssueId() Group by the prisco_prissu_id column
 * @method     ChildProjectIssueCommentQuery groupByComment() Group by the prisco_comment column
 * @method     ChildProjectIssueCommentQuery groupByCreatedByUserId() Group by the prisco_created_by_user_id column
 * @method     ChildProjectIssueCommentQuery groupByUpdatedByUserId() Group by the prisco_updated_by_user_id column
 * @method     ChildProjectIssueCommentQuery groupByCreatedAt() Group by the prisco_created_at column
 * @method     ChildProjectIssueCommentQuery groupByUpdatedAt() Group by the prisco_updated_at column
 *
 * @method     ChildProjectIssueCommentQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildProjectIssueCommentQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildProjectIssueCommentQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildProjectIssueCommentQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildProjectIssueCommentQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildProjectIssueCommentQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildProjectIssueCommentQuery leftJoinProjectIssue($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProjectIssue relation
 * @method     ChildProjectIssueCommentQuery rightJoinProjectIssue($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProjectIssue relation
 * @method     ChildProjectIssueCommentQuery innerJoinProjectIssue($relationAlias = null) Adds a INNER JOIN clause to the query using the ProjectIssue relation
 *
 * @method     ChildProjectIssueCommentQuery joinWithProjectIssue($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProjectIssue relation
 *
 * @method     ChildProjectIssueCommentQuery leftJoinWithProjectIssue() Adds a LEFT JOIN clause and with to the query using the ProjectIssue relation
 * @method     ChildProjectIssueCommentQuery rightJoinWithProjectIssue() Adds a RIGHT JOIN clause and with to the query using the ProjectIssue relation
 * @method     ChildProjectIssueCommentQuery innerJoinWithProjectIssue() Adds a INNER JOIN clause and with to the query using the ProjectIssue relation
 *
 * @method     ChildProjectIssueCommentQuery leftJoinCreatedByUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the CreatedByUser relation
 * @method     ChildProjectIssueCommentQuery rightJoinCreatedByUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CreatedByUser relation
 * @method     ChildProjectIssueCommentQuery innerJoinCreatedByUser($relationAlias = null) Adds a INNER JOIN clause to the query using the CreatedByUser relation
 *
 * @method     ChildProjectIssueCommentQuery joinWithCreatedByUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CreatedByUser relation
 *
 * @method     ChildProjectIssueCommentQuery leftJoinWithCreatedByUser() Adds a LEFT JOIN clause and with to the query using the CreatedByUser relation
 * @method     ChildProjectIssueCommentQuery rightJoinWithCreatedByUser() Adds a RIGHT JOIN clause and with to the query using the CreatedByUser relation
 * @method     ChildProjectIssueCommentQuery innerJoinWithCreatedByUser() Adds a INNER JOIN clause and with to the query using the CreatedByUser relation
 *
 * @method     ChildProjectIssueCommentQuery leftJoinUpdatedByUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the UpdatedByUser relation
 * @method     ChildProjectIssueCommentQuery rightJoinUpdatedByUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UpdatedByUser relation
 * @method     ChildProjectIssueCommentQuery innerJoinUpdatedByUser($relationAlias = null) Adds a INNER JOIN clause to the query using the UpdatedByUser relation
 *
 * @method     ChildProjectIssueCommentQuery joinWithUpdatedByUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UpdatedByUser relation
 *
 * @method     ChildProjectIssueCommentQuery leftJoinWithUpdatedByUser() Adds a LEFT JOIN clause and with to the query using the UpdatedByUser relation
 * @method     ChildProjectIssueCommentQuery rightJoinWithUpdatedByUser() Adds a RIGHT JOIN clause and with to the query using the UpdatedByUser relation
 * @method     ChildProjectIssueCommentQuery innerJoinWithUpdatedByUser() Adds a INNER JOIN clause and with to the query using the UpdatedByUser relation
 *
 * @method     \IiMedias\ProjectBundle\Model\ProjectIssueQuery|\IiMedias\AdminBundle\Model\UserQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildProjectIssueComment findOne(ConnectionInterface $con = null) Return the first ChildProjectIssueComment matching the query
 * @method     ChildProjectIssueComment findOneOrCreate(ConnectionInterface $con = null) Return the first ChildProjectIssueComment matching the query, or a new ChildProjectIssueComment object populated from the query conditions when no match is found
 *
 * @method     ChildProjectIssueComment findOneById(int $prisco_id) Return the first ChildProjectIssueComment filtered by the prisco_id column
 * @method     ChildProjectIssueComment findOneByIssueId(int $prisco_prissu_id) Return the first ChildProjectIssueComment filtered by the prisco_prissu_id column
 * @method     ChildProjectIssueComment findOneByComment(string $prisco_comment) Return the first ChildProjectIssueComment filtered by the prisco_comment column
 * @method     ChildProjectIssueComment findOneByCreatedByUserId(int $prisco_created_by_user_id) Return the first ChildProjectIssueComment filtered by the prisco_created_by_user_id column
 * @method     ChildProjectIssueComment findOneByUpdatedByUserId(int $prisco_updated_by_user_id) Return the first ChildProjectIssueComment filtered by the prisco_updated_by_user_id column
 * @method     ChildProjectIssueComment findOneByCreatedAt(string $prisco_created_at) Return the first ChildProjectIssueComment filtered by the prisco_created_at column
 * @method     ChildProjectIssueComment findOneByUpdatedAt(string $prisco_updated_at) Return the first ChildProjectIssueComment filtered by the prisco_updated_at column *

 * @method     ChildProjectIssueComment requirePk($key, ConnectionInterface $con = null) Return the ChildProjectIssueComment by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssueComment requireOne(ConnectionInterface $con = null) Return the first ChildProjectIssueComment matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildProjectIssueComment requireOneById(int $prisco_id) Return the first ChildProjectIssueComment filtered by the prisco_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssueComment requireOneByIssueId(int $prisco_prissu_id) Return the first ChildProjectIssueComment filtered by the prisco_prissu_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssueComment requireOneByComment(string $prisco_comment) Return the first ChildProjectIssueComment filtered by the prisco_comment column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssueComment requireOneByCreatedByUserId(int $prisco_created_by_user_id) Return the first ChildProjectIssueComment filtered by the prisco_created_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssueComment requireOneByUpdatedByUserId(int $prisco_updated_by_user_id) Return the first ChildProjectIssueComment filtered by the prisco_updated_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssueComment requireOneByCreatedAt(string $prisco_created_at) Return the first ChildProjectIssueComment filtered by the prisco_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssueComment requireOneByUpdatedAt(string $prisco_updated_at) Return the first ChildProjectIssueComment filtered by the prisco_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildProjectIssueComment[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildProjectIssueComment objects based on current ModelCriteria
 * @method     ChildProjectIssueComment[]|ObjectCollection findById(int $prisco_id) Return ChildProjectIssueComment objects filtered by the prisco_id column
 * @method     ChildProjectIssueComment[]|ObjectCollection findByIssueId(int $prisco_prissu_id) Return ChildProjectIssueComment objects filtered by the prisco_prissu_id column
 * @method     ChildProjectIssueComment[]|ObjectCollection findByComment(string $prisco_comment) Return ChildProjectIssueComment objects filtered by the prisco_comment column
 * @method     ChildProjectIssueComment[]|ObjectCollection findByCreatedByUserId(int $prisco_created_by_user_id) Return ChildProjectIssueComment objects filtered by the prisco_created_by_user_id column
 * @method     ChildProjectIssueComment[]|ObjectCollection findByUpdatedByUserId(int $prisco_updated_by_user_id) Return ChildProjectIssueComment objects filtered by the prisco_updated_by_user_id column
 * @method     ChildProjectIssueComment[]|ObjectCollection findByCreatedAt(string $prisco_created_at) Return ChildProjectIssueComment objects filtered by the prisco_created_at column
 * @method     ChildProjectIssueComment[]|ObjectCollection findByUpdatedAt(string $prisco_updated_at) Return ChildProjectIssueComment objects filtered by the prisco_updated_at column
 * @method     ChildProjectIssueComment[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ProjectIssueCommentQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\ProjectBundle\Model\Base\ProjectIssueCommentQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\ProjectBundle\\Model\\ProjectIssueComment', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildProjectIssueCommentQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildProjectIssueCommentQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildProjectIssueCommentQuery) {
            return $criteria;
        }
        $query = new ChildProjectIssueCommentQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildProjectIssueComment|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ProjectIssueCommentTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ProjectIssueCommentTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProjectIssueComment A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT prisco_id, prisco_prissu_id, prisco_comment, prisco_created_by_user_id, prisco_updated_by_user_id, prisco_created_at, prisco_updated_at FROM project_issue_comment_prisco WHERE prisco_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildProjectIssueComment $obj */
            $obj = new ChildProjectIssueComment();
            $obj->hydrate($row);
            ProjectIssueCommentTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildProjectIssueComment|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildProjectIssueCommentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildProjectIssueCommentQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the prisco_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE prisco_id = 1234
     * $query->filterById(array(12, 34)); // WHERE prisco_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE prisco_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueCommentQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_ID, $id, $comparison);
    }

    /**
     * Filter the query on the prisco_prissu_id column
     *
     * Example usage:
     * <code>
     * $query->filterByIssueId(1234); // WHERE prisco_prissu_id = 1234
     * $query->filterByIssueId(array(12, 34)); // WHERE prisco_prissu_id IN (12, 34)
     * $query->filterByIssueId(array('min' => 12)); // WHERE prisco_prissu_id > 12
     * </code>
     *
     * @see       filterByProjectIssue()
     *
     * @param     mixed $issueId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueCommentQuery The current query, for fluid interface
     */
    public function filterByIssueId($issueId = null, $comparison = null)
    {
        if (is_array($issueId)) {
            $useMinMax = false;
            if (isset($issueId['min'])) {
                $this->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_PRISSU_ID, $issueId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($issueId['max'])) {
                $this->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_PRISSU_ID, $issueId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_PRISSU_ID, $issueId, $comparison);
    }

    /**
     * Filter the query on the prisco_comment column
     *
     * Example usage:
     * <code>
     * $query->filterByComment('fooValue');   // WHERE prisco_comment = 'fooValue'
     * $query->filterByComment('%fooValue%'); // WHERE prisco_comment LIKE '%fooValue%'
     * </code>
     *
     * @param     string $comment The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueCommentQuery The current query, for fluid interface
     */
    public function filterByComment($comment = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($comment)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_COMMENT, $comment, $comparison);
    }

    /**
     * Filter the query on the prisco_created_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedByUserId(1234); // WHERE prisco_created_by_user_id = 1234
     * $query->filterByCreatedByUserId(array(12, 34)); // WHERE prisco_created_by_user_id IN (12, 34)
     * $query->filterByCreatedByUserId(array('min' => 12)); // WHERE prisco_created_by_user_id > 12
     * </code>
     *
     * @see       filterByCreatedByUser()
     *
     * @param     mixed $createdByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueCommentQuery The current query, for fluid interface
     */
    public function filterByCreatedByUserId($createdByUserId = null, $comparison = null)
    {
        if (is_array($createdByUserId)) {
            $useMinMax = false;
            if (isset($createdByUserId['min'])) {
                $this->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_CREATED_BY_USER_ID, $createdByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdByUserId['max'])) {
                $this->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_CREATED_BY_USER_ID, $createdByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_CREATED_BY_USER_ID, $createdByUserId, $comparison);
    }

    /**
     * Filter the query on the prisco_updated_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedByUserId(1234); // WHERE prisco_updated_by_user_id = 1234
     * $query->filterByUpdatedByUserId(array(12, 34)); // WHERE prisco_updated_by_user_id IN (12, 34)
     * $query->filterByUpdatedByUserId(array('min' => 12)); // WHERE prisco_updated_by_user_id > 12
     * </code>
     *
     * @see       filterByUpdatedByUser()
     *
     * @param     mixed $updatedByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueCommentQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUserId($updatedByUserId = null, $comparison = null)
    {
        if (is_array($updatedByUserId)) {
            $useMinMax = false;
            if (isset($updatedByUserId['min'])) {
                $this->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_UPDATED_BY_USER_ID, $updatedByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedByUserId['max'])) {
                $this->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_UPDATED_BY_USER_ID, $updatedByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_UPDATED_BY_USER_ID, $updatedByUserId, $comparison);
    }

    /**
     * Filter the query on the prisco_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE prisco_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE prisco_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE prisco_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueCommentQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the prisco_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE prisco_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE prisco_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE prisco_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueCommentQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\ProjectBundle\Model\ProjectIssue object
     *
     * @param \IiMedias\ProjectBundle\Model\ProjectIssue|ObjectCollection $projectIssue The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProjectIssueCommentQuery The current query, for fluid interface
     */
    public function filterByProjectIssue($projectIssue, $comparison = null)
    {
        if ($projectIssue instanceof \IiMedias\ProjectBundle\Model\ProjectIssue) {
            return $this
                ->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_PRISSU_ID, $projectIssue->getId(), $comparison);
        } elseif ($projectIssue instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_PRISSU_ID, $projectIssue->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByProjectIssue() only accepts arguments of type \IiMedias\ProjectBundle\Model\ProjectIssue or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProjectIssue relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProjectIssueCommentQuery The current query, for fluid interface
     */
    public function joinProjectIssue($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProjectIssue');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProjectIssue');
        }

        return $this;
    }

    /**
     * Use the ProjectIssue relation ProjectIssue object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\ProjectBundle\Model\ProjectIssueQuery A secondary query class using the current class as primary query
     */
    public function useProjectIssueQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProjectIssue($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProjectIssue', '\IiMedias\ProjectBundle\Model\ProjectIssueQuery');
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\User object
     *
     * @param \IiMedias\AdminBundle\Model\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProjectIssueCommentQuery The current query, for fluid interface
     */
    public function filterByCreatedByUser($user, $comparison = null)
    {
        if ($user instanceof \IiMedias\AdminBundle\Model\User) {
            return $this
                ->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_CREATED_BY_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_CREATED_BY_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCreatedByUser() only accepts arguments of type \IiMedias\AdminBundle\Model\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CreatedByUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProjectIssueCommentQuery The current query, for fluid interface
     */
    public function joinCreatedByUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CreatedByUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CreatedByUser');
        }

        return $this;
    }

    /**
     * Use the CreatedByUser relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useCreatedByUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCreatedByUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CreatedByUser', '\IiMedias\AdminBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\User object
     *
     * @param \IiMedias\AdminBundle\Model\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProjectIssueCommentQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUser($user, $comparison = null)
    {
        if ($user instanceof \IiMedias\AdminBundle\Model\User) {
            return $this
                ->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_UPDATED_BY_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_UPDATED_BY_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUpdatedByUser() only accepts arguments of type \IiMedias\AdminBundle\Model\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UpdatedByUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProjectIssueCommentQuery The current query, for fluid interface
     */
    public function joinUpdatedByUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UpdatedByUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UpdatedByUser');
        }

        return $this;
    }

    /**
     * Use the UpdatedByUser relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useUpdatedByUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUpdatedByUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UpdatedByUser', '\IiMedias\AdminBundle\Model\UserQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildProjectIssueComment $projectIssueComment Object to remove from the list of results
     *
     * @return $this|ChildProjectIssueCommentQuery The current query, for fluid interface
     */
    public function prune($projectIssueComment = null)
    {
        if ($projectIssueComment) {
            $this->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_ID, $projectIssueComment->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the project_issue_comment_prisco table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProjectIssueCommentTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ProjectIssueCommentTableMap::clearInstancePool();
            ProjectIssueCommentTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProjectIssueCommentTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ProjectIssueCommentTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ProjectIssueCommentTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ProjectIssueCommentTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildProjectIssueCommentQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildProjectIssueCommentQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(ProjectIssueCommentTableMap::COL_PRISCO_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildProjectIssueCommentQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(ProjectIssueCommentTableMap::COL_PRISCO_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildProjectIssueCommentQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(ProjectIssueCommentTableMap::COL_PRISCO_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildProjectIssueCommentQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(ProjectIssueCommentTableMap::COL_PRISCO_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildProjectIssueCommentQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(ProjectIssueCommentTableMap::COL_PRISCO_CREATED_AT);
    }

} // ProjectIssueCommentQuery

<?php

namespace IiMedias\ProjectBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\AdminBundle\Model\User;
use IiMedias\ProjectBundle\Model\ProjectMember as ChildProjectMember;
use IiMedias\ProjectBundle\Model\ProjectMemberQuery as ChildProjectMemberQuery;
use IiMedias\ProjectBundle\Model\Map\ProjectMemberTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'project_member_prmmbr' table.
 *
 *
 *
 * @method     ChildProjectMemberQuery orderById($order = Criteria::ASC) Order by the prmmbr_id column
 * @method     ChildProjectMemberQuery orderByProjectId($order = Criteria::ASC) Order by the prmmbr_prproj_id column
 * @method     ChildProjectMemberQuery orderByUserId($order = Criteria::ASC) Order by the prmmbr_usrusr_id column
 * @method     ChildProjectMemberQuery orderByIsActive($order = Criteria::ASC) Order by the prmmbr_is_active column
 * @method     ChildProjectMemberQuery orderByCreatedByUserId($order = Criteria::ASC) Order by the prmmbr_created_by_user_id column
 * @method     ChildProjectMemberQuery orderByUpdatedByUserId($order = Criteria::ASC) Order by the prmmbr_updated_by_user_id column
 * @method     ChildProjectMemberQuery orderByCreatedAt($order = Criteria::ASC) Order by the prmmbr_created_at column
 * @method     ChildProjectMemberQuery orderByUpdatedAt($order = Criteria::ASC) Order by the prmmbr_updated_at column
 *
 * @method     ChildProjectMemberQuery groupById() Group by the prmmbr_id column
 * @method     ChildProjectMemberQuery groupByProjectId() Group by the prmmbr_prproj_id column
 * @method     ChildProjectMemberQuery groupByUserId() Group by the prmmbr_usrusr_id column
 * @method     ChildProjectMemberQuery groupByIsActive() Group by the prmmbr_is_active column
 * @method     ChildProjectMemberQuery groupByCreatedByUserId() Group by the prmmbr_created_by_user_id column
 * @method     ChildProjectMemberQuery groupByUpdatedByUserId() Group by the prmmbr_updated_by_user_id column
 * @method     ChildProjectMemberQuery groupByCreatedAt() Group by the prmmbr_created_at column
 * @method     ChildProjectMemberQuery groupByUpdatedAt() Group by the prmmbr_updated_at column
 *
 * @method     ChildProjectMemberQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildProjectMemberQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildProjectMemberQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildProjectMemberQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildProjectMemberQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildProjectMemberQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildProjectMemberQuery leftJoinProject($relationAlias = null) Adds a LEFT JOIN clause to the query using the Project relation
 * @method     ChildProjectMemberQuery rightJoinProject($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Project relation
 * @method     ChildProjectMemberQuery innerJoinProject($relationAlias = null) Adds a INNER JOIN clause to the query using the Project relation
 *
 * @method     ChildProjectMemberQuery joinWithProject($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the Project relation
 *
 * @method     ChildProjectMemberQuery leftJoinWithProject() Adds a LEFT JOIN clause and with to the query using the Project relation
 * @method     ChildProjectMemberQuery rightJoinWithProject() Adds a RIGHT JOIN clause and with to the query using the Project relation
 * @method     ChildProjectMemberQuery innerJoinWithProject() Adds a INNER JOIN clause and with to the query using the Project relation
 *
 * @method     ChildProjectMemberQuery leftJoinUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the User relation
 * @method     ChildProjectMemberQuery rightJoinUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the User relation
 * @method     ChildProjectMemberQuery innerJoinUser($relationAlias = null) Adds a INNER JOIN clause to the query using the User relation
 *
 * @method     ChildProjectMemberQuery joinWithUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the User relation
 *
 * @method     ChildProjectMemberQuery leftJoinWithUser() Adds a LEFT JOIN clause and with to the query using the User relation
 * @method     ChildProjectMemberQuery rightJoinWithUser() Adds a RIGHT JOIN clause and with to the query using the User relation
 * @method     ChildProjectMemberQuery innerJoinWithUser() Adds a INNER JOIN clause and with to the query using the User relation
 *
 * @method     ChildProjectMemberQuery leftJoinCreatedByUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the CreatedByUser relation
 * @method     ChildProjectMemberQuery rightJoinCreatedByUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CreatedByUser relation
 * @method     ChildProjectMemberQuery innerJoinCreatedByUser($relationAlias = null) Adds a INNER JOIN clause to the query using the CreatedByUser relation
 *
 * @method     ChildProjectMemberQuery joinWithCreatedByUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CreatedByUser relation
 *
 * @method     ChildProjectMemberQuery leftJoinWithCreatedByUser() Adds a LEFT JOIN clause and with to the query using the CreatedByUser relation
 * @method     ChildProjectMemberQuery rightJoinWithCreatedByUser() Adds a RIGHT JOIN clause and with to the query using the CreatedByUser relation
 * @method     ChildProjectMemberQuery innerJoinWithCreatedByUser() Adds a INNER JOIN clause and with to the query using the CreatedByUser relation
 *
 * @method     ChildProjectMemberQuery leftJoinUpdatedByUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the UpdatedByUser relation
 * @method     ChildProjectMemberQuery rightJoinUpdatedByUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UpdatedByUser relation
 * @method     ChildProjectMemberQuery innerJoinUpdatedByUser($relationAlias = null) Adds a INNER JOIN clause to the query using the UpdatedByUser relation
 *
 * @method     ChildProjectMemberQuery joinWithUpdatedByUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UpdatedByUser relation
 *
 * @method     ChildProjectMemberQuery leftJoinWithUpdatedByUser() Adds a LEFT JOIN clause and with to the query using the UpdatedByUser relation
 * @method     ChildProjectMemberQuery rightJoinWithUpdatedByUser() Adds a RIGHT JOIN clause and with to the query using the UpdatedByUser relation
 * @method     ChildProjectMemberQuery innerJoinWithUpdatedByUser() Adds a INNER JOIN clause and with to the query using the UpdatedByUser relation
 *
 * @method     \IiMedias\ProjectBundle\Model\ProjectQuery|\IiMedias\AdminBundle\Model\UserQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildProjectMember findOne(ConnectionInterface $con = null) Return the first ChildProjectMember matching the query
 * @method     ChildProjectMember findOneOrCreate(ConnectionInterface $con = null) Return the first ChildProjectMember matching the query, or a new ChildProjectMember object populated from the query conditions when no match is found
 *
 * @method     ChildProjectMember findOneById(int $prmmbr_id) Return the first ChildProjectMember filtered by the prmmbr_id column
 * @method     ChildProjectMember findOneByProjectId(int $prmmbr_prproj_id) Return the first ChildProjectMember filtered by the prmmbr_prproj_id column
 * @method     ChildProjectMember findOneByUserId(int $prmmbr_usrusr_id) Return the first ChildProjectMember filtered by the prmmbr_usrusr_id column
 * @method     ChildProjectMember findOneByIsActive(boolean $prmmbr_is_active) Return the first ChildProjectMember filtered by the prmmbr_is_active column
 * @method     ChildProjectMember findOneByCreatedByUserId(int $prmmbr_created_by_user_id) Return the first ChildProjectMember filtered by the prmmbr_created_by_user_id column
 * @method     ChildProjectMember findOneByUpdatedByUserId(int $prmmbr_updated_by_user_id) Return the first ChildProjectMember filtered by the prmmbr_updated_by_user_id column
 * @method     ChildProjectMember findOneByCreatedAt(string $prmmbr_created_at) Return the first ChildProjectMember filtered by the prmmbr_created_at column
 * @method     ChildProjectMember findOneByUpdatedAt(string $prmmbr_updated_at) Return the first ChildProjectMember filtered by the prmmbr_updated_at column *

 * @method     ChildProjectMember requirePk($key, ConnectionInterface $con = null) Return the ChildProjectMember by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectMember requireOne(ConnectionInterface $con = null) Return the first ChildProjectMember matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildProjectMember requireOneById(int $prmmbr_id) Return the first ChildProjectMember filtered by the prmmbr_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectMember requireOneByProjectId(int $prmmbr_prproj_id) Return the first ChildProjectMember filtered by the prmmbr_prproj_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectMember requireOneByUserId(int $prmmbr_usrusr_id) Return the first ChildProjectMember filtered by the prmmbr_usrusr_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectMember requireOneByIsActive(boolean $prmmbr_is_active) Return the first ChildProjectMember filtered by the prmmbr_is_active column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectMember requireOneByCreatedByUserId(int $prmmbr_created_by_user_id) Return the first ChildProjectMember filtered by the prmmbr_created_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectMember requireOneByUpdatedByUserId(int $prmmbr_updated_by_user_id) Return the first ChildProjectMember filtered by the prmmbr_updated_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectMember requireOneByCreatedAt(string $prmmbr_created_at) Return the first ChildProjectMember filtered by the prmmbr_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectMember requireOneByUpdatedAt(string $prmmbr_updated_at) Return the first ChildProjectMember filtered by the prmmbr_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildProjectMember[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildProjectMember objects based on current ModelCriteria
 * @method     ChildProjectMember[]|ObjectCollection findById(int $prmmbr_id) Return ChildProjectMember objects filtered by the prmmbr_id column
 * @method     ChildProjectMember[]|ObjectCollection findByProjectId(int $prmmbr_prproj_id) Return ChildProjectMember objects filtered by the prmmbr_prproj_id column
 * @method     ChildProjectMember[]|ObjectCollection findByUserId(int $prmmbr_usrusr_id) Return ChildProjectMember objects filtered by the prmmbr_usrusr_id column
 * @method     ChildProjectMember[]|ObjectCollection findByIsActive(boolean $prmmbr_is_active) Return ChildProjectMember objects filtered by the prmmbr_is_active column
 * @method     ChildProjectMember[]|ObjectCollection findByCreatedByUserId(int $prmmbr_created_by_user_id) Return ChildProjectMember objects filtered by the prmmbr_created_by_user_id column
 * @method     ChildProjectMember[]|ObjectCollection findByUpdatedByUserId(int $prmmbr_updated_by_user_id) Return ChildProjectMember objects filtered by the prmmbr_updated_by_user_id column
 * @method     ChildProjectMember[]|ObjectCollection findByCreatedAt(string $prmmbr_created_at) Return ChildProjectMember objects filtered by the prmmbr_created_at column
 * @method     ChildProjectMember[]|ObjectCollection findByUpdatedAt(string $prmmbr_updated_at) Return ChildProjectMember objects filtered by the prmmbr_updated_at column
 * @method     ChildProjectMember[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ProjectMemberQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\ProjectBundle\Model\Base\ProjectMemberQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\ProjectBundle\\Model\\ProjectMember', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildProjectMemberQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildProjectMemberQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildProjectMemberQuery) {
            return $criteria;
        }
        $query = new ChildProjectMemberQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildProjectMember|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ProjectMemberTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ProjectMemberTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProjectMember A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT prmmbr_id, prmmbr_prproj_id, prmmbr_usrusr_id, prmmbr_is_active, prmmbr_created_by_user_id, prmmbr_updated_by_user_id, prmmbr_created_at, prmmbr_updated_at FROM project_member_prmmbr WHERE prmmbr_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildProjectMember $obj */
            $obj = new ChildProjectMember();
            $obj->hydrate($row);
            ProjectMemberTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildProjectMember|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildProjectMemberQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildProjectMemberQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the prmmbr_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE prmmbr_id = 1234
     * $query->filterById(array(12, 34)); // WHERE prmmbr_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE prmmbr_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectMemberQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_ID, $id, $comparison);
    }

    /**
     * Filter the query on the prmmbr_prproj_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProjectId(1234); // WHERE prmmbr_prproj_id = 1234
     * $query->filterByProjectId(array(12, 34)); // WHERE prmmbr_prproj_id IN (12, 34)
     * $query->filterByProjectId(array('min' => 12)); // WHERE prmmbr_prproj_id > 12
     * </code>
     *
     * @see       filterByProject()
     *
     * @param     mixed $projectId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectMemberQuery The current query, for fluid interface
     */
    public function filterByProjectId($projectId = null, $comparison = null)
    {
        if (is_array($projectId)) {
            $useMinMax = false;
            if (isset($projectId['min'])) {
                $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_PRPROJ_ID, $projectId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($projectId['max'])) {
                $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_PRPROJ_ID, $projectId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_PRPROJ_ID, $projectId, $comparison);
    }

    /**
     * Filter the query on the prmmbr_usrusr_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUserId(1234); // WHERE prmmbr_usrusr_id = 1234
     * $query->filterByUserId(array(12, 34)); // WHERE prmmbr_usrusr_id IN (12, 34)
     * $query->filterByUserId(array('min' => 12)); // WHERE prmmbr_usrusr_id > 12
     * </code>
     *
     * @see       filterByUser()
     *
     * @param     mixed $userId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectMemberQuery The current query, for fluid interface
     */
    public function filterByUserId($userId = null, $comparison = null)
    {
        if (is_array($userId)) {
            $useMinMax = false;
            if (isset($userId['min'])) {
                $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_USRUSR_ID, $userId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($userId['max'])) {
                $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_USRUSR_ID, $userId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_USRUSR_ID, $userId, $comparison);
    }

    /**
     * Filter the query on the prmmbr_is_active column
     *
     * Example usage:
     * <code>
     * $query->filterByIsActive(true); // WHERE prmmbr_is_active = true
     * $query->filterByIsActive('yes'); // WHERE prmmbr_is_active = true
     * </code>
     *
     * @param     boolean|string $isActive The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectMemberQuery The current query, for fluid interface
     */
    public function filterByIsActive($isActive = null, $comparison = null)
    {
        if (is_string($isActive)) {
            $isActive = in_array(strtolower($isActive), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_IS_ACTIVE, $isActive, $comparison);
    }

    /**
     * Filter the query on the prmmbr_created_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedByUserId(1234); // WHERE prmmbr_created_by_user_id = 1234
     * $query->filterByCreatedByUserId(array(12, 34)); // WHERE prmmbr_created_by_user_id IN (12, 34)
     * $query->filterByCreatedByUserId(array('min' => 12)); // WHERE prmmbr_created_by_user_id > 12
     * </code>
     *
     * @see       filterByCreatedByUser()
     *
     * @param     mixed $createdByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectMemberQuery The current query, for fluid interface
     */
    public function filterByCreatedByUserId($createdByUserId = null, $comparison = null)
    {
        if (is_array($createdByUserId)) {
            $useMinMax = false;
            if (isset($createdByUserId['min'])) {
                $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_CREATED_BY_USER_ID, $createdByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdByUserId['max'])) {
                $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_CREATED_BY_USER_ID, $createdByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_CREATED_BY_USER_ID, $createdByUserId, $comparison);
    }

    /**
     * Filter the query on the prmmbr_updated_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedByUserId(1234); // WHERE prmmbr_updated_by_user_id = 1234
     * $query->filterByUpdatedByUserId(array(12, 34)); // WHERE prmmbr_updated_by_user_id IN (12, 34)
     * $query->filterByUpdatedByUserId(array('min' => 12)); // WHERE prmmbr_updated_by_user_id > 12
     * </code>
     *
     * @see       filterByUpdatedByUser()
     *
     * @param     mixed $updatedByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectMemberQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUserId($updatedByUserId = null, $comparison = null)
    {
        if (is_array($updatedByUserId)) {
            $useMinMax = false;
            if (isset($updatedByUserId['min'])) {
                $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_UPDATED_BY_USER_ID, $updatedByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedByUserId['max'])) {
                $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_UPDATED_BY_USER_ID, $updatedByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_UPDATED_BY_USER_ID, $updatedByUserId, $comparison);
    }

    /**
     * Filter the query on the prmmbr_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE prmmbr_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE prmmbr_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE prmmbr_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectMemberQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the prmmbr_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE prmmbr_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE prmmbr_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE prmmbr_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectMemberQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\ProjectBundle\Model\Project object
     *
     * @param \IiMedias\ProjectBundle\Model\Project|ObjectCollection $project The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProjectMemberQuery The current query, for fluid interface
     */
    public function filterByProject($project, $comparison = null)
    {
        if ($project instanceof \IiMedias\ProjectBundle\Model\Project) {
            return $this
                ->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_PRPROJ_ID, $project->getId(), $comparison);
        } elseif ($project instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_PRPROJ_ID, $project->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByProject() only accepts arguments of type \IiMedias\ProjectBundle\Model\Project or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Project relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProjectMemberQuery The current query, for fluid interface
     */
    public function joinProject($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Project');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Project');
        }

        return $this;
    }

    /**
     * Use the Project relation Project object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\ProjectBundle\Model\ProjectQuery A secondary query class using the current class as primary query
     */
    public function useProjectQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProject($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Project', '\IiMedias\ProjectBundle\Model\ProjectQuery');
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\User object
     *
     * @param \IiMedias\AdminBundle\Model\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProjectMemberQuery The current query, for fluid interface
     */
    public function filterByUser($user, $comparison = null)
    {
        if ($user instanceof \IiMedias\AdminBundle\Model\User) {
            return $this
                ->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_USRUSR_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_USRUSR_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUser() only accepts arguments of type \IiMedias\AdminBundle\Model\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the User relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProjectMemberQuery The current query, for fluid interface
     */
    public function joinUser($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('User');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'User');
        }

        return $this;
    }

    /**
     * Use the User relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useUserQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'User', '\IiMedias\AdminBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\User object
     *
     * @param \IiMedias\AdminBundle\Model\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProjectMemberQuery The current query, for fluid interface
     */
    public function filterByCreatedByUser($user, $comparison = null)
    {
        if ($user instanceof \IiMedias\AdminBundle\Model\User) {
            return $this
                ->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_CREATED_BY_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_CREATED_BY_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCreatedByUser() only accepts arguments of type \IiMedias\AdminBundle\Model\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CreatedByUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProjectMemberQuery The current query, for fluid interface
     */
    public function joinCreatedByUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CreatedByUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CreatedByUser');
        }

        return $this;
    }

    /**
     * Use the CreatedByUser relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useCreatedByUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCreatedByUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CreatedByUser', '\IiMedias\AdminBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\User object
     *
     * @param \IiMedias\AdminBundle\Model\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProjectMemberQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUser($user, $comparison = null)
    {
        if ($user instanceof \IiMedias\AdminBundle\Model\User) {
            return $this
                ->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_UPDATED_BY_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_UPDATED_BY_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUpdatedByUser() only accepts arguments of type \IiMedias\AdminBundle\Model\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UpdatedByUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProjectMemberQuery The current query, for fluid interface
     */
    public function joinUpdatedByUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UpdatedByUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UpdatedByUser');
        }

        return $this;
    }

    /**
     * Use the UpdatedByUser relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useUpdatedByUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUpdatedByUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UpdatedByUser', '\IiMedias\AdminBundle\Model\UserQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildProjectMember $projectMember Object to remove from the list of results
     *
     * @return $this|ChildProjectMemberQuery The current query, for fluid interface
     */
    public function prune($projectMember = null)
    {
        if ($projectMember) {
            $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_ID, $projectMember->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the project_member_prmmbr table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProjectMemberTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ProjectMemberTableMap::clearInstancePool();
            ProjectMemberTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProjectMemberTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ProjectMemberTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ProjectMemberTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ProjectMemberTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildProjectMemberQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildProjectMemberQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(ProjectMemberTableMap::COL_PRMMBR_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildProjectMemberQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(ProjectMemberTableMap::COL_PRMMBR_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildProjectMemberQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(ProjectMemberTableMap::COL_PRMMBR_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildProjectMemberQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(ProjectMemberTableMap::COL_PRMMBR_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildProjectMemberQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(ProjectMemberTableMap::COL_PRMMBR_CREATED_AT);
    }

} // ProjectMemberQuery

<?php

namespace IiMedias\ProjectBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use IiMedias\AdminBundle\Model\User;
use IiMedias\AdminBundle\Model\UserQuery;
use IiMedias\ProjectBundle\Model\Project as ChildProject;
use IiMedias\ProjectBundle\Model\ProjectIssue as ChildProjectIssue;
use IiMedias\ProjectBundle\Model\ProjectIssueComment as ChildProjectIssueComment;
use IiMedias\ProjectBundle\Model\ProjectIssueCommentQuery as ChildProjectIssueCommentQuery;
use IiMedias\ProjectBundle\Model\ProjectIssuePriority as ChildProjectIssuePriority;
use IiMedias\ProjectBundle\Model\ProjectIssuePriorityQuery as ChildProjectIssuePriorityQuery;
use IiMedias\ProjectBundle\Model\ProjectIssueQuery as ChildProjectIssueQuery;
use IiMedias\ProjectBundle\Model\ProjectIssueStatus as ChildProjectIssueStatus;
use IiMedias\ProjectBundle\Model\ProjectIssueStatusQuery as ChildProjectIssueStatusQuery;
use IiMedias\ProjectBundle\Model\ProjectIssueType as ChildProjectIssueType;
use IiMedias\ProjectBundle\Model\ProjectIssueTypeQuery as ChildProjectIssueTypeQuery;
use IiMedias\ProjectBundle\Model\ProjectQuery as ChildProjectQuery;
use IiMedias\ProjectBundle\Model\Map\ProjectIssueCommentTableMap;
use IiMedias\ProjectBundle\Model\Map\ProjectIssueTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'project_issue_prissu' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.ProjectBundle.Model.Base
 */
abstract class ProjectIssue implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\ProjectBundle\\Model\\Map\\ProjectIssueTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the prissu_id field.
     *
     * @var        int
     */
    protected $prissu_id;

    /**
     * The value for the prissu_prproj_id field.
     *
     * @var        int
     */
    protected $prissu_prproj_id;

    /**
     * The value for the prissu_parent_id field.
     *
     * @var        int
     */
    protected $prissu_parent_id;

    /**
     * The value for the prissu_pristy_id field.
     *
     * @var        int
     */
    protected $prissu_pristy_id;

    /**
     * The value for the prissu_subject field.
     *
     * @var        string
     */
    protected $prissu_subject;

    /**
     * The value for the prissu_description field.
     *
     * @var        string
     */
    protected $prissu_description;

    /**
     * The value for the prissu_prisst_id field.
     *
     * @var        int
     */
    protected $prissu_prisst_id;

    /**
     * The value for the prissu_prispr_id field.
     *
     * @var        int
     */
    protected $prissu_prispr_id;

    /**
     * The value for the prissu_assigned_user_id field.
     *
     * @var        int
     */
    protected $prissu_assigned_user_id;

    /**
     * The value for the prissu_start_at field.
     *
     * @var        DateTime
     */
    protected $prissu_start_at;

    /**
     * The value for the prissu_end_at field.
     *
     * @var        DateTime
     */
    protected $prissu_end_at;

    /**
     * The value for the prissu_estimated_time field.
     *
     * @var        double
     */
    protected $prissu_estimated_time;

    /**
     * The value for the prissu_completed_percentage field.
     *
     * @var        double
     */
    protected $prissu_completed_percentage;

    /**
     * The value for the prissu_children_completed_percentage field.
     *
     * @var        double
     */
    protected $prissu_children_completed_percentage;

    /**
     * The value for the prissu_created_by_user_id field.
     *
     * @var        int
     */
    protected $prissu_created_by_user_id;

    /**
     * The value for the prissu_updated_by_user_id field.
     *
     * @var        int
     */
    protected $prissu_updated_by_user_id;

    /**
     * The value for the prissu_created_at field.
     *
     * @var        DateTime
     */
    protected $prissu_created_at;

    /**
     * The value for the prissu_updated_at field.
     *
     * @var        DateTime
     */
    protected $prissu_updated_at;

    /**
     * @var        ChildProject
     */
    protected $aProject;

    /**
     * @var        ChildProjectIssue
     */
    protected $aProjectIssueParent;

    /**
     * @var        ChildProjectIssueType
     */
    protected $aProjectIssueType;

    /**
     * @var        ChildProjectIssueStatus
     */
    protected $aProjectIssueStatus;

    /**
     * @var        ChildProjectIssuePriority
     */
    protected $aProjectIssuePriority;

    /**
     * @var        User
     */
    protected $aAssignedUser;

    /**
     * @var        User
     */
    protected $aCreatedByUser;

    /**
     * @var        User
     */
    protected $aUpdatedByUser;

    /**
     * @var        ObjectCollection|ChildProjectIssue[] Collection to store aggregation of ChildProjectIssue objects.
     */
    protected $collProjectIssuechildren;
    protected $collProjectIssuechildrenPartial;

    /**
     * @var        ObjectCollection|ChildProjectIssueComment[] Collection to store aggregation of ChildProjectIssueComment objects.
     */
    protected $collProjectIssueComments;
    protected $collProjectIssueCommentsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildProjectIssue[]
     */
    protected $projectIssuechildrenScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildProjectIssueComment[]
     */
    protected $projectIssueCommentsScheduledForDeletion = null;

    /**
     * Initializes internal state of IiMedias\ProjectBundle\Model\Base\ProjectIssue object.
     */
    public function __construct()
    {
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>ProjectIssue</code> instance.  If
     * <code>obj</code> is an instance of <code>ProjectIssue</code>, delegates to
     * <code>equals(ProjectIssue)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|ProjectIssue The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [prissu_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->prissu_id;
    }

    /**
     * Get the [prissu_prproj_id] column value.
     *
     * @return int
     */
    public function getProjectId()
    {
        return $this->prissu_prproj_id;
    }

    /**
     * Get the [prissu_parent_id] column value.
     *
     * @return int
     */
    public function getParentId()
    {
        return $this->prissu_parent_id;
    }

    /**
     * Get the [prissu_pristy_id] column value.
     *
     * @return int
     */
    public function getTypeId()
    {
        return $this->prissu_pristy_id;
    }

    /**
     * Get the [prissu_subject] column value.
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->prissu_subject;
    }

    /**
     * Get the [prissu_description] column value.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->prissu_description;
    }

    /**
     * Get the [prissu_prisst_id] column value.
     *
     * @return int
     */
    public function getStatusId()
    {
        return $this->prissu_prisst_id;
    }

    /**
     * Get the [prissu_prispr_id] column value.
     *
     * @return int
     */
    public function getPriorityId()
    {
        return $this->prissu_prispr_id;
    }

    /**
     * Get the [prissu_assigned_user_id] column value.
     *
     * @return int
     */
    public function getAssignedUserId()
    {
        return $this->prissu_assigned_user_id;
    }

    /**
     * Get the [optionally formatted] temporal [prissu_start_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getStartAt($format = NULL)
    {
        if ($format === null) {
            return $this->prissu_start_at;
        } else {
            return $this->prissu_start_at instanceof \DateTimeInterface ? $this->prissu_start_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [prissu_end_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getEndAt($format = NULL)
    {
        if ($format === null) {
            return $this->prissu_end_at;
        } else {
            return $this->prissu_end_at instanceof \DateTimeInterface ? $this->prissu_end_at->format($format) : null;
        }
    }

    /**
     * Get the [prissu_estimated_time] column value.
     *
     * @return double
     */
    public function getEstimatedTime()
    {
        return $this->prissu_estimated_time;
    }

    /**
     * Get the [prissu_completed_percentage] column value.
     *
     * @return double
     */
    public function getCompletedPercentage()
    {
        return $this->prissu_completed_percentage;
    }

    /**
     * Get the [prissu_children_completed_percentage] column value.
     *
     * @return double
     */
    public function getChildrenCompletedPercentage()
    {
        return $this->prissu_children_completed_percentage;
    }

    /**
     * Get the [prissu_created_by_user_id] column value.
     *
     * @return int
     */
    public function getCreatedByUserId()
    {
        return $this->prissu_created_by_user_id;
    }

    /**
     * Get the [prissu_updated_by_user_id] column value.
     *
     * @return int
     */
    public function getUpdatedByUserId()
    {
        return $this->prissu_updated_by_user_id;
    }

    /**
     * Get the [optionally formatted] temporal [prissu_created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->prissu_created_at;
        } else {
            return $this->prissu_created_at instanceof \DateTimeInterface ? $this->prissu_created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [prissu_updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->prissu_updated_at;
        } else {
            return $this->prissu_updated_at instanceof \DateTimeInterface ? $this->prissu_updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [prissu_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->prissu_id !== $v) {
            $this->prissu_id = $v;
            $this->modifiedColumns[ProjectIssueTableMap::COL_PRISSU_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [prissu_prproj_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     */
    public function setProjectId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->prissu_prproj_id !== $v) {
            $this->prissu_prproj_id = $v;
            $this->modifiedColumns[ProjectIssueTableMap::COL_PRISSU_PRPROJ_ID] = true;
        }

        if ($this->aProject !== null && $this->aProject->getId() !== $v) {
            $this->aProject = null;
        }

        return $this;
    } // setProjectId()

    /**
     * Set the value of [prissu_parent_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     */
    public function setParentId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->prissu_parent_id !== $v) {
            $this->prissu_parent_id = $v;
            $this->modifiedColumns[ProjectIssueTableMap::COL_PRISSU_PARENT_ID] = true;
        }

        if ($this->aProjectIssueParent !== null && $this->aProjectIssueParent->getId() !== $v) {
            $this->aProjectIssueParent = null;
        }

        return $this;
    } // setParentId()

    /**
     * Set the value of [prissu_pristy_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     */
    public function setTypeId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->prissu_pristy_id !== $v) {
            $this->prissu_pristy_id = $v;
            $this->modifiedColumns[ProjectIssueTableMap::COL_PRISSU_PRISTY_ID] = true;
        }

        if ($this->aProjectIssueType !== null && $this->aProjectIssueType->getId() !== $v) {
            $this->aProjectIssueType = null;
        }

        return $this;
    } // setTypeId()

    /**
     * Set the value of [prissu_subject] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     */
    public function setSubject($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->prissu_subject !== $v) {
            $this->prissu_subject = $v;
            $this->modifiedColumns[ProjectIssueTableMap::COL_PRISSU_SUBJECT] = true;
        }

        return $this;
    } // setSubject()

    /**
     * Set the value of [prissu_description] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->prissu_description !== $v) {
            $this->prissu_description = $v;
            $this->modifiedColumns[ProjectIssueTableMap::COL_PRISSU_DESCRIPTION] = true;
        }

        return $this;
    } // setDescription()

    /**
     * Set the value of [prissu_prisst_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     */
    public function setStatusId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->prissu_prisst_id !== $v) {
            $this->prissu_prisst_id = $v;
            $this->modifiedColumns[ProjectIssueTableMap::COL_PRISSU_PRISST_ID] = true;
        }

        if ($this->aProjectIssueStatus !== null && $this->aProjectIssueStatus->getId() !== $v) {
            $this->aProjectIssueStatus = null;
        }

        return $this;
    } // setStatusId()

    /**
     * Set the value of [prissu_prispr_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     */
    public function setPriorityId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->prissu_prispr_id !== $v) {
            $this->prissu_prispr_id = $v;
            $this->modifiedColumns[ProjectIssueTableMap::COL_PRISSU_PRISPR_ID] = true;
        }

        if ($this->aProjectIssuePriority !== null && $this->aProjectIssuePriority->getId() !== $v) {
            $this->aProjectIssuePriority = null;
        }

        return $this;
    } // setPriorityId()

    /**
     * Set the value of [prissu_assigned_user_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     */
    public function setAssignedUserId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->prissu_assigned_user_id !== $v) {
            $this->prissu_assigned_user_id = $v;
            $this->modifiedColumns[ProjectIssueTableMap::COL_PRISSU_ASSIGNED_USER_ID] = true;
        }

        if ($this->aAssignedUser !== null && $this->aAssignedUser->getId() !== $v) {
            $this->aAssignedUser = null;
        }

        return $this;
    } // setAssignedUserId()

    /**
     * Sets the value of [prissu_start_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     */
    public function setStartAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->prissu_start_at !== null || $dt !== null) {
            if ($this->prissu_start_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->prissu_start_at->format("Y-m-d H:i:s.u")) {
                $this->prissu_start_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ProjectIssueTableMap::COL_PRISSU_START_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setStartAt()

    /**
     * Sets the value of [prissu_end_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     */
    public function setEndAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->prissu_end_at !== null || $dt !== null) {
            if ($this->prissu_end_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->prissu_end_at->format("Y-m-d H:i:s.u")) {
                $this->prissu_end_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ProjectIssueTableMap::COL_PRISSU_END_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setEndAt()

    /**
     * Set the value of [prissu_estimated_time] column.
     *
     * @param double $v new value
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     */
    public function setEstimatedTime($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->prissu_estimated_time !== $v) {
            $this->prissu_estimated_time = $v;
            $this->modifiedColumns[ProjectIssueTableMap::COL_PRISSU_ESTIMATED_TIME] = true;
        }

        return $this;
    } // setEstimatedTime()

    /**
     * Set the value of [prissu_completed_percentage] column.
     *
     * @param double $v new value
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     */
    public function setCompletedPercentage($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->prissu_completed_percentage !== $v) {
            $this->prissu_completed_percentage = $v;
            $this->modifiedColumns[ProjectIssueTableMap::COL_PRISSU_COMPLETED_PERCENTAGE] = true;
        }

        return $this;
    } // setCompletedPercentage()

    /**
     * Set the value of [prissu_children_completed_percentage] column.
     *
     * @param double $v new value
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     */
    public function setChildrenCompletedPercentage($v)
    {
        if ($v !== null) {
            $v = (double) $v;
        }

        if ($this->prissu_children_completed_percentage !== $v) {
            $this->prissu_children_completed_percentage = $v;
            $this->modifiedColumns[ProjectIssueTableMap::COL_PRISSU_CHILDREN_COMPLETED_PERCENTAGE] = true;
        }

        return $this;
    } // setChildrenCompletedPercentage()

    /**
     * Set the value of [prissu_created_by_user_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     */
    public function setCreatedByUserId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->prissu_created_by_user_id !== $v) {
            $this->prissu_created_by_user_id = $v;
            $this->modifiedColumns[ProjectIssueTableMap::COL_PRISSU_CREATED_BY_USER_ID] = true;
        }

        if ($this->aCreatedByUser !== null && $this->aCreatedByUser->getId() !== $v) {
            $this->aCreatedByUser = null;
        }

        return $this;
    } // setCreatedByUserId()

    /**
     * Set the value of [prissu_updated_by_user_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     */
    public function setUpdatedByUserId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->prissu_updated_by_user_id !== $v) {
            $this->prissu_updated_by_user_id = $v;
            $this->modifiedColumns[ProjectIssueTableMap::COL_PRISSU_UPDATED_BY_USER_ID] = true;
        }

        if ($this->aUpdatedByUser !== null && $this->aUpdatedByUser->getId() !== $v) {
            $this->aUpdatedByUser = null;
        }

        return $this;
    } // setUpdatedByUserId()

    /**
     * Sets the value of [prissu_created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->prissu_created_at !== null || $dt !== null) {
            if ($this->prissu_created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->prissu_created_at->format("Y-m-d H:i:s.u")) {
                $this->prissu_created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ProjectIssueTableMap::COL_PRISSU_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [prissu_updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->prissu_updated_at !== null || $dt !== null) {
            if ($this->prissu_updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->prissu_updated_at->format("Y-m-d H:i:s.u")) {
                $this->prissu_updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ProjectIssueTableMap::COL_PRISSU_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ProjectIssueTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prissu_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ProjectIssueTableMap::translateFieldName('ProjectId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prissu_prproj_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ProjectIssueTableMap::translateFieldName('ParentId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prissu_parent_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ProjectIssueTableMap::translateFieldName('TypeId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prissu_pristy_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ProjectIssueTableMap::translateFieldName('Subject', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prissu_subject = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ProjectIssueTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prissu_description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ProjectIssueTableMap::translateFieldName('StatusId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prissu_prisst_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ProjectIssueTableMap::translateFieldName('PriorityId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prissu_prispr_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ProjectIssueTableMap::translateFieldName('AssignedUserId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prissu_assigned_user_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ProjectIssueTableMap::translateFieldName('StartAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->prissu_start_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 10 + $startcol : ProjectIssueTableMap::translateFieldName('EndAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->prissu_end_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 11 + $startcol : ProjectIssueTableMap::translateFieldName('EstimatedTime', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prissu_estimated_time = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 12 + $startcol : ProjectIssueTableMap::translateFieldName('CompletedPercentage', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prissu_completed_percentage = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 13 + $startcol : ProjectIssueTableMap::translateFieldName('ChildrenCompletedPercentage', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prissu_children_completed_percentage = (null !== $col) ? (double) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 14 + $startcol : ProjectIssueTableMap::translateFieldName('CreatedByUserId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prissu_created_by_user_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 15 + $startcol : ProjectIssueTableMap::translateFieldName('UpdatedByUserId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prissu_updated_by_user_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 16 + $startcol : ProjectIssueTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->prissu_created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 17 + $startcol : ProjectIssueTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->prissu_updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 18; // 18 = ProjectIssueTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\ProjectBundle\\Model\\ProjectIssue'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aProject !== null && $this->prissu_prproj_id !== $this->aProject->getId()) {
            $this->aProject = null;
        }
        if ($this->aProjectIssueParent !== null && $this->prissu_parent_id !== $this->aProjectIssueParent->getId()) {
            $this->aProjectIssueParent = null;
        }
        if ($this->aProjectIssueType !== null && $this->prissu_pristy_id !== $this->aProjectIssueType->getId()) {
            $this->aProjectIssueType = null;
        }
        if ($this->aProjectIssueStatus !== null && $this->prissu_prisst_id !== $this->aProjectIssueStatus->getId()) {
            $this->aProjectIssueStatus = null;
        }
        if ($this->aProjectIssuePriority !== null && $this->prissu_prispr_id !== $this->aProjectIssuePriority->getId()) {
            $this->aProjectIssuePriority = null;
        }
        if ($this->aAssignedUser !== null && $this->prissu_assigned_user_id !== $this->aAssignedUser->getId()) {
            $this->aAssignedUser = null;
        }
        if ($this->aCreatedByUser !== null && $this->prissu_created_by_user_id !== $this->aCreatedByUser->getId()) {
            $this->aCreatedByUser = null;
        }
        if ($this->aUpdatedByUser !== null && $this->prissu_updated_by_user_id !== $this->aUpdatedByUser->getId()) {
            $this->aUpdatedByUser = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ProjectIssueTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildProjectIssueQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aProject = null;
            $this->aProjectIssueParent = null;
            $this->aProjectIssueType = null;
            $this->aProjectIssueStatus = null;
            $this->aProjectIssuePriority = null;
            $this->aAssignedUser = null;
            $this->aCreatedByUser = null;
            $this->aUpdatedByUser = null;
            $this->collProjectIssuechildren = null;

            $this->collProjectIssueComments = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see ProjectIssue::setDeleted()
     * @see ProjectIssue::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProjectIssueTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildProjectIssueQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProjectIssueTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_CREATED_AT)) {
                    $this->setCreatedAt(time());
                }
                if (!$this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_UPDATED_AT)) {
                    $this->setUpdatedAt(time());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_UPDATED_AT)) {
                    $this->setUpdatedAt(time());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ProjectIssueTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aProject !== null) {
                if ($this->aProject->isModified() || $this->aProject->isNew()) {
                    $affectedRows += $this->aProject->save($con);
                }
                $this->setProject($this->aProject);
            }

            if ($this->aProjectIssueParent !== null) {
                if ($this->aProjectIssueParent->isModified() || $this->aProjectIssueParent->isNew()) {
                    $affectedRows += $this->aProjectIssueParent->save($con);
                }
                $this->setProjectIssueParent($this->aProjectIssueParent);
            }

            if ($this->aProjectIssueType !== null) {
                if ($this->aProjectIssueType->isModified() || $this->aProjectIssueType->isNew()) {
                    $affectedRows += $this->aProjectIssueType->save($con);
                }
                $this->setProjectIssueType($this->aProjectIssueType);
            }

            if ($this->aProjectIssueStatus !== null) {
                if ($this->aProjectIssueStatus->isModified() || $this->aProjectIssueStatus->isNew()) {
                    $affectedRows += $this->aProjectIssueStatus->save($con);
                }
                $this->setProjectIssueStatus($this->aProjectIssueStatus);
            }

            if ($this->aProjectIssuePriority !== null) {
                if ($this->aProjectIssuePriority->isModified() || $this->aProjectIssuePriority->isNew()) {
                    $affectedRows += $this->aProjectIssuePriority->save($con);
                }
                $this->setProjectIssuePriority($this->aProjectIssuePriority);
            }

            if ($this->aAssignedUser !== null) {
                if ($this->aAssignedUser->isModified() || $this->aAssignedUser->isNew()) {
                    $affectedRows += $this->aAssignedUser->save($con);
                }
                $this->setAssignedUser($this->aAssignedUser);
            }

            if ($this->aCreatedByUser !== null) {
                if ($this->aCreatedByUser->isModified() || $this->aCreatedByUser->isNew()) {
                    $affectedRows += $this->aCreatedByUser->save($con);
                }
                $this->setCreatedByUser($this->aCreatedByUser);
            }

            if ($this->aUpdatedByUser !== null) {
                if ($this->aUpdatedByUser->isModified() || $this->aUpdatedByUser->isNew()) {
                    $affectedRows += $this->aUpdatedByUser->save($con);
                }
                $this->setUpdatedByUser($this->aUpdatedByUser);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->projectIssuechildrenScheduledForDeletion !== null) {
                if (!$this->projectIssuechildrenScheduledForDeletion->isEmpty()) {
                    \IiMedias\ProjectBundle\Model\ProjectIssueQuery::create()
                        ->filterByPrimaryKeys($this->projectIssuechildrenScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->projectIssuechildrenScheduledForDeletion = null;
                }
            }

            if ($this->collProjectIssuechildren !== null) {
                foreach ($this->collProjectIssuechildren as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->projectIssueCommentsScheduledForDeletion !== null) {
                if (!$this->projectIssueCommentsScheduledForDeletion->isEmpty()) {
                    \IiMedias\ProjectBundle\Model\ProjectIssueCommentQuery::create()
                        ->filterByPrimaryKeys($this->projectIssueCommentsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->projectIssueCommentsScheduledForDeletion = null;
                }
            }

            if ($this->collProjectIssueComments !== null) {
                foreach ($this->collProjectIssueComments as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ProjectIssueTableMap::COL_PRISSU_ID] = true;
        if (null !== $this->prissu_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ProjectIssueTableMap::COL_PRISSU_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_ID)) {
            $modifiedColumns[':p' . $index++]  = 'prissu_id';
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_PRPROJ_ID)) {
            $modifiedColumns[':p' . $index++]  = 'prissu_prproj_id';
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_PARENT_ID)) {
            $modifiedColumns[':p' . $index++]  = 'prissu_parent_id';
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_PRISTY_ID)) {
            $modifiedColumns[':p' . $index++]  = 'prissu_pristy_id';
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_SUBJECT)) {
            $modifiedColumns[':p' . $index++]  = 'prissu_subject';
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'prissu_description';
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_PRISST_ID)) {
            $modifiedColumns[':p' . $index++]  = 'prissu_prisst_id';
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_PRISPR_ID)) {
            $modifiedColumns[':p' . $index++]  = 'prissu_prispr_id';
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_ASSIGNED_USER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'prissu_assigned_user_id';
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_START_AT)) {
            $modifiedColumns[':p' . $index++]  = 'prissu_start_at';
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_END_AT)) {
            $modifiedColumns[':p' . $index++]  = 'prissu_end_at';
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_ESTIMATED_TIME)) {
            $modifiedColumns[':p' . $index++]  = 'prissu_estimated_time';
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_COMPLETED_PERCENTAGE)) {
            $modifiedColumns[':p' . $index++]  = 'prissu_completed_percentage';
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_CHILDREN_COMPLETED_PERCENTAGE)) {
            $modifiedColumns[':p' . $index++]  = 'prissu_children_completed_percentage';
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_CREATED_BY_USER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'prissu_created_by_user_id';
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_UPDATED_BY_USER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'prissu_updated_by_user_id';
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'prissu_created_at';
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'prissu_updated_at';
        }

        $sql = sprintf(
            'INSERT INTO project_issue_prissu (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'prissu_id':
                        $stmt->bindValue($identifier, $this->prissu_id, PDO::PARAM_INT);
                        break;
                    case 'prissu_prproj_id':
                        $stmt->bindValue($identifier, $this->prissu_prproj_id, PDO::PARAM_INT);
                        break;
                    case 'prissu_parent_id':
                        $stmt->bindValue($identifier, $this->prissu_parent_id, PDO::PARAM_INT);
                        break;
                    case 'prissu_pristy_id':
                        $stmt->bindValue($identifier, $this->prissu_pristy_id, PDO::PARAM_INT);
                        break;
                    case 'prissu_subject':
                        $stmt->bindValue($identifier, $this->prissu_subject, PDO::PARAM_STR);
                        break;
                    case 'prissu_description':
                        $stmt->bindValue($identifier, $this->prissu_description, PDO::PARAM_STR);
                        break;
                    case 'prissu_prisst_id':
                        $stmt->bindValue($identifier, $this->prissu_prisst_id, PDO::PARAM_INT);
                        break;
                    case 'prissu_prispr_id':
                        $stmt->bindValue($identifier, $this->prissu_prispr_id, PDO::PARAM_INT);
                        break;
                    case 'prissu_assigned_user_id':
                        $stmt->bindValue($identifier, $this->prissu_assigned_user_id, PDO::PARAM_INT);
                        break;
                    case 'prissu_start_at':
                        $stmt->bindValue($identifier, $this->prissu_start_at ? $this->prissu_start_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'prissu_end_at':
                        $stmt->bindValue($identifier, $this->prissu_end_at ? $this->prissu_end_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'prissu_estimated_time':
                        $stmt->bindValue($identifier, $this->prissu_estimated_time, PDO::PARAM_STR);
                        break;
                    case 'prissu_completed_percentage':
                        $stmt->bindValue($identifier, $this->prissu_completed_percentage, PDO::PARAM_STR);
                        break;
                    case 'prissu_children_completed_percentage':
                        $stmt->bindValue($identifier, $this->prissu_children_completed_percentage, PDO::PARAM_STR);
                        break;
                    case 'prissu_created_by_user_id':
                        $stmt->bindValue($identifier, $this->prissu_created_by_user_id, PDO::PARAM_INT);
                        break;
                    case 'prissu_updated_by_user_id':
                        $stmt->bindValue($identifier, $this->prissu_updated_by_user_id, PDO::PARAM_INT);
                        break;
                    case 'prissu_created_at':
                        $stmt->bindValue($identifier, $this->prissu_created_at ? $this->prissu_created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'prissu_updated_at':
                        $stmt->bindValue($identifier, $this->prissu_updated_at ? $this->prissu_updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ProjectIssueTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getProjectId();
                break;
            case 2:
                return $this->getParentId();
                break;
            case 3:
                return $this->getTypeId();
                break;
            case 4:
                return $this->getSubject();
                break;
            case 5:
                return $this->getDescription();
                break;
            case 6:
                return $this->getStatusId();
                break;
            case 7:
                return $this->getPriorityId();
                break;
            case 8:
                return $this->getAssignedUserId();
                break;
            case 9:
                return $this->getStartAt();
                break;
            case 10:
                return $this->getEndAt();
                break;
            case 11:
                return $this->getEstimatedTime();
                break;
            case 12:
                return $this->getCompletedPercentage();
                break;
            case 13:
                return $this->getChildrenCompletedPercentage();
                break;
            case 14:
                return $this->getCreatedByUserId();
                break;
            case 15:
                return $this->getUpdatedByUserId();
                break;
            case 16:
                return $this->getCreatedAt();
                break;
            case 17:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['ProjectIssue'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ProjectIssue'][$this->hashCode()] = true;
        $keys = ProjectIssueTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getProjectId(),
            $keys[2] => $this->getParentId(),
            $keys[3] => $this->getTypeId(),
            $keys[4] => $this->getSubject(),
            $keys[5] => $this->getDescription(),
            $keys[6] => $this->getStatusId(),
            $keys[7] => $this->getPriorityId(),
            $keys[8] => $this->getAssignedUserId(),
            $keys[9] => $this->getStartAt(),
            $keys[10] => $this->getEndAt(),
            $keys[11] => $this->getEstimatedTime(),
            $keys[12] => $this->getCompletedPercentage(),
            $keys[13] => $this->getChildrenCompletedPercentage(),
            $keys[14] => $this->getCreatedByUserId(),
            $keys[15] => $this->getUpdatedByUserId(),
            $keys[16] => $this->getCreatedAt(),
            $keys[17] => $this->getUpdatedAt(),
        );
        if ($result[$keys[9]] instanceof \DateTime) {
            $result[$keys[9]] = $result[$keys[9]]->format('c');
        }

        if ($result[$keys[10]] instanceof \DateTime) {
            $result[$keys[10]] = $result[$keys[10]]->format('c');
        }

        if ($result[$keys[16]] instanceof \DateTime) {
            $result[$keys[16]] = $result[$keys[16]]->format('c');
        }

        if ($result[$keys[17]] instanceof \DateTime) {
            $result[$keys[17]] = $result[$keys[17]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aProject) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'project';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'project_project_prproj';
                        break;
                    default:
                        $key = 'Project';
                }

                $result[$key] = $this->aProject->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aProjectIssueParent) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'projectIssue';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'project_issue_prissu';
                        break;
                    default:
                        $key = 'ProjectIssueParent';
                }

                $result[$key] = $this->aProjectIssueParent->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aProjectIssueType) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'projectIssueType';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'project_issue_type_pristy';
                        break;
                    default:
                        $key = 'ProjectIssueType';
                }

                $result[$key] = $this->aProjectIssueType->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aProjectIssueStatus) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'projectIssueStatus';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'project_issue_status_prisst';
                        break;
                    default:
                        $key = 'ProjectIssueStatus';
                }

                $result[$key] = $this->aProjectIssueStatus->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aProjectIssuePriority) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'projectIssuePriority';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'project_issue_priority_prispr';
                        break;
                    default:
                        $key = 'ProjectIssuePriority';
                }

                $result[$key] = $this->aProjectIssuePriority->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aAssignedUser) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'user';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user_user_usrusr';
                        break;
                    default:
                        $key = 'AssignedUser';
                }

                $result[$key] = $this->aAssignedUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aCreatedByUser) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'user';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user_user_usrusr';
                        break;
                    default:
                        $key = 'CreatedByUser';
                }

                $result[$key] = $this->aCreatedByUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUpdatedByUser) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'user';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user_user_usrusr';
                        break;
                    default:
                        $key = 'UpdatedByUser';
                }

                $result[$key] = $this->aUpdatedByUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collProjectIssuechildren) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'projectIssues';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'project_issue_prissus';
                        break;
                    default:
                        $key = 'ProjectIssuechildren';
                }

                $result[$key] = $this->collProjectIssuechildren->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collProjectIssueComments) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'projectIssueComments';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'project_issue_comment_priscos';
                        break;
                    default:
                        $key = 'ProjectIssueComments';
                }

                $result[$key] = $this->collProjectIssueComments->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ProjectIssueTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setProjectId($value);
                break;
            case 2:
                $this->setParentId($value);
                break;
            case 3:
                $this->setTypeId($value);
                break;
            case 4:
                $this->setSubject($value);
                break;
            case 5:
                $this->setDescription($value);
                break;
            case 6:
                $this->setStatusId($value);
                break;
            case 7:
                $this->setPriorityId($value);
                break;
            case 8:
                $this->setAssignedUserId($value);
                break;
            case 9:
                $this->setStartAt($value);
                break;
            case 10:
                $this->setEndAt($value);
                break;
            case 11:
                $this->setEstimatedTime($value);
                break;
            case 12:
                $this->setCompletedPercentage($value);
                break;
            case 13:
                $this->setChildrenCompletedPercentage($value);
                break;
            case 14:
                $this->setCreatedByUserId($value);
                break;
            case 15:
                $this->setUpdatedByUserId($value);
                break;
            case 16:
                $this->setCreatedAt($value);
                break;
            case 17:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ProjectIssueTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setProjectId($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setParentId($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setTypeId($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setSubject($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setDescription($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setStatusId($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setPriorityId($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setAssignedUserId($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setStartAt($arr[$keys[9]]);
        }
        if (array_key_exists($keys[10], $arr)) {
            $this->setEndAt($arr[$keys[10]]);
        }
        if (array_key_exists($keys[11], $arr)) {
            $this->setEstimatedTime($arr[$keys[11]]);
        }
        if (array_key_exists($keys[12], $arr)) {
            $this->setCompletedPercentage($arr[$keys[12]]);
        }
        if (array_key_exists($keys[13], $arr)) {
            $this->setChildrenCompletedPercentage($arr[$keys[13]]);
        }
        if (array_key_exists($keys[14], $arr)) {
            $this->setCreatedByUserId($arr[$keys[14]]);
        }
        if (array_key_exists($keys[15], $arr)) {
            $this->setUpdatedByUserId($arr[$keys[15]]);
        }
        if (array_key_exists($keys[16], $arr)) {
            $this->setCreatedAt($arr[$keys[16]]);
        }
        if (array_key_exists($keys[17], $arr)) {
            $this->setUpdatedAt($arr[$keys[17]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ProjectIssueTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_ID)) {
            $criteria->add(ProjectIssueTableMap::COL_PRISSU_ID, $this->prissu_id);
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_PRPROJ_ID)) {
            $criteria->add(ProjectIssueTableMap::COL_PRISSU_PRPROJ_ID, $this->prissu_prproj_id);
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_PARENT_ID)) {
            $criteria->add(ProjectIssueTableMap::COL_PRISSU_PARENT_ID, $this->prissu_parent_id);
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_PRISTY_ID)) {
            $criteria->add(ProjectIssueTableMap::COL_PRISSU_PRISTY_ID, $this->prissu_pristy_id);
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_SUBJECT)) {
            $criteria->add(ProjectIssueTableMap::COL_PRISSU_SUBJECT, $this->prissu_subject);
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_DESCRIPTION)) {
            $criteria->add(ProjectIssueTableMap::COL_PRISSU_DESCRIPTION, $this->prissu_description);
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_PRISST_ID)) {
            $criteria->add(ProjectIssueTableMap::COL_PRISSU_PRISST_ID, $this->prissu_prisst_id);
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_PRISPR_ID)) {
            $criteria->add(ProjectIssueTableMap::COL_PRISSU_PRISPR_ID, $this->prissu_prispr_id);
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_ASSIGNED_USER_ID)) {
            $criteria->add(ProjectIssueTableMap::COL_PRISSU_ASSIGNED_USER_ID, $this->prissu_assigned_user_id);
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_START_AT)) {
            $criteria->add(ProjectIssueTableMap::COL_PRISSU_START_AT, $this->prissu_start_at);
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_END_AT)) {
            $criteria->add(ProjectIssueTableMap::COL_PRISSU_END_AT, $this->prissu_end_at);
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_ESTIMATED_TIME)) {
            $criteria->add(ProjectIssueTableMap::COL_PRISSU_ESTIMATED_TIME, $this->prissu_estimated_time);
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_COMPLETED_PERCENTAGE)) {
            $criteria->add(ProjectIssueTableMap::COL_PRISSU_COMPLETED_PERCENTAGE, $this->prissu_completed_percentage);
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_CHILDREN_COMPLETED_PERCENTAGE)) {
            $criteria->add(ProjectIssueTableMap::COL_PRISSU_CHILDREN_COMPLETED_PERCENTAGE, $this->prissu_children_completed_percentage);
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_CREATED_BY_USER_ID)) {
            $criteria->add(ProjectIssueTableMap::COL_PRISSU_CREATED_BY_USER_ID, $this->prissu_created_by_user_id);
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_UPDATED_BY_USER_ID)) {
            $criteria->add(ProjectIssueTableMap::COL_PRISSU_UPDATED_BY_USER_ID, $this->prissu_updated_by_user_id);
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_CREATED_AT)) {
            $criteria->add(ProjectIssueTableMap::COL_PRISSU_CREATED_AT, $this->prissu_created_at);
        }
        if ($this->isColumnModified(ProjectIssueTableMap::COL_PRISSU_UPDATED_AT)) {
            $criteria->add(ProjectIssueTableMap::COL_PRISSU_UPDATED_AT, $this->prissu_updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildProjectIssueQuery::create();
        $criteria->add(ProjectIssueTableMap::COL_PRISSU_ID, $this->prissu_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (prissu_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\ProjectBundle\Model\ProjectIssue (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setProjectId($this->getProjectId());
        $copyObj->setParentId($this->getParentId());
        $copyObj->setTypeId($this->getTypeId());
        $copyObj->setSubject($this->getSubject());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setStatusId($this->getStatusId());
        $copyObj->setPriorityId($this->getPriorityId());
        $copyObj->setAssignedUserId($this->getAssignedUserId());
        $copyObj->setStartAt($this->getStartAt());
        $copyObj->setEndAt($this->getEndAt());
        $copyObj->setEstimatedTime($this->getEstimatedTime());
        $copyObj->setCompletedPercentage($this->getCompletedPercentage());
        $copyObj->setChildrenCompletedPercentage($this->getChildrenCompletedPercentage());
        $copyObj->setCreatedByUserId($this->getCreatedByUserId());
        $copyObj->setUpdatedByUserId($this->getUpdatedByUserId());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getProjectIssuechildren() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProjectIssueChild($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getProjectIssueComments() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProjectIssueComment($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\ProjectBundle\Model\ProjectIssue Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a ChildProject object.
     *
     * @param  ChildProject $v
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     * @throws PropelException
     */
    public function setProject(ChildProject $v = null)
    {
        if ($v === null) {
            $this->setProjectId(NULL);
        } else {
            $this->setProjectId($v->getId());
        }

        $this->aProject = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildProject object, it will not be re-added.
        if ($v !== null) {
            $v->addProjectIssue($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildProject object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildProject The associated ChildProject object.
     * @throws PropelException
     */
    public function getProject(ConnectionInterface $con = null)
    {
        if ($this->aProject === null && ($this->prissu_prproj_id !== null)) {
            $this->aProject = ChildProjectQuery::create()->findPk($this->prissu_prproj_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aProject->addProjectIssues($this);
             */
        }

        return $this->aProject;
    }

    /**
     * Declares an association between this object and a ChildProjectIssue object.
     *
     * @param  ChildProjectIssue $v
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     * @throws PropelException
     */
    public function setProjectIssueParent(ChildProjectIssue $v = null)
    {
        if ($v === null) {
            $this->setParentId(NULL);
        } else {
            $this->setParentId($v->getId());
        }

        $this->aProjectIssueParent = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildProjectIssue object, it will not be re-added.
        if ($v !== null) {
            $v->addProjectIssueChild($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildProjectIssue object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildProjectIssue The associated ChildProjectIssue object.
     * @throws PropelException
     */
    public function getProjectIssueParent(ConnectionInterface $con = null)
    {
        if ($this->aProjectIssueParent === null && ($this->prissu_parent_id !== null)) {
            $this->aProjectIssueParent = ChildProjectIssueQuery::create()->findPk($this->prissu_parent_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aProjectIssueParent->addProjectIssuechildren($this);
             */
        }

        return $this->aProjectIssueParent;
    }

    /**
     * Declares an association between this object and a ChildProjectIssueType object.
     *
     * @param  ChildProjectIssueType $v
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     * @throws PropelException
     */
    public function setProjectIssueType(ChildProjectIssueType $v = null)
    {
        if ($v === null) {
            $this->setTypeId(NULL);
        } else {
            $this->setTypeId($v->getId());
        }

        $this->aProjectIssueType = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildProjectIssueType object, it will not be re-added.
        if ($v !== null) {
            $v->addProjectIssue($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildProjectIssueType object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildProjectIssueType The associated ChildProjectIssueType object.
     * @throws PropelException
     */
    public function getProjectIssueType(ConnectionInterface $con = null)
    {
        if ($this->aProjectIssueType === null && ($this->prissu_pristy_id !== null)) {
            $this->aProjectIssueType = ChildProjectIssueTypeQuery::create()->findPk($this->prissu_pristy_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aProjectIssueType->addProjectIssues($this);
             */
        }

        return $this->aProjectIssueType;
    }

    /**
     * Declares an association between this object and a ChildProjectIssueStatus object.
     *
     * @param  ChildProjectIssueStatus $v
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     * @throws PropelException
     */
    public function setProjectIssueStatus(ChildProjectIssueStatus $v = null)
    {
        if ($v === null) {
            $this->setStatusId(NULL);
        } else {
            $this->setStatusId($v->getId());
        }

        $this->aProjectIssueStatus = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildProjectIssueStatus object, it will not be re-added.
        if ($v !== null) {
            $v->addProjectIssue($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildProjectIssueStatus object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildProjectIssueStatus The associated ChildProjectIssueStatus object.
     * @throws PropelException
     */
    public function getProjectIssueStatus(ConnectionInterface $con = null)
    {
        if ($this->aProjectIssueStatus === null && ($this->prissu_prisst_id !== null)) {
            $this->aProjectIssueStatus = ChildProjectIssueStatusQuery::create()->findPk($this->prissu_prisst_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aProjectIssueStatus->addProjectIssues($this);
             */
        }

        return $this->aProjectIssueStatus;
    }

    /**
     * Declares an association between this object and a ChildProjectIssuePriority object.
     *
     * @param  ChildProjectIssuePriority $v
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     * @throws PropelException
     */
    public function setProjectIssuePriority(ChildProjectIssuePriority $v = null)
    {
        if ($v === null) {
            $this->setPriorityId(NULL);
        } else {
            $this->setPriorityId($v->getId());
        }

        $this->aProjectIssuePriority = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the ChildProjectIssuePriority object, it will not be re-added.
        if ($v !== null) {
            $v->addProjectIssue($this);
        }


        return $this;
    }


    /**
     * Get the associated ChildProjectIssuePriority object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return ChildProjectIssuePriority The associated ChildProjectIssuePriority object.
     * @throws PropelException
     */
    public function getProjectIssuePriority(ConnectionInterface $con = null)
    {
        if ($this->aProjectIssuePriority === null && ($this->prissu_prispr_id !== null)) {
            $this->aProjectIssuePriority = ChildProjectIssuePriorityQuery::create()->findPk($this->prissu_prispr_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aProjectIssuePriority->addProjectIssues($this);
             */
        }

        return $this->aProjectIssuePriority;
    }

    /**
     * Declares an association between this object and a User object.
     *
     * @param  User $v
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     * @throws PropelException
     */
    public function setAssignedUser(User $v = null)
    {
        if ($v === null) {
            $this->setAssignedUserId(NULL);
        } else {
            $this->setAssignedUserId($v->getId());
        }

        $this->aAssignedUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the User object, it will not be re-added.
        if ($v !== null) {
            $v->addAssignedUserIssue($this);
        }


        return $this;
    }


    /**
     * Get the associated User object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return User The associated User object.
     * @throws PropelException
     */
    public function getAssignedUser(ConnectionInterface $con = null)
    {
        if ($this->aAssignedUser === null && ($this->prissu_assigned_user_id !== null)) {
            $this->aAssignedUser = UserQuery::create()->findPk($this->prissu_assigned_user_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aAssignedUser->addAssignedUserIssues($this);
             */
        }

        return $this->aAssignedUser;
    }

    /**
     * Declares an association between this object and a User object.
     *
     * @param  User $v
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCreatedByUser(User $v = null)
    {
        if ($v === null) {
            $this->setCreatedByUserId(NULL);
        } else {
            $this->setCreatedByUserId($v->getId());
        }

        $this->aCreatedByUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the User object, it will not be re-added.
        if ($v !== null) {
            $v->addCreatedByUserProjectIssue($this);
        }


        return $this;
    }


    /**
     * Get the associated User object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return User The associated User object.
     * @throws PropelException
     */
    public function getCreatedByUser(ConnectionInterface $con = null)
    {
        if ($this->aCreatedByUser === null && ($this->prissu_created_by_user_id !== null)) {
            $this->aCreatedByUser = UserQuery::create()->findPk($this->prissu_created_by_user_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCreatedByUser->addCreatedByUserProjectIssues($this);
             */
        }

        return $this->aCreatedByUser;
    }

    /**
     * Declares an association between this object and a User object.
     *
     * @param  User $v
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUpdatedByUser(User $v = null)
    {
        if ($v === null) {
            $this->setUpdatedByUserId(NULL);
        } else {
            $this->setUpdatedByUserId($v->getId());
        }

        $this->aUpdatedByUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the User object, it will not be re-added.
        if ($v !== null) {
            $v->addUpdatedByUserProjectIssue($this);
        }


        return $this;
    }


    /**
     * Get the associated User object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return User The associated User object.
     * @throws PropelException
     */
    public function getUpdatedByUser(ConnectionInterface $con = null)
    {
        if ($this->aUpdatedByUser === null && ($this->prissu_updated_by_user_id !== null)) {
            $this->aUpdatedByUser = UserQuery::create()->findPk($this->prissu_updated_by_user_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUpdatedByUser->addUpdatedByUserProjectIssues($this);
             */
        }

        return $this->aUpdatedByUser;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('ProjectIssueChild' == $relationName) {
            return $this->initProjectIssuechildren();
        }
        if ('ProjectIssueComment' == $relationName) {
            return $this->initProjectIssueComments();
        }
    }

    /**
     * Clears out the collProjectIssuechildren collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProjectIssuechildren()
     */
    public function clearProjectIssuechildren()
    {
        $this->collProjectIssuechildren = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProjectIssuechildren collection loaded partially.
     */
    public function resetPartialProjectIssuechildren($v = true)
    {
        $this->collProjectIssuechildrenPartial = $v;
    }

    /**
     * Initializes the collProjectIssuechildren collection.
     *
     * By default this just sets the collProjectIssuechildren collection to an empty array (like clearcollProjectIssuechildren());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProjectIssuechildren($overrideExisting = true)
    {
        if (null !== $this->collProjectIssuechildren && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProjectIssueTableMap::getTableMap()->getCollectionClassName();

        $this->collProjectIssuechildren = new $collectionClassName;
        $this->collProjectIssuechildren->setModel('\IiMedias\ProjectBundle\Model\ProjectIssue');
    }

    /**
     * Gets an array of ChildProjectIssue objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProjectIssue is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildProjectIssue[] List of ChildProjectIssue objects
     * @throws PropelException
     */
    public function getProjectIssuechildren(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProjectIssuechildrenPartial && !$this->isNew();
        if (null === $this->collProjectIssuechildren || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collProjectIssuechildren) {
                // return empty collection
                $this->initProjectIssuechildren();
            } else {
                $collProjectIssuechildren = ChildProjectIssueQuery::create(null, $criteria)
                    ->filterByProjectIssueParent($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProjectIssuechildrenPartial && count($collProjectIssuechildren)) {
                        $this->initProjectIssuechildren(false);

                        foreach ($collProjectIssuechildren as $obj) {
                            if (false == $this->collProjectIssuechildren->contains($obj)) {
                                $this->collProjectIssuechildren->append($obj);
                            }
                        }

                        $this->collProjectIssuechildrenPartial = true;
                    }

                    return $collProjectIssuechildren;
                }

                if ($partial && $this->collProjectIssuechildren) {
                    foreach ($this->collProjectIssuechildren as $obj) {
                        if ($obj->isNew()) {
                            $collProjectIssuechildren[] = $obj;
                        }
                    }
                }

                $this->collProjectIssuechildren = $collProjectIssuechildren;
                $this->collProjectIssuechildrenPartial = false;
            }
        }

        return $this->collProjectIssuechildren;
    }

    /**
     * Sets a collection of ChildProjectIssue objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $projectIssuechildren A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProjectIssue The current object (for fluent API support)
     */
    public function setProjectIssuechildren(Collection $projectIssuechildren, ConnectionInterface $con = null)
    {
        /** @var ChildProjectIssue[] $projectIssuechildrenToDelete */
        $projectIssuechildrenToDelete = $this->getProjectIssuechildren(new Criteria(), $con)->diff($projectIssuechildren);


        $this->projectIssuechildrenScheduledForDeletion = $projectIssuechildrenToDelete;

        foreach ($projectIssuechildrenToDelete as $projectIssueChildRemoved) {
            $projectIssueChildRemoved->setProjectIssueParent(null);
        }

        $this->collProjectIssuechildren = null;
        foreach ($projectIssuechildren as $projectIssueChild) {
            $this->addProjectIssueChild($projectIssueChild);
        }

        $this->collProjectIssuechildren = $projectIssuechildren;
        $this->collProjectIssuechildrenPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ProjectIssue objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ProjectIssue objects.
     * @throws PropelException
     */
    public function countProjectIssuechildren(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProjectIssuechildrenPartial && !$this->isNew();
        if (null === $this->collProjectIssuechildren || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProjectIssuechildren) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProjectIssuechildren());
            }

            $query = ChildProjectIssueQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProjectIssueParent($this)
                ->count($con);
        }

        return count($this->collProjectIssuechildren);
    }

    /**
     * Method called to associate a ChildProjectIssue object to this object
     * through the ChildProjectIssue foreign key attribute.
     *
     * @param  ChildProjectIssue $l ChildProjectIssue
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     */
    public function addProjectIssueChild(ChildProjectIssue $l)
    {
        if ($this->collProjectIssuechildren === null) {
            $this->initProjectIssuechildren();
            $this->collProjectIssuechildrenPartial = true;
        }

        if (!$this->collProjectIssuechildren->contains($l)) {
            $this->doAddProjectIssueChild($l);

            if ($this->projectIssuechildrenScheduledForDeletion and $this->projectIssuechildrenScheduledForDeletion->contains($l)) {
                $this->projectIssuechildrenScheduledForDeletion->remove($this->projectIssuechildrenScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildProjectIssue $projectIssueChild The ChildProjectIssue object to add.
     */
    protected function doAddProjectIssueChild(ChildProjectIssue $projectIssueChild)
    {
        $this->collProjectIssuechildren[]= $projectIssueChild;
        $projectIssueChild->setProjectIssueParent($this);
    }

    /**
     * @param  ChildProjectIssue $projectIssueChild The ChildProjectIssue object to remove.
     * @return $this|ChildProjectIssue The current object (for fluent API support)
     */
    public function removeProjectIssueChild(ChildProjectIssue $projectIssueChild)
    {
        if ($this->getProjectIssuechildren()->contains($projectIssueChild)) {
            $pos = $this->collProjectIssuechildren->search($projectIssueChild);
            $this->collProjectIssuechildren->remove($pos);
            if (null === $this->projectIssuechildrenScheduledForDeletion) {
                $this->projectIssuechildrenScheduledForDeletion = clone $this->collProjectIssuechildren;
                $this->projectIssuechildrenScheduledForDeletion->clear();
            }
            $this->projectIssuechildrenScheduledForDeletion[]= $projectIssueChild;
            $projectIssueChild->setProjectIssueParent(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ProjectIssue is new, it will return
     * an empty collection; or if this ProjectIssue has previously
     * been saved, it will retrieve related ProjectIssuechildren from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ProjectIssue.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProjectIssue[] List of ChildProjectIssue objects
     */
    public function getProjectIssuechildrenJoinProject(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProjectIssueQuery::create(null, $criteria);
        $query->joinWith('Project', $joinBehavior);

        return $this->getProjectIssuechildren($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ProjectIssue is new, it will return
     * an empty collection; or if this ProjectIssue has previously
     * been saved, it will retrieve related ProjectIssuechildren from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ProjectIssue.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProjectIssue[] List of ChildProjectIssue objects
     */
    public function getProjectIssuechildrenJoinProjectIssueType(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProjectIssueQuery::create(null, $criteria);
        $query->joinWith('ProjectIssueType', $joinBehavior);

        return $this->getProjectIssuechildren($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ProjectIssue is new, it will return
     * an empty collection; or if this ProjectIssue has previously
     * been saved, it will retrieve related ProjectIssuechildren from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ProjectIssue.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProjectIssue[] List of ChildProjectIssue objects
     */
    public function getProjectIssuechildrenJoinProjectIssueStatus(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProjectIssueQuery::create(null, $criteria);
        $query->joinWith('ProjectIssueStatus', $joinBehavior);

        return $this->getProjectIssuechildren($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ProjectIssue is new, it will return
     * an empty collection; or if this ProjectIssue has previously
     * been saved, it will retrieve related ProjectIssuechildren from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ProjectIssue.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProjectIssue[] List of ChildProjectIssue objects
     */
    public function getProjectIssuechildrenJoinProjectIssuePriority(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProjectIssueQuery::create(null, $criteria);
        $query->joinWith('ProjectIssuePriority', $joinBehavior);

        return $this->getProjectIssuechildren($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ProjectIssue is new, it will return
     * an empty collection; or if this ProjectIssue has previously
     * been saved, it will retrieve related ProjectIssuechildren from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ProjectIssue.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProjectIssue[] List of ChildProjectIssue objects
     */
    public function getProjectIssuechildrenJoinAssignedUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProjectIssueQuery::create(null, $criteria);
        $query->joinWith('AssignedUser', $joinBehavior);

        return $this->getProjectIssuechildren($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ProjectIssue is new, it will return
     * an empty collection; or if this ProjectIssue has previously
     * been saved, it will retrieve related ProjectIssuechildren from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ProjectIssue.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProjectIssue[] List of ChildProjectIssue objects
     */
    public function getProjectIssuechildrenJoinCreatedByUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProjectIssueQuery::create(null, $criteria);
        $query->joinWith('CreatedByUser', $joinBehavior);

        return $this->getProjectIssuechildren($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ProjectIssue is new, it will return
     * an empty collection; or if this ProjectIssue has previously
     * been saved, it will retrieve related ProjectIssuechildren from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ProjectIssue.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProjectIssue[] List of ChildProjectIssue objects
     */
    public function getProjectIssuechildrenJoinUpdatedByUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProjectIssueQuery::create(null, $criteria);
        $query->joinWith('UpdatedByUser', $joinBehavior);

        return $this->getProjectIssuechildren($query, $con);
    }

    /**
     * Clears out the collProjectIssueComments collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProjectIssueComments()
     */
    public function clearProjectIssueComments()
    {
        $this->collProjectIssueComments = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProjectIssueComments collection loaded partially.
     */
    public function resetPartialProjectIssueComments($v = true)
    {
        $this->collProjectIssueCommentsPartial = $v;
    }

    /**
     * Initializes the collProjectIssueComments collection.
     *
     * By default this just sets the collProjectIssueComments collection to an empty array (like clearcollProjectIssueComments());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProjectIssueComments($overrideExisting = true)
    {
        if (null !== $this->collProjectIssueComments && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProjectIssueCommentTableMap::getTableMap()->getCollectionClassName();

        $this->collProjectIssueComments = new $collectionClassName;
        $this->collProjectIssueComments->setModel('\IiMedias\ProjectBundle\Model\ProjectIssueComment');
    }

    /**
     * Gets an array of ChildProjectIssueComment objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProjectIssue is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildProjectIssueComment[] List of ChildProjectIssueComment objects
     * @throws PropelException
     */
    public function getProjectIssueComments(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProjectIssueCommentsPartial && !$this->isNew();
        if (null === $this->collProjectIssueComments || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collProjectIssueComments) {
                // return empty collection
                $this->initProjectIssueComments();
            } else {
                $collProjectIssueComments = ChildProjectIssueCommentQuery::create(null, $criteria)
                    ->filterByProjectIssue($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProjectIssueCommentsPartial && count($collProjectIssueComments)) {
                        $this->initProjectIssueComments(false);

                        foreach ($collProjectIssueComments as $obj) {
                            if (false == $this->collProjectIssueComments->contains($obj)) {
                                $this->collProjectIssueComments->append($obj);
                            }
                        }

                        $this->collProjectIssueCommentsPartial = true;
                    }

                    return $collProjectIssueComments;
                }

                if ($partial && $this->collProjectIssueComments) {
                    foreach ($this->collProjectIssueComments as $obj) {
                        if ($obj->isNew()) {
                            $collProjectIssueComments[] = $obj;
                        }
                    }
                }

                $this->collProjectIssueComments = $collProjectIssueComments;
                $this->collProjectIssueCommentsPartial = false;
            }
        }

        return $this->collProjectIssueComments;
    }

    /**
     * Sets a collection of ChildProjectIssueComment objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $projectIssueComments A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProjectIssue The current object (for fluent API support)
     */
    public function setProjectIssueComments(Collection $projectIssueComments, ConnectionInterface $con = null)
    {
        /** @var ChildProjectIssueComment[] $projectIssueCommentsToDelete */
        $projectIssueCommentsToDelete = $this->getProjectIssueComments(new Criteria(), $con)->diff($projectIssueComments);


        $this->projectIssueCommentsScheduledForDeletion = $projectIssueCommentsToDelete;

        foreach ($projectIssueCommentsToDelete as $projectIssueCommentRemoved) {
            $projectIssueCommentRemoved->setProjectIssue(null);
        }

        $this->collProjectIssueComments = null;
        foreach ($projectIssueComments as $projectIssueComment) {
            $this->addProjectIssueComment($projectIssueComment);
        }

        $this->collProjectIssueComments = $projectIssueComments;
        $this->collProjectIssueCommentsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ProjectIssueComment objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ProjectIssueComment objects.
     * @throws PropelException
     */
    public function countProjectIssueComments(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProjectIssueCommentsPartial && !$this->isNew();
        if (null === $this->collProjectIssueComments || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProjectIssueComments) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProjectIssueComments());
            }

            $query = ChildProjectIssueCommentQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProjectIssue($this)
                ->count($con);
        }

        return count($this->collProjectIssueComments);
    }

    /**
     * Method called to associate a ChildProjectIssueComment object to this object
     * through the ChildProjectIssueComment foreign key attribute.
     *
     * @param  ChildProjectIssueComment $l ChildProjectIssueComment
     * @return $this|\IiMedias\ProjectBundle\Model\ProjectIssue The current object (for fluent API support)
     */
    public function addProjectIssueComment(ChildProjectIssueComment $l)
    {
        if ($this->collProjectIssueComments === null) {
            $this->initProjectIssueComments();
            $this->collProjectIssueCommentsPartial = true;
        }

        if (!$this->collProjectIssueComments->contains($l)) {
            $this->doAddProjectIssueComment($l);

            if ($this->projectIssueCommentsScheduledForDeletion and $this->projectIssueCommentsScheduledForDeletion->contains($l)) {
                $this->projectIssueCommentsScheduledForDeletion->remove($this->projectIssueCommentsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildProjectIssueComment $projectIssueComment The ChildProjectIssueComment object to add.
     */
    protected function doAddProjectIssueComment(ChildProjectIssueComment $projectIssueComment)
    {
        $this->collProjectIssueComments[]= $projectIssueComment;
        $projectIssueComment->setProjectIssue($this);
    }

    /**
     * @param  ChildProjectIssueComment $projectIssueComment The ChildProjectIssueComment object to remove.
     * @return $this|ChildProjectIssue The current object (for fluent API support)
     */
    public function removeProjectIssueComment(ChildProjectIssueComment $projectIssueComment)
    {
        if ($this->getProjectIssueComments()->contains($projectIssueComment)) {
            $pos = $this->collProjectIssueComments->search($projectIssueComment);
            $this->collProjectIssueComments->remove($pos);
            if (null === $this->projectIssueCommentsScheduledForDeletion) {
                $this->projectIssueCommentsScheduledForDeletion = clone $this->collProjectIssueComments;
                $this->projectIssueCommentsScheduledForDeletion->clear();
            }
            $this->projectIssueCommentsScheduledForDeletion[]= clone $projectIssueComment;
            $projectIssueComment->setProjectIssue(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ProjectIssue is new, it will return
     * an empty collection; or if this ProjectIssue has previously
     * been saved, it will retrieve related ProjectIssueComments from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ProjectIssue.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProjectIssueComment[] List of ChildProjectIssueComment objects
     */
    public function getProjectIssueCommentsJoinCreatedByUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProjectIssueCommentQuery::create(null, $criteria);
        $query->joinWith('CreatedByUser', $joinBehavior);

        return $this->getProjectIssueComments($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ProjectIssue is new, it will return
     * an empty collection; or if this ProjectIssue has previously
     * been saved, it will retrieve related ProjectIssueComments from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ProjectIssue.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProjectIssueComment[] List of ChildProjectIssueComment objects
     */
    public function getProjectIssueCommentsJoinUpdatedByUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProjectIssueCommentQuery::create(null, $criteria);
        $query->joinWith('UpdatedByUser', $joinBehavior);

        return $this->getProjectIssueComments($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aProject) {
            $this->aProject->removeProjectIssue($this);
        }
        if (null !== $this->aProjectIssueParent) {
            $this->aProjectIssueParent->removeProjectIssueChild($this);
        }
        if (null !== $this->aProjectIssueType) {
            $this->aProjectIssueType->removeProjectIssue($this);
        }
        if (null !== $this->aProjectIssueStatus) {
            $this->aProjectIssueStatus->removeProjectIssue($this);
        }
        if (null !== $this->aProjectIssuePriority) {
            $this->aProjectIssuePriority->removeProjectIssue($this);
        }
        if (null !== $this->aAssignedUser) {
            $this->aAssignedUser->removeAssignedUserIssue($this);
        }
        if (null !== $this->aCreatedByUser) {
            $this->aCreatedByUser->removeCreatedByUserProjectIssue($this);
        }
        if (null !== $this->aUpdatedByUser) {
            $this->aUpdatedByUser->removeUpdatedByUserProjectIssue($this);
        }
        $this->prissu_id = null;
        $this->prissu_prproj_id = null;
        $this->prissu_parent_id = null;
        $this->prissu_pristy_id = null;
        $this->prissu_subject = null;
        $this->prissu_description = null;
        $this->prissu_prisst_id = null;
        $this->prissu_prispr_id = null;
        $this->prissu_assigned_user_id = null;
        $this->prissu_start_at = null;
        $this->prissu_end_at = null;
        $this->prissu_estimated_time = null;
        $this->prissu_completed_percentage = null;
        $this->prissu_children_completed_percentage = null;
        $this->prissu_created_by_user_id = null;
        $this->prissu_updated_by_user_id = null;
        $this->prissu_created_at = null;
        $this->prissu_updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collProjectIssuechildren) {
                foreach ($this->collProjectIssuechildren as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collProjectIssueComments) {
                foreach ($this->collProjectIssueComments as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collProjectIssuechildren = null;
        $this->collProjectIssueComments = null;
        $this->aProject = null;
        $this->aProjectIssueParent = null;
        $this->aProjectIssueType = null;
        $this->aProjectIssueStatus = null;
        $this->aProjectIssuePriority = null;
        $this->aAssignedUser = null;
        $this->aCreatedByUser = null;
        $this->aUpdatedByUser = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ProjectIssueTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildProjectIssue The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[ProjectIssueTableMap::COL_PRISSU_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}

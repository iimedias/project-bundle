<?php

namespace IiMedias\ProjectBundle\Model\Base;

use \DateTime;
use \Exception;
use \PDO;
use IiMedias\AdminBundle\Model\User;
use IiMedias\AdminBundle\Model\UserQuery;
use IiMedias\ProjectBundle\Model\Project as ChildProject;
use IiMedias\ProjectBundle\Model\ProjectIssue as ChildProjectIssue;
use IiMedias\ProjectBundle\Model\ProjectIssueQuery as ChildProjectIssueQuery;
use IiMedias\ProjectBundle\Model\ProjectMember as ChildProjectMember;
use IiMedias\ProjectBundle\Model\ProjectMemberQuery as ChildProjectMemberQuery;
use IiMedias\ProjectBundle\Model\ProjectQuery as ChildProjectQuery;
use IiMedias\ProjectBundle\Model\Map\ProjectIssueTableMap;
use IiMedias\ProjectBundle\Model\Map\ProjectMemberTableMap;
use IiMedias\ProjectBundle\Model\Map\ProjectTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveRecord\ActiveRecordInterface;
use Propel\Runtime\Collection\Collection;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\BadMethodCallException;
use Propel\Runtime\Exception\LogicException;
use Propel\Runtime\Exception\PropelException;
use Propel\Runtime\Map\TableMap;
use Propel\Runtime\Parser\AbstractParser;
use Propel\Runtime\Util\PropelDateTime;

/**
 * Base class that represents a row from the 'project_project_prproj' table.
 *
 *
 *
 * @package    propel.generator.src.IiMedias.ProjectBundle.Model.Base
 */
abstract class Project implements ActiveRecordInterface
{
    /**
     * TableMap class name
     */
    const TABLE_MAP = '\\IiMedias\\ProjectBundle\\Model\\Map\\ProjectTableMap';


    /**
     * attribute to determine if this object has previously been saved.
     * @var boolean
     */
    protected $new = true;

    /**
     * attribute to determine whether this object has been deleted.
     * @var boolean
     */
    protected $deleted = false;

    /**
     * The columns that have been modified in current object.
     * Tracking modified columns allows us to only update modified columns.
     * @var array
     */
    protected $modifiedColumns = array();

    /**
     * The (virtual) columns that are added at runtime
     * The formatters can add supplementary columns based on a resultset
     * @var array
     */
    protected $virtualColumns = array();

    /**
     * The value for the prproj_id field.
     *
     * @var        int
     */
    protected $prproj_id;

    /**
     * The value for the prproj_name field.
     *
     * @var        string
     */
    protected $prproj_name;

    /**
     * The value for the prproj_slug field.
     *
     * @var        string
     */
    protected $prproj_slug;

    /**
     * The value for the prproj_description field.
     *
     * @var        string
     */
    protected $prproj_description;

    /**
     * The value for the prproj_website field.
     *
     * @var        string
     */
    protected $prproj_website;

    /**
     * The value for the prproj_is_public field.
     *
     * Note: this column has a database default value of: false
     * @var        boolean
     */
    protected $prproj_is_public;

    /**
     * The value for the prproj_created_by_user_id field.
     *
     * @var        int
     */
    protected $prproj_created_by_user_id;

    /**
     * The value for the prproj_updated_by_user_id field.
     *
     * @var        int
     */
    protected $prproj_updated_by_user_id;

    /**
     * The value for the prproj_created_at field.
     *
     * @var        DateTime
     */
    protected $prproj_created_at;

    /**
     * The value for the prproj_updated_at field.
     *
     * @var        DateTime
     */
    protected $prproj_updated_at;

    /**
     * @var        User
     */
    protected $aCreatedByUser;

    /**
     * @var        User
     */
    protected $aUpdatedByUser;

    /**
     * @var        ObjectCollection|ChildProjectMember[] Collection to store aggregation of ChildProjectMember objects.
     */
    protected $collProjectMembers;
    protected $collProjectMembersPartial;

    /**
     * @var        ObjectCollection|ChildProjectIssue[] Collection to store aggregation of ChildProjectIssue objects.
     */
    protected $collProjectIssues;
    protected $collProjectIssuesPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     *
     * @var boolean
     */
    protected $alreadyInSave = false;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildProjectMember[]
     */
    protected $projectMembersScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var ObjectCollection|ChildProjectIssue[]
     */
    protected $projectIssuesScheduledForDeletion = null;

    /**
     * Applies default values to this object.
     * This method should be called from the object's constructor (or
     * equivalent initialization method).
     * @see __construct()
     */
    public function applyDefaultValues()
    {
        $this->prproj_is_public = false;
    }

    /**
     * Initializes internal state of IiMedias\ProjectBundle\Model\Base\Project object.
     * @see applyDefaults()
     */
    public function __construct()
    {
        $this->applyDefaultValues();
    }

    /**
     * Returns whether the object has been modified.
     *
     * @return boolean True if the object has been modified.
     */
    public function isModified()
    {
        return !!$this->modifiedColumns;
    }

    /**
     * Has specified column been modified?
     *
     * @param  string  $col column fully qualified name (TableMap::TYPE_COLNAME), e.g. Book::AUTHOR_ID
     * @return boolean True if $col has been modified.
     */
    public function isColumnModified($col)
    {
        return $this->modifiedColumns && isset($this->modifiedColumns[$col]);
    }

    /**
     * Get the columns that have been modified in this object.
     * @return array A unique list of the modified column names for this object.
     */
    public function getModifiedColumns()
    {
        return $this->modifiedColumns ? array_keys($this->modifiedColumns) : [];
    }

    /**
     * Returns whether the object has ever been saved.  This will
     * be false, if the object was retrieved from storage or was created
     * and then saved.
     *
     * @return boolean true, if the object has never been persisted.
     */
    public function isNew()
    {
        return $this->new;
    }

    /**
     * Setter for the isNew attribute.  This method will be called
     * by Propel-generated children and objects.
     *
     * @param boolean $b the state of the object.
     */
    public function setNew($b)
    {
        $this->new = (boolean) $b;
    }

    /**
     * Whether this object has been deleted.
     * @return boolean The deleted state of this object.
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * Specify whether this object has been deleted.
     * @param  boolean $b The deleted state of this object.
     * @return void
     */
    public function setDeleted($b)
    {
        $this->deleted = (boolean) $b;
    }

    /**
     * Sets the modified state for the object to be false.
     * @param  string $col If supplied, only the specified column is reset.
     * @return void
     */
    public function resetModified($col = null)
    {
        if (null !== $col) {
            if (isset($this->modifiedColumns[$col])) {
                unset($this->modifiedColumns[$col]);
            }
        } else {
            $this->modifiedColumns = array();
        }
    }

    /**
     * Compares this with another <code>Project</code> instance.  If
     * <code>obj</code> is an instance of <code>Project</code>, delegates to
     * <code>equals(Project)</code>.  Otherwise, returns <code>false</code>.
     *
     * @param  mixed   $obj The object to compare to.
     * @return boolean Whether equal to the object specified.
     */
    public function equals($obj)
    {
        if (!$obj instanceof static) {
            return false;
        }

        if ($this === $obj) {
            return true;
        }

        if (null === $this->getPrimaryKey() || null === $obj->getPrimaryKey()) {
            return false;
        }

        return $this->getPrimaryKey() === $obj->getPrimaryKey();
    }

    /**
     * Get the associative array of the virtual columns in this object
     *
     * @return array
     */
    public function getVirtualColumns()
    {
        return $this->virtualColumns;
    }

    /**
     * Checks the existence of a virtual column in this object
     *
     * @param  string  $name The virtual column name
     * @return boolean
     */
    public function hasVirtualColumn($name)
    {
        return array_key_exists($name, $this->virtualColumns);
    }

    /**
     * Get the value of a virtual column in this object
     *
     * @param  string $name The virtual column name
     * @return mixed
     *
     * @throws PropelException
     */
    public function getVirtualColumn($name)
    {
        if (!$this->hasVirtualColumn($name)) {
            throw new PropelException(sprintf('Cannot get value of inexistent virtual column %s.', $name));
        }

        return $this->virtualColumns[$name];
    }

    /**
     * Set the value of a virtual column in this object
     *
     * @param string $name  The virtual column name
     * @param mixed  $value The value to give to the virtual column
     *
     * @return $this|Project The current object, for fluid interface
     */
    public function setVirtualColumn($name, $value)
    {
        $this->virtualColumns[$name] = $value;

        return $this;
    }

    /**
     * Logs a message using Propel::log().
     *
     * @param  string  $msg
     * @param  int     $priority One of the Propel::LOG_* logging levels
     * @return boolean
     */
    protected function log($msg, $priority = Propel::LOG_INFO)
    {
        return Propel::log(get_class($this) . ': ' . $msg, $priority);
    }

    /**
     * Export the current object properties to a string, using a given parser format
     * <code>
     * $book = BookQuery::create()->findPk(9012);
     * echo $book->exportTo('JSON');
     *  => {"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * @param  mixed   $parser                 A AbstractParser instance, or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param  boolean $includeLazyLoadColumns (optional) Whether to include lazy load(ed) columns. Defaults to TRUE.
     * @return string  The exported data
     */
    public function exportTo($parser, $includeLazyLoadColumns = true)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        return $parser->fromArray($this->toArray(TableMap::TYPE_PHPNAME, $includeLazyLoadColumns, array(), true));
    }

    /**
     * Clean up internal collections prior to serializing
     * Avoids recursive loops that turn into segmentation faults when serializing
     */
    public function __sleep()
    {
        $this->clearAllReferences();

        $cls = new \ReflectionClass($this);
        $propertyNames = [];
        $serializableProperties = array_diff($cls->getProperties(), $cls->getProperties(\ReflectionProperty::IS_STATIC));

        foreach($serializableProperties as $property) {
            $propertyNames[] = $property->getName();
        }

        return $propertyNames;
    }

    /**
     * Get the [prproj_id] column value.
     *
     * @return int
     */
    public function getId()
    {
        return $this->prproj_id;
    }

    /**
     * Get the [prproj_name] column value.
     *
     * @return string
     */
    public function getName()
    {
        return $this->prproj_name;
    }

    /**
     * Get the [prproj_slug] column value.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->prproj_slug;
    }

    /**
     * Get the [prproj_description] column value.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->prproj_description;
    }

    /**
     * Get the [prproj_website] column value.
     *
     * @return string
     */
    public function getWebSite()
    {
        return $this->prproj_website;
    }

    /**
     * Get the [prproj_is_public] column value.
     *
     * @return boolean
     */
    public function getIsPublic()
    {
        return $this->prproj_is_public;
    }

    /**
     * Get the [prproj_is_public] column value.
     *
     * @return boolean
     */
    public function isPublic()
    {
        return $this->getIsPublic();
    }

    /**
     * Get the [prproj_created_by_user_id] column value.
     *
     * @return int
     */
    public function getCreatedByUserId()
    {
        return $this->prproj_created_by_user_id;
    }

    /**
     * Get the [prproj_updated_by_user_id] column value.
     *
     * @return int
     */
    public function getUpdatedByUserId()
    {
        return $this->prproj_updated_by_user_id;
    }

    /**
     * Get the [optionally formatted] temporal [prproj_created_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getCreatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->prproj_created_at;
        } else {
            return $this->prproj_created_at instanceof \DateTimeInterface ? $this->prproj_created_at->format($format) : null;
        }
    }

    /**
     * Get the [optionally formatted] temporal [prproj_updated_at] column value.
     *
     *
     * @param      string $format The date/time format string (either date()-style or strftime()-style).
     *                            If format is NULL, then the raw DateTime object will be returned.
     *
     * @return string|DateTime Formatted date/time value as string or DateTime object (if format is NULL), NULL if column is NULL, and 0 if column value is 0000-00-00 00:00:00
     *
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getUpdatedAt($format = NULL)
    {
        if ($format === null) {
            return $this->prproj_updated_at;
        } else {
            return $this->prproj_updated_at instanceof \DateTimeInterface ? $this->prproj_updated_at->format($format) : null;
        }
    }

    /**
     * Set the value of [prproj_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\ProjectBundle\Model\Project The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->prproj_id !== $v) {
            $this->prproj_id = $v;
            $this->modifiedColumns[ProjectTableMap::COL_PRPROJ_ID] = true;
        }

        return $this;
    } // setId()

    /**
     * Set the value of [prproj_name] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\ProjectBundle\Model\Project The current object (for fluent API support)
     */
    public function setName($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->prproj_name !== $v) {
            $this->prproj_name = $v;
            $this->modifiedColumns[ProjectTableMap::COL_PRPROJ_NAME] = true;
        }

        return $this;
    } // setName()

    /**
     * Set the value of [prproj_slug] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\ProjectBundle\Model\Project The current object (for fluent API support)
     */
    public function setSlug($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->prproj_slug !== $v) {
            $this->prproj_slug = $v;
            $this->modifiedColumns[ProjectTableMap::COL_PRPROJ_SLUG] = true;
        }

        return $this;
    } // setSlug()

    /**
     * Set the value of [prproj_description] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\ProjectBundle\Model\Project The current object (for fluent API support)
     */
    public function setDescription($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->prproj_description !== $v) {
            $this->prproj_description = $v;
            $this->modifiedColumns[ProjectTableMap::COL_PRPROJ_DESCRIPTION] = true;
        }

        return $this;
    } // setDescription()

    /**
     * Set the value of [prproj_website] column.
     *
     * @param string $v new value
     * @return $this|\IiMedias\ProjectBundle\Model\Project The current object (for fluent API support)
     */
    public function setWebSite($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->prproj_website !== $v) {
            $this->prproj_website = $v;
            $this->modifiedColumns[ProjectTableMap::COL_PRPROJ_WEBSITE] = true;
        }

        return $this;
    } // setWebSite()

    /**
     * Sets the value of the [prproj_is_public] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param  boolean|integer|string $v The new value
     * @return $this|\IiMedias\ProjectBundle\Model\Project The current object (for fluent API support)
     */
    public function setIsPublic($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->prproj_is_public !== $v) {
            $this->prproj_is_public = $v;
            $this->modifiedColumns[ProjectTableMap::COL_PRPROJ_IS_PUBLIC] = true;
        }

        return $this;
    } // setIsPublic()

    /**
     * Set the value of [prproj_created_by_user_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\ProjectBundle\Model\Project The current object (for fluent API support)
     */
    public function setCreatedByUserId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->prproj_created_by_user_id !== $v) {
            $this->prproj_created_by_user_id = $v;
            $this->modifiedColumns[ProjectTableMap::COL_PRPROJ_CREATED_BY_USER_ID] = true;
        }

        if ($this->aCreatedByUser !== null && $this->aCreatedByUser->getId() !== $v) {
            $this->aCreatedByUser = null;
        }

        return $this;
    } // setCreatedByUserId()

    /**
     * Set the value of [prproj_updated_by_user_id] column.
     *
     * @param int $v new value
     * @return $this|\IiMedias\ProjectBundle\Model\Project The current object (for fluent API support)
     */
    public function setUpdatedByUserId($v)
    {
        if ($v !== null) {
            $v = (int) $v;
        }

        if ($this->prproj_updated_by_user_id !== $v) {
            $this->prproj_updated_by_user_id = $v;
            $this->modifiedColumns[ProjectTableMap::COL_PRPROJ_UPDATED_BY_USER_ID] = true;
        }

        if ($this->aUpdatedByUser !== null && $this->aUpdatedByUser->getId() !== $v) {
            $this->aUpdatedByUser = null;
        }

        return $this;
    } // setUpdatedByUserId()

    /**
     * Sets the value of [prproj_created_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\ProjectBundle\Model\Project The current object (for fluent API support)
     */
    public function setCreatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->prproj_created_at !== null || $dt !== null) {
            if ($this->prproj_created_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->prproj_created_at->format("Y-m-d H:i:s.u")) {
                $this->prproj_created_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ProjectTableMap::COL_PRPROJ_CREATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setCreatedAt()

    /**
     * Sets the value of [prproj_updated_at] column to a normalized version of the date/time value specified.
     *
     * @param  mixed $v string, integer (timestamp), or \DateTimeInterface value.
     *               Empty strings are treated as NULL.
     * @return $this|\IiMedias\ProjectBundle\Model\Project The current object (for fluent API support)
     */
    public function setUpdatedAt($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->prproj_updated_at !== null || $dt !== null) {
            if ($this->prproj_updated_at === null || $dt === null || $dt->format("Y-m-d H:i:s.u") !== $this->prproj_updated_at->format("Y-m-d H:i:s.u")) {
                $this->prproj_updated_at = $dt === null ? null : clone $dt;
                $this->modifiedColumns[ProjectTableMap::COL_PRPROJ_UPDATED_AT] = true;
            }
        } // if either are not null

        return $this;
    } // setUpdatedAt()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
            if ($this->prproj_is_public !== false) {
                return false;
            }

        // otherwise, everything was equal, so return TRUE
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array   $row       The row returned by DataFetcher->fetch().
     * @param int     $startcol  0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @param string  $indexType The index type of $row. Mostly DataFetcher->getIndexType().
                                  One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                            TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false, $indexType = TableMap::TYPE_NUM)
    {
        try {

            $col = $row[TableMap::TYPE_NUM == $indexType ? 0 + $startcol : ProjectTableMap::translateFieldName('Id', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prproj_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 1 + $startcol : ProjectTableMap::translateFieldName('Name', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prproj_name = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 2 + $startcol : ProjectTableMap::translateFieldName('Slug', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prproj_slug = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 3 + $startcol : ProjectTableMap::translateFieldName('Description', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prproj_description = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 4 + $startcol : ProjectTableMap::translateFieldName('WebSite', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prproj_website = (null !== $col) ? (string) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 5 + $startcol : ProjectTableMap::translateFieldName('IsPublic', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prproj_is_public = (null !== $col) ? (boolean) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 6 + $startcol : ProjectTableMap::translateFieldName('CreatedByUserId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prproj_created_by_user_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 7 + $startcol : ProjectTableMap::translateFieldName('UpdatedByUserId', TableMap::TYPE_PHPNAME, $indexType)];
            $this->prproj_updated_by_user_id = (null !== $col) ? (int) $col : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 8 + $startcol : ProjectTableMap::translateFieldName('CreatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->prproj_created_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;

            $col = $row[TableMap::TYPE_NUM == $indexType ? 9 + $startcol : ProjectTableMap::translateFieldName('UpdatedAt', TableMap::TYPE_PHPNAME, $indexType)];
            if ($col === '0000-00-00 00:00:00') {
                $col = null;
            }
            $this->prproj_updated_at = (null !== $col) ? PropelDateTime::newInstance($col, null, 'DateTime') : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }

            return $startcol + 10; // 10 = ProjectTableMap::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException(sprintf('Error populating %s object', '\\IiMedias\\ProjectBundle\\Model\\Project'), 0, $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {
        if ($this->aCreatedByUser !== null && $this->prproj_created_by_user_id !== $this->aCreatedByUser->getId()) {
            $this->aCreatedByUser = null;
        }
        if ($this->aUpdatedByUser !== null && $this->prproj_updated_by_user_id !== $this->aUpdatedByUser->getId()) {
            $this->aUpdatedByUser = null;
        }
    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param      boolean $deep (optional) Whether to also de-associated any related objects.
     * @param      ConnectionInterface $con (optional) The ConnectionInterface connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ProjectTableMap::DATABASE_NAME);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $dataFetcher = ChildProjectQuery::create(null, $this->buildPkeyCriteria())->setFormatter(ModelCriteria::FORMAT_STATEMENT)->find($con);
        $row = $dataFetcher->fetch();
        $dataFetcher->close();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true, $dataFetcher->getIndexType()); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->aCreatedByUser = null;
            $this->aUpdatedByUser = null;
            $this->collProjectMembers = null;

            $this->collProjectIssues = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param      ConnectionInterface $con
     * @return void
     * @throws PropelException
     * @see Project::setDeleted()
     * @see Project::isDeleted()
     */
    public function delete(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProjectTableMap::DATABASE_NAME);
        }

        $con->transaction(function () use ($con) {
            $deleteQuery = ChildProjectQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $this->setDeleted(true);
            }
        });
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see doSave()
     */
    public function save(ConnectionInterface $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProjectTableMap::DATABASE_NAME);
        }

        return $con->transaction(function () use ($con) {
            $ret = $this->preSave($con);
            $isInsert = $this->isNew();
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
                // timestampable behavior

                if (!$this->isColumnModified(ProjectTableMap::COL_PRPROJ_CREATED_AT)) {
                    $this->setCreatedAt(time());
                }
                if (!$this->isColumnModified(ProjectTableMap::COL_PRPROJ_UPDATED_AT)) {
                    $this->setUpdatedAt(time());
                }
            } else {
                $ret = $ret && $this->preUpdate($con);
                // timestampable behavior
                if ($this->isModified() && !$this->isColumnModified(ProjectTableMap::COL_PRPROJ_UPDATED_AT)) {
                    $this->setUpdatedAt(time());
                }
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ProjectTableMap::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }

            return $affectedRows;
        });
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param      ConnectionInterface $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see save()
     */
    protected function doSave(ConnectionInterface $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            // We call the save method on the following object(s) if they
            // were passed to this object by their corresponding set
            // method.  This object relates to these object(s) by a
            // foreign key reference.

            if ($this->aCreatedByUser !== null) {
                if ($this->aCreatedByUser->isModified() || $this->aCreatedByUser->isNew()) {
                    $affectedRows += $this->aCreatedByUser->save($con);
                }
                $this->setCreatedByUser($this->aCreatedByUser);
            }

            if ($this->aUpdatedByUser !== null) {
                if ($this->aUpdatedByUser->isModified() || $this->aUpdatedByUser->isNew()) {
                    $affectedRows += $this->aUpdatedByUser->save($con);
                }
                $this->setUpdatedByUser($this->aUpdatedByUser);
            }

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                    $affectedRows += 1;
                } else {
                    $affectedRows += $this->doUpdate($con);
                }
                $this->resetModified();
            }

            if ($this->projectMembersScheduledForDeletion !== null) {
                if (!$this->projectMembersScheduledForDeletion->isEmpty()) {
                    \IiMedias\ProjectBundle\Model\ProjectMemberQuery::create()
                        ->filterByPrimaryKeys($this->projectMembersScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->projectMembersScheduledForDeletion = null;
                }
            }

            if ($this->collProjectMembers !== null) {
                foreach ($this->collProjectMembers as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->projectIssuesScheduledForDeletion !== null) {
                if (!$this->projectIssuesScheduledForDeletion->isEmpty()) {
                    \IiMedias\ProjectBundle\Model\ProjectIssueQuery::create()
                        ->filterByPrimaryKeys($this->projectIssuesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->projectIssuesScheduledForDeletion = null;
                }
            }

            if ($this->collProjectIssues !== null) {
                foreach ($this->collProjectIssues as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @throws PropelException
     * @see doSave()
     */
    protected function doInsert(ConnectionInterface $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[ProjectTableMap::COL_PRPROJ_ID] = true;
        if (null !== $this->prproj_id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ProjectTableMap::COL_PRPROJ_ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ProjectTableMap::COL_PRPROJ_ID)) {
            $modifiedColumns[':p' . $index++]  = 'prproj_id';
        }
        if ($this->isColumnModified(ProjectTableMap::COL_PRPROJ_NAME)) {
            $modifiedColumns[':p' . $index++]  = 'prproj_name';
        }
        if ($this->isColumnModified(ProjectTableMap::COL_PRPROJ_SLUG)) {
            $modifiedColumns[':p' . $index++]  = 'prproj_slug';
        }
        if ($this->isColumnModified(ProjectTableMap::COL_PRPROJ_DESCRIPTION)) {
            $modifiedColumns[':p' . $index++]  = 'prproj_description';
        }
        if ($this->isColumnModified(ProjectTableMap::COL_PRPROJ_WEBSITE)) {
            $modifiedColumns[':p' . $index++]  = 'prproj_website';
        }
        if ($this->isColumnModified(ProjectTableMap::COL_PRPROJ_IS_PUBLIC)) {
            $modifiedColumns[':p' . $index++]  = 'prproj_is_public';
        }
        if ($this->isColumnModified(ProjectTableMap::COL_PRPROJ_CREATED_BY_USER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'prproj_created_by_user_id';
        }
        if ($this->isColumnModified(ProjectTableMap::COL_PRPROJ_UPDATED_BY_USER_ID)) {
            $modifiedColumns[':p' . $index++]  = 'prproj_updated_by_user_id';
        }
        if ($this->isColumnModified(ProjectTableMap::COL_PRPROJ_CREATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'prproj_created_at';
        }
        if ($this->isColumnModified(ProjectTableMap::COL_PRPROJ_UPDATED_AT)) {
            $modifiedColumns[':p' . $index++]  = 'prproj_updated_at';
        }

        $sql = sprintf(
            'INSERT INTO project_project_prproj (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case 'prproj_id':
                        $stmt->bindValue($identifier, $this->prproj_id, PDO::PARAM_INT);
                        break;
                    case 'prproj_name':
                        $stmt->bindValue($identifier, $this->prproj_name, PDO::PARAM_STR);
                        break;
                    case 'prproj_slug':
                        $stmt->bindValue($identifier, $this->prproj_slug, PDO::PARAM_STR);
                        break;
                    case 'prproj_description':
                        $stmt->bindValue($identifier, $this->prproj_description, PDO::PARAM_STR);
                        break;
                    case 'prproj_website':
                        $stmt->bindValue($identifier, $this->prproj_website, PDO::PARAM_STR);
                        break;
                    case 'prproj_is_public':
                        $stmt->bindValue($identifier, (int) $this->prproj_is_public, PDO::PARAM_INT);
                        break;
                    case 'prproj_created_by_user_id':
                        $stmt->bindValue($identifier, $this->prproj_created_by_user_id, PDO::PARAM_INT);
                        break;
                    case 'prproj_updated_by_user_id':
                        $stmt->bindValue($identifier, $this->prproj_updated_by_user_id, PDO::PARAM_INT);
                        break;
                    case 'prproj_created_at':
                        $stmt->bindValue($identifier, $this->prproj_created_at ? $this->prproj_created_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                    case 'prproj_updated_at':
                        $stmt->bindValue($identifier, $this->prproj_updated_at ? $this->prproj_updated_at->format("Y-m-d H:i:s.u") : null, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), 0, $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', 0, $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param      ConnectionInterface $con
     *
     * @return Integer Number of updated rows
     * @see doSave()
     */
    protected function doUpdate(ConnectionInterface $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();

        return $selectCriteria->doUpdate($valuesCriteria, $con);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param      string $name name
     * @param      string $type The type of fieldname the $name is of:
     *                     one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                     TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                     Defaults to TableMap::TYPE_PHPNAME.
     * @return mixed Value of field.
     */
    public function getByName($name, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ProjectTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param      int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getName();
                break;
            case 2:
                return $this->getSlug();
                break;
            case 3:
                return $this->getDescription();
                break;
            case 4:
                return $this->getWebSite();
                break;
            case 5:
                return $this->getIsPublic();
                break;
            case 6:
                return $this->getCreatedByUserId();
                break;
            case 7:
                return $this->getUpdatedByUserId();
                break;
            case 8:
                return $this->getCreatedAt();
                break;
            case 9:
                return $this->getUpdatedAt();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     *                    TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                    Defaults to TableMap::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to TRUE.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = TableMap::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {

        if (isset($alreadyDumpedObjects['Project'][$this->hashCode()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Project'][$this->hashCode()] = true;
        $keys = ProjectTableMap::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getName(),
            $keys[2] => $this->getSlug(),
            $keys[3] => $this->getDescription(),
            $keys[4] => $this->getWebSite(),
            $keys[5] => $this->getIsPublic(),
            $keys[6] => $this->getCreatedByUserId(),
            $keys[7] => $this->getUpdatedByUserId(),
            $keys[8] => $this->getCreatedAt(),
            $keys[9] => $this->getUpdatedAt(),
        );
        if ($result[$keys[8]] instanceof \DateTime) {
            $result[$keys[8]] = $result[$keys[8]]->format('c');
        }

        if ($result[$keys[9]] instanceof \DateTime) {
            $result[$keys[9]] = $result[$keys[9]]->format('c');
        }

        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->aCreatedByUser) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'user';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user_user_usrusr';
                        break;
                    default:
                        $key = 'CreatedByUser';
                }

                $result[$key] = $this->aCreatedByUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->aUpdatedByUser) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'user';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'user_user_usrusr';
                        break;
                    default:
                        $key = 'UpdatedByUser';
                }

                $result[$key] = $this->aUpdatedByUser->toArray($keyType, $includeLazyLoadColumns,  $alreadyDumpedObjects, true);
            }
            if (null !== $this->collProjectMembers) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'projectMembers';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'project_member_prmmbrs';
                        break;
                    default:
                        $key = 'ProjectMembers';
                }

                $result[$key] = $this->collProjectMembers->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collProjectIssues) {

                switch ($keyType) {
                    case TableMap::TYPE_CAMELNAME:
                        $key = 'projectIssues';
                        break;
                    case TableMap::TYPE_FIELDNAME:
                        $key = 'project_issue_prissus';
                        break;
                    default:
                        $key = 'ProjectIssues';
                }

                $result[$key] = $this->collProjectIssues->toArray(null, false, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param  string $name
     * @param  mixed  $value field value
     * @param  string $type The type of fieldname the $name is of:
     *                one of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME
     *                TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     *                Defaults to TableMap::TYPE_PHPNAME.
     * @return $this|\IiMedias\ProjectBundle\Model\Project
     */
    public function setByName($name, $value, $type = TableMap::TYPE_PHPNAME)
    {
        $pos = ProjectTableMap::translateFieldName($name, $type, TableMap::TYPE_NUM);

        return $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param  int $pos position in xml schema
     * @param  mixed $value field value
     * @return $this|\IiMedias\ProjectBundle\Model\Project
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setName($value);
                break;
            case 2:
                $this->setSlug($value);
                break;
            case 3:
                $this->setDescription($value);
                break;
            case 4:
                $this->setWebSite($value);
                break;
            case 5:
                $this->setIsPublic($value);
                break;
            case 6:
                $this->setCreatedByUserId($value);
                break;
            case 7:
                $this->setUpdatedByUserId($value);
                break;
            case 8:
                $this->setCreatedAt($value);
                break;
            case 9:
                $this->setUpdatedAt($value);
                break;
        } // switch()

        return $this;
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param      array  $arr     An array to populate the object from.
     * @param      string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = TableMap::TYPE_PHPNAME)
    {
        $keys = ProjectTableMap::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) {
            $this->setId($arr[$keys[0]]);
        }
        if (array_key_exists($keys[1], $arr)) {
            $this->setName($arr[$keys[1]]);
        }
        if (array_key_exists($keys[2], $arr)) {
            $this->setSlug($arr[$keys[2]]);
        }
        if (array_key_exists($keys[3], $arr)) {
            $this->setDescription($arr[$keys[3]]);
        }
        if (array_key_exists($keys[4], $arr)) {
            $this->setWebSite($arr[$keys[4]]);
        }
        if (array_key_exists($keys[5], $arr)) {
            $this->setIsPublic($arr[$keys[5]]);
        }
        if (array_key_exists($keys[6], $arr)) {
            $this->setCreatedByUserId($arr[$keys[6]]);
        }
        if (array_key_exists($keys[7], $arr)) {
            $this->setUpdatedByUserId($arr[$keys[7]]);
        }
        if (array_key_exists($keys[8], $arr)) {
            $this->setCreatedAt($arr[$keys[8]]);
        }
        if (array_key_exists($keys[9], $arr)) {
            $this->setUpdatedAt($arr[$keys[9]]);
        }
    }

     /**
     * Populate the current object from a string, using a given parser format
     * <code>
     * $book = new Book();
     * $book->importFrom('JSON', '{"Id":9012,"Title":"Don Juan","ISBN":"0140422161","Price":12.99,"PublisherId":1234,"AuthorId":5678}');
     * </code>
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants TableMap::TYPE_PHPNAME, TableMap::TYPE_CAMELNAME,
     * TableMap::TYPE_COLNAME, TableMap::TYPE_FIELDNAME, TableMap::TYPE_NUM.
     * The default key type is the column's TableMap::TYPE_PHPNAME.
     *
     * @param mixed $parser A AbstractParser instance,
     *                       or a format name ('XML', 'YAML', 'JSON', 'CSV')
     * @param string $data The source data to import from
     * @param string $keyType The type of keys the array uses.
     *
     * @return $this|\IiMedias\ProjectBundle\Model\Project The current object, for fluid interface
     */
    public function importFrom($parser, $data, $keyType = TableMap::TYPE_PHPNAME)
    {
        if (!$parser instanceof AbstractParser) {
            $parser = AbstractParser::getParser($parser);
        }

        $this->fromArray($parser->toArray($data), $keyType);

        return $this;
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ProjectTableMap::DATABASE_NAME);

        if ($this->isColumnModified(ProjectTableMap::COL_PRPROJ_ID)) {
            $criteria->add(ProjectTableMap::COL_PRPROJ_ID, $this->prproj_id);
        }
        if ($this->isColumnModified(ProjectTableMap::COL_PRPROJ_NAME)) {
            $criteria->add(ProjectTableMap::COL_PRPROJ_NAME, $this->prproj_name);
        }
        if ($this->isColumnModified(ProjectTableMap::COL_PRPROJ_SLUG)) {
            $criteria->add(ProjectTableMap::COL_PRPROJ_SLUG, $this->prproj_slug);
        }
        if ($this->isColumnModified(ProjectTableMap::COL_PRPROJ_DESCRIPTION)) {
            $criteria->add(ProjectTableMap::COL_PRPROJ_DESCRIPTION, $this->prproj_description);
        }
        if ($this->isColumnModified(ProjectTableMap::COL_PRPROJ_WEBSITE)) {
            $criteria->add(ProjectTableMap::COL_PRPROJ_WEBSITE, $this->prproj_website);
        }
        if ($this->isColumnModified(ProjectTableMap::COL_PRPROJ_IS_PUBLIC)) {
            $criteria->add(ProjectTableMap::COL_PRPROJ_IS_PUBLIC, $this->prproj_is_public);
        }
        if ($this->isColumnModified(ProjectTableMap::COL_PRPROJ_CREATED_BY_USER_ID)) {
            $criteria->add(ProjectTableMap::COL_PRPROJ_CREATED_BY_USER_ID, $this->prproj_created_by_user_id);
        }
        if ($this->isColumnModified(ProjectTableMap::COL_PRPROJ_UPDATED_BY_USER_ID)) {
            $criteria->add(ProjectTableMap::COL_PRPROJ_UPDATED_BY_USER_ID, $this->prproj_updated_by_user_id);
        }
        if ($this->isColumnModified(ProjectTableMap::COL_PRPROJ_CREATED_AT)) {
            $criteria->add(ProjectTableMap::COL_PRPROJ_CREATED_AT, $this->prproj_created_at);
        }
        if ($this->isColumnModified(ProjectTableMap::COL_PRPROJ_UPDATED_AT)) {
            $criteria->add(ProjectTableMap::COL_PRPROJ_UPDATED_AT, $this->prproj_updated_at);
        }

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @throws LogicException if no primary key is defined
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = ChildProjectQuery::create();
        $criteria->add(ProjectTableMap::COL_PRPROJ_ID, $this->prproj_id);

        return $criteria;
    }

    /**
     * If the primary key is not null, return the hashcode of the
     * primary key. Otherwise, return the hash code of the object.
     *
     * @return int Hashcode
     */
    public function hashCode()
    {
        $validPk = null !== $this->getId();

        $validPrimaryKeyFKs = 0;
        $primaryKeyFKs = [];

        if ($validPk) {
            return crc32(json_encode($this->getPrimaryKey(), JSON_UNESCAPED_UNICODE));
        } elseif ($validPrimaryKeyFKs) {
            return crc32(json_encode($primaryKeyFKs, JSON_UNESCAPED_UNICODE));
        }

        return spl_object_hash($this);
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (prproj_id column).
     *
     * @param       int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {
        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param      object $copyObj An object of \IiMedias\ProjectBundle\Model\Project (or compatible) type.
     * @param      boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param      boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setName($this->getName());
        $copyObj->setSlug($this->getSlug());
        $copyObj->setDescription($this->getDescription());
        $copyObj->setWebSite($this->getWebSite());
        $copyObj->setIsPublic($this->getIsPublic());
        $copyObj->setCreatedByUserId($this->getCreatedByUserId());
        $copyObj->setUpdatedByUserId($this->getUpdatedByUserId());
        $copyObj->setCreatedAt($this->getCreatedAt());
        $copyObj->setUpdatedAt($this->getUpdatedAt());

        if ($deepCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);

            foreach ($this->getProjectMembers() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProjectMember($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getProjectIssues() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addProjectIssue($relObj->copy($deepCopy));
                }
            }

        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param  boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return \IiMedias\ProjectBundle\Model\Project Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Declares an association between this object and a User object.
     *
     * @param  User $v
     * @return $this|\IiMedias\ProjectBundle\Model\Project The current object (for fluent API support)
     * @throws PropelException
     */
    public function setCreatedByUser(User $v = null)
    {
        if ($v === null) {
            $this->setCreatedByUserId(NULL);
        } else {
            $this->setCreatedByUserId($v->getId());
        }

        $this->aCreatedByUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the User object, it will not be re-added.
        if ($v !== null) {
            $v->addCreatedByUserProject($this);
        }


        return $this;
    }


    /**
     * Get the associated User object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return User The associated User object.
     * @throws PropelException
     */
    public function getCreatedByUser(ConnectionInterface $con = null)
    {
        if ($this->aCreatedByUser === null && ($this->prproj_created_by_user_id !== null)) {
            $this->aCreatedByUser = UserQuery::create()->findPk($this->prproj_created_by_user_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aCreatedByUser->addCreatedByUserProjects($this);
             */
        }

        return $this->aCreatedByUser;
    }

    /**
     * Declares an association between this object and a User object.
     *
     * @param  User $v
     * @return $this|\IiMedias\ProjectBundle\Model\Project The current object (for fluent API support)
     * @throws PropelException
     */
    public function setUpdatedByUser(User $v = null)
    {
        if ($v === null) {
            $this->setUpdatedByUserId(NULL);
        } else {
            $this->setUpdatedByUserId($v->getId());
        }

        $this->aUpdatedByUser = $v;

        // Add binding for other direction of this n:n relationship.
        // If this object has already been added to the User object, it will not be re-added.
        if ($v !== null) {
            $v->addUpdatedByUserProject($this);
        }


        return $this;
    }


    /**
     * Get the associated User object
     *
     * @param  ConnectionInterface $con Optional Connection object.
     * @return User The associated User object.
     * @throws PropelException
     */
    public function getUpdatedByUser(ConnectionInterface $con = null)
    {
        if ($this->aUpdatedByUser === null && ($this->prproj_updated_by_user_id !== null)) {
            $this->aUpdatedByUser = UserQuery::create()->findPk($this->prproj_updated_by_user_id, $con);
            /* The following can be used additionally to
                guarantee the related object contains a reference
                to this object.  This level of coupling may, however, be
                undesirable since it could result in an only partially populated collection
                in the referenced object.
                $this->aUpdatedByUser->addUpdatedByUserProjects($this);
             */
        }

        return $this->aUpdatedByUser;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param      string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('ProjectMember' == $relationName) {
            return $this->initProjectMembers();
        }
        if ('ProjectIssue' == $relationName) {
            return $this->initProjectIssues();
        }
    }

    /**
     * Clears out the collProjectMembers collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProjectMembers()
     */
    public function clearProjectMembers()
    {
        $this->collProjectMembers = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProjectMembers collection loaded partially.
     */
    public function resetPartialProjectMembers($v = true)
    {
        $this->collProjectMembersPartial = $v;
    }

    /**
     * Initializes the collProjectMembers collection.
     *
     * By default this just sets the collProjectMembers collection to an empty array (like clearcollProjectMembers());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProjectMembers($overrideExisting = true)
    {
        if (null !== $this->collProjectMembers && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProjectMemberTableMap::getTableMap()->getCollectionClassName();

        $this->collProjectMembers = new $collectionClassName;
        $this->collProjectMembers->setModel('\IiMedias\ProjectBundle\Model\ProjectMember');
    }

    /**
     * Gets an array of ChildProjectMember objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProject is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildProjectMember[] List of ChildProjectMember objects
     * @throws PropelException
     */
    public function getProjectMembers(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProjectMembersPartial && !$this->isNew();
        if (null === $this->collProjectMembers || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collProjectMembers) {
                // return empty collection
                $this->initProjectMembers();
            } else {
                $collProjectMembers = ChildProjectMemberQuery::create(null, $criteria)
                    ->filterByProject($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProjectMembersPartial && count($collProjectMembers)) {
                        $this->initProjectMembers(false);

                        foreach ($collProjectMembers as $obj) {
                            if (false == $this->collProjectMembers->contains($obj)) {
                                $this->collProjectMembers->append($obj);
                            }
                        }

                        $this->collProjectMembersPartial = true;
                    }

                    return $collProjectMembers;
                }

                if ($partial && $this->collProjectMembers) {
                    foreach ($this->collProjectMembers as $obj) {
                        if ($obj->isNew()) {
                            $collProjectMembers[] = $obj;
                        }
                    }
                }

                $this->collProjectMembers = $collProjectMembers;
                $this->collProjectMembersPartial = false;
            }
        }

        return $this->collProjectMembers;
    }

    /**
     * Sets a collection of ChildProjectMember objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $projectMembers A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProject The current object (for fluent API support)
     */
    public function setProjectMembers(Collection $projectMembers, ConnectionInterface $con = null)
    {
        /** @var ChildProjectMember[] $projectMembersToDelete */
        $projectMembersToDelete = $this->getProjectMembers(new Criteria(), $con)->diff($projectMembers);


        $this->projectMembersScheduledForDeletion = $projectMembersToDelete;

        foreach ($projectMembersToDelete as $projectMemberRemoved) {
            $projectMemberRemoved->setProject(null);
        }

        $this->collProjectMembers = null;
        foreach ($projectMembers as $projectMember) {
            $this->addProjectMember($projectMember);
        }

        $this->collProjectMembers = $projectMembers;
        $this->collProjectMembersPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ProjectMember objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ProjectMember objects.
     * @throws PropelException
     */
    public function countProjectMembers(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProjectMembersPartial && !$this->isNew();
        if (null === $this->collProjectMembers || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProjectMembers) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProjectMembers());
            }

            $query = ChildProjectMemberQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProject($this)
                ->count($con);
        }

        return count($this->collProjectMembers);
    }

    /**
     * Method called to associate a ChildProjectMember object to this object
     * through the ChildProjectMember foreign key attribute.
     *
     * @param  ChildProjectMember $l ChildProjectMember
     * @return $this|\IiMedias\ProjectBundle\Model\Project The current object (for fluent API support)
     */
    public function addProjectMember(ChildProjectMember $l)
    {
        if ($this->collProjectMembers === null) {
            $this->initProjectMembers();
            $this->collProjectMembersPartial = true;
        }

        if (!$this->collProjectMembers->contains($l)) {
            $this->doAddProjectMember($l);

            if ($this->projectMembersScheduledForDeletion and $this->projectMembersScheduledForDeletion->contains($l)) {
                $this->projectMembersScheduledForDeletion->remove($this->projectMembersScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildProjectMember $projectMember The ChildProjectMember object to add.
     */
    protected function doAddProjectMember(ChildProjectMember $projectMember)
    {
        $this->collProjectMembers[]= $projectMember;
        $projectMember->setProject($this);
    }

    /**
     * @param  ChildProjectMember $projectMember The ChildProjectMember object to remove.
     * @return $this|ChildProject The current object (for fluent API support)
     */
    public function removeProjectMember(ChildProjectMember $projectMember)
    {
        if ($this->getProjectMembers()->contains($projectMember)) {
            $pos = $this->collProjectMembers->search($projectMember);
            $this->collProjectMembers->remove($pos);
            if (null === $this->projectMembersScheduledForDeletion) {
                $this->projectMembersScheduledForDeletion = clone $this->collProjectMembers;
                $this->projectMembersScheduledForDeletion->clear();
            }
            $this->projectMembersScheduledForDeletion[]= clone $projectMember;
            $projectMember->setProject(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Project is new, it will return
     * an empty collection; or if this Project has previously
     * been saved, it will retrieve related ProjectMembers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Project.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProjectMember[] List of ChildProjectMember objects
     */
    public function getProjectMembersJoinUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProjectMemberQuery::create(null, $criteria);
        $query->joinWith('User', $joinBehavior);

        return $this->getProjectMembers($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Project is new, it will return
     * an empty collection; or if this Project has previously
     * been saved, it will retrieve related ProjectMembers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Project.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProjectMember[] List of ChildProjectMember objects
     */
    public function getProjectMembersJoinCreatedByUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProjectMemberQuery::create(null, $criteria);
        $query->joinWith('CreatedByUser', $joinBehavior);

        return $this->getProjectMembers($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Project is new, it will return
     * an empty collection; or if this Project has previously
     * been saved, it will retrieve related ProjectMembers from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Project.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProjectMember[] List of ChildProjectMember objects
     */
    public function getProjectMembersJoinUpdatedByUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProjectMemberQuery::create(null, $criteria);
        $query->joinWith('UpdatedByUser', $joinBehavior);

        return $this->getProjectMembers($query, $con);
    }

    /**
     * Clears out the collProjectIssues collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return void
     * @see        addProjectIssues()
     */
    public function clearProjectIssues()
    {
        $this->collProjectIssues = null; // important to set this to NULL since that means it is uninitialized
    }

    /**
     * Reset is the collProjectIssues collection loaded partially.
     */
    public function resetPartialProjectIssues($v = true)
    {
        $this->collProjectIssuesPartial = $v;
    }

    /**
     * Initializes the collProjectIssues collection.
     *
     * By default this just sets the collProjectIssues collection to an empty array (like clearcollProjectIssues());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param      boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initProjectIssues($overrideExisting = true)
    {
        if (null !== $this->collProjectIssues && !$overrideExisting) {
            return;
        }

        $collectionClassName = ProjectIssueTableMap::getTableMap()->getCollectionClassName();

        $this->collProjectIssues = new $collectionClassName;
        $this->collProjectIssues->setModel('\IiMedias\ProjectBundle\Model\ProjectIssue');
    }

    /**
     * Gets an array of ChildProjectIssue objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ChildProject is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @return ObjectCollection|ChildProjectIssue[] List of ChildProjectIssue objects
     * @throws PropelException
     */
    public function getProjectIssues(Criteria $criteria = null, ConnectionInterface $con = null)
    {
        $partial = $this->collProjectIssuesPartial && !$this->isNew();
        if (null === $this->collProjectIssues || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collProjectIssues) {
                // return empty collection
                $this->initProjectIssues();
            } else {
                $collProjectIssues = ChildProjectIssueQuery::create(null, $criteria)
                    ->filterByProject($this)
                    ->find($con);

                if (null !== $criteria) {
                    if (false !== $this->collProjectIssuesPartial && count($collProjectIssues)) {
                        $this->initProjectIssues(false);

                        foreach ($collProjectIssues as $obj) {
                            if (false == $this->collProjectIssues->contains($obj)) {
                                $this->collProjectIssues->append($obj);
                            }
                        }

                        $this->collProjectIssuesPartial = true;
                    }

                    return $collProjectIssues;
                }

                if ($partial && $this->collProjectIssues) {
                    foreach ($this->collProjectIssues as $obj) {
                        if ($obj->isNew()) {
                            $collProjectIssues[] = $obj;
                        }
                    }
                }

                $this->collProjectIssues = $collProjectIssues;
                $this->collProjectIssuesPartial = false;
            }
        }

        return $this->collProjectIssues;
    }

    /**
     * Sets a collection of ChildProjectIssue objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param      Collection $projectIssues A Propel collection.
     * @param      ConnectionInterface $con Optional connection object
     * @return $this|ChildProject The current object (for fluent API support)
     */
    public function setProjectIssues(Collection $projectIssues, ConnectionInterface $con = null)
    {
        /** @var ChildProjectIssue[] $projectIssuesToDelete */
        $projectIssuesToDelete = $this->getProjectIssues(new Criteria(), $con)->diff($projectIssues);


        $this->projectIssuesScheduledForDeletion = $projectIssuesToDelete;

        foreach ($projectIssuesToDelete as $projectIssueRemoved) {
            $projectIssueRemoved->setProject(null);
        }

        $this->collProjectIssues = null;
        foreach ($projectIssues as $projectIssue) {
            $this->addProjectIssue($projectIssue);
        }

        $this->collProjectIssues = $projectIssues;
        $this->collProjectIssuesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related ProjectIssue objects.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct
     * @param      ConnectionInterface $con
     * @return int             Count of related ProjectIssue objects.
     * @throws PropelException
     */
    public function countProjectIssues(Criteria $criteria = null, $distinct = false, ConnectionInterface $con = null)
    {
        $partial = $this->collProjectIssuesPartial && !$this->isNew();
        if (null === $this->collProjectIssues || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collProjectIssues) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getProjectIssues());
            }

            $query = ChildProjectIssueQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProject($this)
                ->count($con);
        }

        return count($this->collProjectIssues);
    }

    /**
     * Method called to associate a ChildProjectIssue object to this object
     * through the ChildProjectIssue foreign key attribute.
     *
     * @param  ChildProjectIssue $l ChildProjectIssue
     * @return $this|\IiMedias\ProjectBundle\Model\Project The current object (for fluent API support)
     */
    public function addProjectIssue(ChildProjectIssue $l)
    {
        if ($this->collProjectIssues === null) {
            $this->initProjectIssues();
            $this->collProjectIssuesPartial = true;
        }

        if (!$this->collProjectIssues->contains($l)) {
            $this->doAddProjectIssue($l);

            if ($this->projectIssuesScheduledForDeletion and $this->projectIssuesScheduledForDeletion->contains($l)) {
                $this->projectIssuesScheduledForDeletion->remove($this->projectIssuesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param ChildProjectIssue $projectIssue The ChildProjectIssue object to add.
     */
    protected function doAddProjectIssue(ChildProjectIssue $projectIssue)
    {
        $this->collProjectIssues[]= $projectIssue;
        $projectIssue->setProject($this);
    }

    /**
     * @param  ChildProjectIssue $projectIssue The ChildProjectIssue object to remove.
     * @return $this|ChildProject The current object (for fluent API support)
     */
    public function removeProjectIssue(ChildProjectIssue $projectIssue)
    {
        if ($this->getProjectIssues()->contains($projectIssue)) {
            $pos = $this->collProjectIssues->search($projectIssue);
            $this->collProjectIssues->remove($pos);
            if (null === $this->projectIssuesScheduledForDeletion) {
                $this->projectIssuesScheduledForDeletion = clone $this->collProjectIssues;
                $this->projectIssuesScheduledForDeletion->clear();
            }
            $this->projectIssuesScheduledForDeletion[]= clone $projectIssue;
            $projectIssue->setProject(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Project is new, it will return
     * an empty collection; or if this Project has previously
     * been saved, it will retrieve related ProjectIssues from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Project.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProjectIssue[] List of ChildProjectIssue objects
     */
    public function getProjectIssuesJoinProjectIssueParent(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProjectIssueQuery::create(null, $criteria);
        $query->joinWith('ProjectIssueParent', $joinBehavior);

        return $this->getProjectIssues($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Project is new, it will return
     * an empty collection; or if this Project has previously
     * been saved, it will retrieve related ProjectIssues from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Project.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProjectIssue[] List of ChildProjectIssue objects
     */
    public function getProjectIssuesJoinProjectIssueType(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProjectIssueQuery::create(null, $criteria);
        $query->joinWith('ProjectIssueType', $joinBehavior);

        return $this->getProjectIssues($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Project is new, it will return
     * an empty collection; or if this Project has previously
     * been saved, it will retrieve related ProjectIssues from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Project.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProjectIssue[] List of ChildProjectIssue objects
     */
    public function getProjectIssuesJoinProjectIssueStatus(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProjectIssueQuery::create(null, $criteria);
        $query->joinWith('ProjectIssueStatus', $joinBehavior);

        return $this->getProjectIssues($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Project is new, it will return
     * an empty collection; or if this Project has previously
     * been saved, it will retrieve related ProjectIssues from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Project.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProjectIssue[] List of ChildProjectIssue objects
     */
    public function getProjectIssuesJoinProjectIssuePriority(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProjectIssueQuery::create(null, $criteria);
        $query->joinWith('ProjectIssuePriority', $joinBehavior);

        return $this->getProjectIssues($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Project is new, it will return
     * an empty collection; or if this Project has previously
     * been saved, it will retrieve related ProjectIssues from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Project.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProjectIssue[] List of ChildProjectIssue objects
     */
    public function getProjectIssuesJoinAssignedUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProjectIssueQuery::create(null, $criteria);
        $query->joinWith('AssignedUser', $joinBehavior);

        return $this->getProjectIssues($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Project is new, it will return
     * an empty collection; or if this Project has previously
     * been saved, it will retrieve related ProjectIssues from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Project.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProjectIssue[] List of ChildProjectIssue objects
     */
    public function getProjectIssuesJoinCreatedByUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProjectIssueQuery::create(null, $criteria);
        $query->joinWith('CreatedByUser', $joinBehavior);

        return $this->getProjectIssues($query, $con);
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Project is new, it will return
     * an empty collection; or if this Project has previously
     * been saved, it will retrieve related ProjectIssues from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Project.
     *
     * @param      Criteria $criteria optional Criteria object to narrow the query
     * @param      ConnectionInterface $con optional connection object
     * @param      string $joinBehavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return ObjectCollection|ChildProjectIssue[] List of ChildProjectIssue objects
     */
    public function getProjectIssuesJoinUpdatedByUser(Criteria $criteria = null, ConnectionInterface $con = null, $joinBehavior = Criteria::LEFT_JOIN)
    {
        $query = ChildProjectIssueQuery::create(null, $criteria);
        $query->joinWith('UpdatedByUser', $joinBehavior);

        return $this->getProjectIssues($query, $con);
    }

    /**
     * Clears the current object, sets all attributes to their default values and removes
     * outgoing references as well as back-references (from other objects to this one. Results probably in a database
     * change of those foreign objects when you call `save` there).
     */
    public function clear()
    {
        if (null !== $this->aCreatedByUser) {
            $this->aCreatedByUser->removeCreatedByUserProject($this);
        }
        if (null !== $this->aUpdatedByUser) {
            $this->aUpdatedByUser->removeUpdatedByUserProject($this);
        }
        $this->prproj_id = null;
        $this->prproj_name = null;
        $this->prproj_slug = null;
        $this->prproj_description = null;
        $this->prproj_website = null;
        $this->prproj_is_public = null;
        $this->prproj_created_by_user_id = null;
        $this->prproj_updated_by_user_id = null;
        $this->prproj_created_at = null;
        $this->prproj_updated_at = null;
        $this->alreadyInSave = false;
        $this->clearAllReferences();
        $this->applyDefaultValues();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references and back-references to other model objects or collections of model objects.
     *
     * This method is used to reset all php object references (not the actual reference in the database).
     * Necessary for object serialisation.
     *
     * @param      boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep) {
            if ($this->collProjectMembers) {
                foreach ($this->collProjectMembers as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collProjectIssues) {
                foreach ($this->collProjectIssues as $o) {
                    $o->clearAllReferences($deep);
                }
            }
        } // if ($deep)

        $this->collProjectMembers = null;
        $this->collProjectIssues = null;
        $this->aCreatedByUser = null;
        $this->aUpdatedByUser = null;
    }

    /**
     * Return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ProjectTableMap::DEFAULT_STRING_FORMAT);
    }

    // timestampable behavior

    /**
     * Mark the current object so that the update date doesn't get updated during next save
     *
     * @return     $this|ChildProject The current object (for fluent API support)
     */
    public function keepUpdateDateUnchanged()
    {
        $this->modifiedColumns[ProjectTableMap::COL_PRPROJ_UPDATED_AT] = true;

        return $this;
    }

    /**
     * Code to be run before persisting the object
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preSave')) {
            return parent::preSave($con);
        }
        return true;
    }

    /**
     * Code to be run after persisting the object
     * @param ConnectionInterface $con
     */
    public function postSave(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postSave')) {
            parent::postSave($con);
        }
    }

    /**
     * Code to be run before inserting to database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preInsert')) {
            return parent::preInsert($con);
        }
        return true;
    }

    /**
     * Code to be run after inserting to database
     * @param ConnectionInterface $con
     */
    public function postInsert(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postInsert')) {
            parent::postInsert($con);
        }
    }

    /**
     * Code to be run before updating the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preUpdate')) {
            return parent::preUpdate($con);
        }
        return true;
    }

    /**
     * Code to be run after updating the object in database
     * @param ConnectionInterface $con
     */
    public function postUpdate(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postUpdate')) {
            parent::postUpdate($con);
        }
    }

    /**
     * Code to be run before deleting the object in database
     * @param  ConnectionInterface $con
     * @return boolean
     */
    public function preDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::preDelete')) {
            return parent::preDelete($con);
        }
        return true;
    }

    /**
     * Code to be run after deleting the object in database
     * @param ConnectionInterface $con
     */
    public function postDelete(ConnectionInterface $con = null)
    {
        if (is_callable('parent::postDelete')) {
            parent::postDelete($con);
        }
    }


    /**
     * Derived method to catches calls to undefined methods.
     *
     * Provides magic import/export method support (fromXML()/toXML(), fromYAML()/toYAML(), etc.).
     * Allows to define default __call() behavior if you overwrite __call()
     *
     * @param string $name
     * @param mixed  $params
     *
     * @return array|string
     */
    public function __call($name, $params)
    {
        if (0 === strpos($name, 'get')) {
            $virtualColumn = substr($name, 3);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }

            $virtualColumn = lcfirst($virtualColumn);
            if ($this->hasVirtualColumn($virtualColumn)) {
                return $this->getVirtualColumn($virtualColumn);
            }
        }

        if (0 === strpos($name, 'from')) {
            $format = substr($name, 4);

            return $this->importFrom($format, reset($params));
        }

        if (0 === strpos($name, 'to')) {
            $format = substr($name, 2);
            $includeLazyLoadColumns = isset($params[0]) ? $params[0] : true;

            return $this->exportTo($format, $includeLazyLoadColumns);
        }

        throw new BadMethodCallException(sprintf('Call to undefined method: %s.', $name));
    }

}

<?php

namespace IiMedias\ProjectBundle\Model\Base;

use \Exception;
use \PDO;
use IiMedias\AdminBundle\Model\User;
use IiMedias\ProjectBundle\Model\ProjectIssueStatus as ChildProjectIssueStatus;
use IiMedias\ProjectBundle\Model\ProjectIssueStatusQuery as ChildProjectIssueStatusQuery;
use IiMedias\ProjectBundle\Model\Map\ProjectIssueStatusTableMap;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;

/**
 * Base class that represents a query for the 'project_issue_status_prisst' table.
 *
 *
 *
 * @method     ChildProjectIssueStatusQuery orderById($order = Criteria::ASC) Order by the prisst_id column
 * @method     ChildProjectIssueStatusQuery orderByName($order = Criteria::ASC) Order by the prisst_name column
 * @method     ChildProjectIssueStatusQuery orderByOrder($order = Criteria::ASC) Order by the prisst_order column
 * @method     ChildProjectIssueStatusQuery orderByCreatedByUserId($order = Criteria::ASC) Order by the prisst_created_by_user_id column
 * @method     ChildProjectIssueStatusQuery orderByUpdatedByUserId($order = Criteria::ASC) Order by the prisst_updated_by_user_id column
 * @method     ChildProjectIssueStatusQuery orderByCreatedAt($order = Criteria::ASC) Order by the prisst_created_at column
 * @method     ChildProjectIssueStatusQuery orderByUpdatedAt($order = Criteria::ASC) Order by the prisst_updated_at column
 *
 * @method     ChildProjectIssueStatusQuery groupById() Group by the prisst_id column
 * @method     ChildProjectIssueStatusQuery groupByName() Group by the prisst_name column
 * @method     ChildProjectIssueStatusQuery groupByOrder() Group by the prisst_order column
 * @method     ChildProjectIssueStatusQuery groupByCreatedByUserId() Group by the prisst_created_by_user_id column
 * @method     ChildProjectIssueStatusQuery groupByUpdatedByUserId() Group by the prisst_updated_by_user_id column
 * @method     ChildProjectIssueStatusQuery groupByCreatedAt() Group by the prisst_created_at column
 * @method     ChildProjectIssueStatusQuery groupByUpdatedAt() Group by the prisst_updated_at column
 *
 * @method     ChildProjectIssueStatusQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildProjectIssueStatusQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildProjectIssueStatusQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildProjectIssueStatusQuery leftJoinWith($relation) Adds a LEFT JOIN clause and with to the query
 * @method     ChildProjectIssueStatusQuery rightJoinWith($relation) Adds a RIGHT JOIN clause and with to the query
 * @method     ChildProjectIssueStatusQuery innerJoinWith($relation) Adds a INNER JOIN clause and with to the query
 *
 * @method     ChildProjectIssueStatusQuery leftJoinCreatedByUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the CreatedByUser relation
 * @method     ChildProjectIssueStatusQuery rightJoinCreatedByUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CreatedByUser relation
 * @method     ChildProjectIssueStatusQuery innerJoinCreatedByUser($relationAlias = null) Adds a INNER JOIN clause to the query using the CreatedByUser relation
 *
 * @method     ChildProjectIssueStatusQuery joinWithCreatedByUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the CreatedByUser relation
 *
 * @method     ChildProjectIssueStatusQuery leftJoinWithCreatedByUser() Adds a LEFT JOIN clause and with to the query using the CreatedByUser relation
 * @method     ChildProjectIssueStatusQuery rightJoinWithCreatedByUser() Adds a RIGHT JOIN clause and with to the query using the CreatedByUser relation
 * @method     ChildProjectIssueStatusQuery innerJoinWithCreatedByUser() Adds a INNER JOIN clause and with to the query using the CreatedByUser relation
 *
 * @method     ChildProjectIssueStatusQuery leftJoinUpdatedByUser($relationAlias = null) Adds a LEFT JOIN clause to the query using the UpdatedByUser relation
 * @method     ChildProjectIssueStatusQuery rightJoinUpdatedByUser($relationAlias = null) Adds a RIGHT JOIN clause to the query using the UpdatedByUser relation
 * @method     ChildProjectIssueStatusQuery innerJoinUpdatedByUser($relationAlias = null) Adds a INNER JOIN clause to the query using the UpdatedByUser relation
 *
 * @method     ChildProjectIssueStatusQuery joinWithUpdatedByUser($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the UpdatedByUser relation
 *
 * @method     ChildProjectIssueStatusQuery leftJoinWithUpdatedByUser() Adds a LEFT JOIN clause and with to the query using the UpdatedByUser relation
 * @method     ChildProjectIssueStatusQuery rightJoinWithUpdatedByUser() Adds a RIGHT JOIN clause and with to the query using the UpdatedByUser relation
 * @method     ChildProjectIssueStatusQuery innerJoinWithUpdatedByUser() Adds a INNER JOIN clause and with to the query using the UpdatedByUser relation
 *
 * @method     ChildProjectIssueStatusQuery leftJoinProjectIssue($relationAlias = null) Adds a LEFT JOIN clause to the query using the ProjectIssue relation
 * @method     ChildProjectIssueStatusQuery rightJoinProjectIssue($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ProjectIssue relation
 * @method     ChildProjectIssueStatusQuery innerJoinProjectIssue($relationAlias = null) Adds a INNER JOIN clause to the query using the ProjectIssue relation
 *
 * @method     ChildProjectIssueStatusQuery joinWithProjectIssue($joinType = Criteria::INNER_JOIN) Adds a join clause and with to the query using the ProjectIssue relation
 *
 * @method     ChildProjectIssueStatusQuery leftJoinWithProjectIssue() Adds a LEFT JOIN clause and with to the query using the ProjectIssue relation
 * @method     ChildProjectIssueStatusQuery rightJoinWithProjectIssue() Adds a RIGHT JOIN clause and with to the query using the ProjectIssue relation
 * @method     ChildProjectIssueStatusQuery innerJoinWithProjectIssue() Adds a INNER JOIN clause and with to the query using the ProjectIssue relation
 *
 * @method     \IiMedias\AdminBundle\Model\UserQuery|\IiMedias\ProjectBundle\Model\ProjectIssueQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildProjectIssueStatus findOne(ConnectionInterface $con = null) Return the first ChildProjectIssueStatus matching the query
 * @method     ChildProjectIssueStatus findOneOrCreate(ConnectionInterface $con = null) Return the first ChildProjectIssueStatus matching the query, or a new ChildProjectIssueStatus object populated from the query conditions when no match is found
 *
 * @method     ChildProjectIssueStatus findOneById(int $prisst_id) Return the first ChildProjectIssueStatus filtered by the prisst_id column
 * @method     ChildProjectIssueStatus findOneByName(string $prisst_name) Return the first ChildProjectIssueStatus filtered by the prisst_name column
 * @method     ChildProjectIssueStatus findOneByOrder(int $prisst_order) Return the first ChildProjectIssueStatus filtered by the prisst_order column
 * @method     ChildProjectIssueStatus findOneByCreatedByUserId(int $prisst_created_by_user_id) Return the first ChildProjectIssueStatus filtered by the prisst_created_by_user_id column
 * @method     ChildProjectIssueStatus findOneByUpdatedByUserId(int $prisst_updated_by_user_id) Return the first ChildProjectIssueStatus filtered by the prisst_updated_by_user_id column
 * @method     ChildProjectIssueStatus findOneByCreatedAt(string $prisst_created_at) Return the first ChildProjectIssueStatus filtered by the prisst_created_at column
 * @method     ChildProjectIssueStatus findOneByUpdatedAt(string $prisst_updated_at) Return the first ChildProjectIssueStatus filtered by the prisst_updated_at column *

 * @method     ChildProjectIssueStatus requirePk($key, ConnectionInterface $con = null) Return the ChildProjectIssueStatus by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssueStatus requireOne(ConnectionInterface $con = null) Return the first ChildProjectIssueStatus matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildProjectIssueStatus requireOneById(int $prisst_id) Return the first ChildProjectIssueStatus filtered by the prisst_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssueStatus requireOneByName(string $prisst_name) Return the first ChildProjectIssueStatus filtered by the prisst_name column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssueStatus requireOneByOrder(int $prisst_order) Return the first ChildProjectIssueStatus filtered by the prisst_order column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssueStatus requireOneByCreatedByUserId(int $prisst_created_by_user_id) Return the first ChildProjectIssueStatus filtered by the prisst_created_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssueStatus requireOneByUpdatedByUserId(int $prisst_updated_by_user_id) Return the first ChildProjectIssueStatus filtered by the prisst_updated_by_user_id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssueStatus requireOneByCreatedAt(string $prisst_created_at) Return the first ChildProjectIssueStatus filtered by the prisst_created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildProjectIssueStatus requireOneByUpdatedAt(string $prisst_updated_at) Return the first ChildProjectIssueStatus filtered by the prisst_updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildProjectIssueStatus[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildProjectIssueStatus objects based on current ModelCriteria
 * @method     ChildProjectIssueStatus[]|ObjectCollection findById(int $prisst_id) Return ChildProjectIssueStatus objects filtered by the prisst_id column
 * @method     ChildProjectIssueStatus[]|ObjectCollection findByName(string $prisst_name) Return ChildProjectIssueStatus objects filtered by the prisst_name column
 * @method     ChildProjectIssueStatus[]|ObjectCollection findByOrder(int $prisst_order) Return ChildProjectIssueStatus objects filtered by the prisst_order column
 * @method     ChildProjectIssueStatus[]|ObjectCollection findByCreatedByUserId(int $prisst_created_by_user_id) Return ChildProjectIssueStatus objects filtered by the prisst_created_by_user_id column
 * @method     ChildProjectIssueStatus[]|ObjectCollection findByUpdatedByUserId(int $prisst_updated_by_user_id) Return ChildProjectIssueStatus objects filtered by the prisst_updated_by_user_id column
 * @method     ChildProjectIssueStatus[]|ObjectCollection findByCreatedAt(string $prisst_created_at) Return ChildProjectIssueStatus objects filtered by the prisst_created_at column
 * @method     ChildProjectIssueStatus[]|ObjectCollection findByUpdatedAt(string $prisst_updated_at) Return ChildProjectIssueStatus objects filtered by the prisst_updated_at column
 * @method     ChildProjectIssueStatus[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class ProjectIssueStatusQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \IiMedias\ProjectBundle\Model\Base\ProjectIssueStatusQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'default', $modelName = '\\IiMedias\\ProjectBundle\\Model\\ProjectIssueStatus', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildProjectIssueStatusQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildProjectIssueStatusQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildProjectIssueStatusQuery) {
            return $criteria;
        }
        $query = new ChildProjectIssueStatusQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildProjectIssueStatus|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }

        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(ProjectIssueStatusTableMap::DATABASE_NAME);
        }

        $this->basePreSelect($con);

        if (
            $this->formatter || $this->modelAlias || $this->with || $this->select
            || $this->selectColumns || $this->asColumns || $this->selectModifiers
            || $this->map || $this->having || $this->joins
        ) {
            return $this->findPkComplex($key, $con);
        }

        if ((null !== ($obj = ProjectIssueStatusTableMap::getInstanceFromPool(null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key)))) {
            // the object is already in the instance pool
            return $obj;
        }

        return $this->findPkSimple($key, $con);
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProjectIssueStatus A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT prisst_id, prisst_name, prisst_order, prisst_created_by_user_id, prisst_updated_by_user_id, prisst_created_at, prisst_updated_at FROM project_issue_status_prisst WHERE prisst_id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildProjectIssueStatus $obj */
            $obj = new ChildProjectIssueStatus();
            $obj->hydrate($row);
            ProjectIssueStatusTableMap::addInstanceToPool($obj, null === $key || is_scalar($key) || is_callable([$key, '__toString']) ? (string) $key : $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildProjectIssueStatus|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildProjectIssueStatusQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildProjectIssueStatusQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the prisst_id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE prisst_id = 1234
     * $query->filterById(array(12, 34)); // WHERE prisst_id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE prisst_id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueStatusQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_ID, $id, $comparison);
    }

    /**
     * Filter the query on the prisst_name column
     *
     * Example usage:
     * <code>
     * $query->filterByName('fooValue');   // WHERE prisst_name = 'fooValue'
     * $query->filterByName('%fooValue%'); // WHERE prisst_name LIKE '%fooValue%'
     * </code>
     *
     * @param     string $name The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueStatusQuery The current query, for fluid interface
     */
    public function filterByName($name = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($name)) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_NAME, $name, $comparison);
    }

    /**
     * Filter the query on the prisst_order column
     *
     * Example usage:
     * <code>
     * $query->filterByOrder(1234); // WHERE prisst_order = 1234
     * $query->filterByOrder(array(12, 34)); // WHERE prisst_order IN (12, 34)
     * $query->filterByOrder(array('min' => 12)); // WHERE prisst_order > 12
     * </code>
     *
     * @param     mixed $order The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueStatusQuery The current query, for fluid interface
     */
    public function filterByOrder($order = null, $comparison = null)
    {
        if (is_array($order)) {
            $useMinMax = false;
            if (isset($order['min'])) {
                $this->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_ORDER, $order['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($order['max'])) {
                $this->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_ORDER, $order['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_ORDER, $order, $comparison);
    }

    /**
     * Filter the query on the prisst_created_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedByUserId(1234); // WHERE prisst_created_by_user_id = 1234
     * $query->filterByCreatedByUserId(array(12, 34)); // WHERE prisst_created_by_user_id IN (12, 34)
     * $query->filterByCreatedByUserId(array('min' => 12)); // WHERE prisst_created_by_user_id > 12
     * </code>
     *
     * @see       filterByCreatedByUser()
     *
     * @param     mixed $createdByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueStatusQuery The current query, for fluid interface
     */
    public function filterByCreatedByUserId($createdByUserId = null, $comparison = null)
    {
        if (is_array($createdByUserId)) {
            $useMinMax = false;
            if (isset($createdByUserId['min'])) {
                $this->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_CREATED_BY_USER_ID, $createdByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdByUserId['max'])) {
                $this->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_CREATED_BY_USER_ID, $createdByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_CREATED_BY_USER_ID, $createdByUserId, $comparison);
    }

    /**
     * Filter the query on the prisst_updated_by_user_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedByUserId(1234); // WHERE prisst_updated_by_user_id = 1234
     * $query->filterByUpdatedByUserId(array(12, 34)); // WHERE prisst_updated_by_user_id IN (12, 34)
     * $query->filterByUpdatedByUserId(array('min' => 12)); // WHERE prisst_updated_by_user_id > 12
     * </code>
     *
     * @see       filterByUpdatedByUser()
     *
     * @param     mixed $updatedByUserId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueStatusQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUserId($updatedByUserId = null, $comparison = null)
    {
        if (is_array($updatedByUserId)) {
            $useMinMax = false;
            if (isset($updatedByUserId['min'])) {
                $this->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_UPDATED_BY_USER_ID, $updatedByUserId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedByUserId['max'])) {
                $this->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_UPDATED_BY_USER_ID, $updatedByUserId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_UPDATED_BY_USER_ID, $updatedByUserId, $comparison);
    }

    /**
     * Filter the query on the prisst_created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE prisst_created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE prisst_created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE prisst_created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueStatusQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the prisst_updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE prisst_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE prisst_updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE prisst_updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildProjectIssueStatusQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\User object
     *
     * @param \IiMedias\AdminBundle\Model\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProjectIssueStatusQuery The current query, for fluid interface
     */
    public function filterByCreatedByUser($user, $comparison = null)
    {
        if ($user instanceof \IiMedias\AdminBundle\Model\User) {
            return $this
                ->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_CREATED_BY_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_CREATED_BY_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCreatedByUser() only accepts arguments of type \IiMedias\AdminBundle\Model\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CreatedByUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProjectIssueStatusQuery The current query, for fluid interface
     */
    public function joinCreatedByUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CreatedByUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CreatedByUser');
        }

        return $this;
    }

    /**
     * Use the CreatedByUser relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useCreatedByUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinCreatedByUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CreatedByUser', '\IiMedias\AdminBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\AdminBundle\Model\User object
     *
     * @param \IiMedias\AdminBundle\Model\User|ObjectCollection $user The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildProjectIssueStatusQuery The current query, for fluid interface
     */
    public function filterByUpdatedByUser($user, $comparison = null)
    {
        if ($user instanceof \IiMedias\AdminBundle\Model\User) {
            return $this
                ->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_UPDATED_BY_USER_ID, $user->getId(), $comparison);
        } elseif ($user instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_UPDATED_BY_USER_ID, $user->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUpdatedByUser() only accepts arguments of type \IiMedias\AdminBundle\Model\User or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the UpdatedByUser relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProjectIssueStatusQuery The current query, for fluid interface
     */
    public function joinUpdatedByUser($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('UpdatedByUser');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'UpdatedByUser');
        }

        return $this;
    }

    /**
     * Use the UpdatedByUser relation User object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\AdminBundle\Model\UserQuery A secondary query class using the current class as primary query
     */
    public function useUpdatedByUserQuery($relationAlias = null, $joinType = Criteria::LEFT_JOIN)
    {
        return $this
            ->joinUpdatedByUser($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'UpdatedByUser', '\IiMedias\AdminBundle\Model\UserQuery');
    }

    /**
     * Filter the query by a related \IiMedias\ProjectBundle\Model\ProjectIssue object
     *
     * @param \IiMedias\ProjectBundle\Model\ProjectIssue|ObjectCollection $projectIssue the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildProjectIssueStatusQuery The current query, for fluid interface
     */
    public function filterByProjectIssue($projectIssue, $comparison = null)
    {
        if ($projectIssue instanceof \IiMedias\ProjectBundle\Model\ProjectIssue) {
            return $this
                ->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_ID, $projectIssue->getStatusId(), $comparison);
        } elseif ($projectIssue instanceof ObjectCollection) {
            return $this
                ->useProjectIssueQuery()
                ->filterByPrimaryKeys($projectIssue->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByProjectIssue() only accepts arguments of type \IiMedias\ProjectBundle\Model\ProjectIssue or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ProjectIssue relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildProjectIssueStatusQuery The current query, for fluid interface
     */
    public function joinProjectIssue($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ProjectIssue');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ProjectIssue');
        }

        return $this;
    }

    /**
     * Use the ProjectIssue relation ProjectIssue object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \IiMedias\ProjectBundle\Model\ProjectIssueQuery A secondary query class using the current class as primary query
     */
    public function useProjectIssueQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProjectIssue($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ProjectIssue', '\IiMedias\ProjectBundle\Model\ProjectIssueQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildProjectIssueStatus $projectIssueStatus Object to remove from the list of results
     *
     * @return $this|ChildProjectIssueStatusQuery The current query, for fluid interface
     */
    public function prune($projectIssueStatus = null)
    {
        if ($projectIssueStatus) {
            $this->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_ID, $projectIssueStatus->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the project_issue_status_prisst table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProjectIssueStatusTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            ProjectIssueStatusTableMap::clearInstancePool();
            ProjectIssueStatusTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(ProjectIssueStatusTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(ProjectIssueStatusTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            ProjectIssueStatusTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            ProjectIssueStatusTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildProjectIssueStatusQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildProjectIssueStatusQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(ProjectIssueStatusTableMap::COL_PRISST_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildProjectIssueStatusQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(ProjectIssueStatusTableMap::COL_PRISST_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildProjectIssueStatusQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(ProjectIssueStatusTableMap::COL_PRISST_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildProjectIssueStatusQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(ProjectIssueStatusTableMap::COL_PRISST_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildProjectIssueStatusQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(ProjectIssueStatusTableMap::COL_PRISST_CREATED_AT);
    }

} // ProjectIssueStatusQuery

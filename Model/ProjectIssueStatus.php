<?php

namespace IiMedias\ProjectBundle\Model;

use IiMedias\ProjectBundle\Model\Base\ProjectIssueStatus as BaseProjectIssueStatus;

/**
 * Skeleton subclass for representing a row from the 'project_issue_status_prisst' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ProjectIssueStatus extends BaseProjectIssueStatus
{
    public function  __toString()
    {
        return $this->getName();
    }
}

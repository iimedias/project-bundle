<?php

namespace IiMedias\ProjectBundle\Model;

use IiMedias\ProjectBundle\Model\Base\ProjectIssueQuery as BaseProjectIssueQuery;
use Propel\Runtime\ActiveQuery\Criteria;
use IiMedias\ProjectBundle\Model\Project;

/**
 * Skeleton subclass for performing query and update operations on the 'project_issue_proiss' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ProjectIssueQuery extends BaseProjectIssueQuery
{
    public static function getAllForProject(Project $project)
    {
        $issues = self::create()
            ->filterByProject($project)
            ->find();
        return $issues;
    }

    public static function prepareIssuesOfProjectId($projectId)
    {
        $issues = self::create()
            ->filterByProjectId($projectId)
            ->orderById(Criteria::DESC)
        ;
        return $issues;
    }

    public static function getOneByProjectAndId(Project $project, $issueId)
    {
        $issue = self::create()
            ->filterByProject($project)
            ->filterById($issueId)
            ->findOne();
        return $issue;
    }
}

<?php

namespace IiMedias\ProjectBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProjectType
 * @package IiMedias\ProjectBundle\Form\Type
 * @author Sébastien "sebii" Bloino <sebii@sebiiheckel.fr>
 * @version 1.0.0
 */
class ProjectType extends AbstractType
{
    /**
     * @since 1.0.0 26/07/2016 Création -- sebii
     * @access public
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                    'attr'       => array(
                        'class'       => 'form-control',
                        'placeholder' => 'Nom',
                    ),
                    'label'      => 'Nom',
                    'label_attr' => array(
                        'class' => 'col-xs-4 col-sm-3 col-md-3 col-lg-3 control-label',
                    ),
                    'required'   => true,
            ))
            ->add('slug', TextType::class, array(
                    'attr'       => array(
                        'class'       => 'form-control',
                        'placeholder' => 'Identifiant',
                    ),
                    'label'      => 'Identifiant',
                    'label_attr' => array(
                        'class' => 'col-xs-4 col-sm-3 col-md-3 col-lg-3 control-label',
                    ),
                    'required'   => true,
            ))
            ->add('description', TextareaType::class, array(
                    'attr'       => array(
                        'class'       => 'form-control',
                        'placeholder' => 'Description',
                    ),
                    'label'      => 'Description',
                    'label_attr' => array(
                        'class' => 'col-xs-4 col-sm-3 col-md-3 col-lg-3 control-label',
                    ),
                    'required'   => false,
            ))
            ->add('webSite', TextType::class, array(
                    'attr'       => array(
                        'class'       => 'form-control',
                        'placeholder' => 'Site web',
                    ),
                    'label'      => 'Site web',
                    'label_attr' => array(
                        'class' => 'col-xs-4 col-sm-3 col-md-3 col-lg-3 control-label',
                    ),
                    'required'   => false,
            ))
            ->add('isPublic', CheckboxType::class, array(
                    'attr'       => array(
                        'class'       => '',
                        'placeholder' => 'Public',
                    ),
                    'label'      => 'Public',
                    'label_attr' => array(
                        'class' => 'col-xs-4 col-sm-3 col-md-3 col-lg-3 control-label',
                    ),
                    'required'   => false,
            ))
            ->add('submit', SubmitType::class, array(
                    'attr'  => array(
                        'class' => 'btn btn-primary',
                        'placeholder' => 'Enregistrer',
                    ),
                    'label' => 'Enregistrer',
            ))
        ;
    }

    /**
     * @since 1.0.0 26/07/2016 Création -- sebii
     * @access public
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
                'data_class' => 'IiMedias\ProjectBundle\Model\Project',
                'name'       => 'project',
        ));
    }
}

<?php

namespace IiMedias\ProjectBundle\Form\Type;

use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataTransformer\IntegerToLocalizedStringTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Propel\Bundle\PropelBundle\Form\Type\ModelType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use IiMedias\AdminBundle\Model\User;
use IiMedias\AdminBundle\Model\UserQuery;
use IiMedias\ProjectBundle\Model\ProjectIssue;
use IiMedias\ProjectBundle\Model\ProjectIssueQuery;
use IiMedias\ProjectBundle\Model\ProjectIssuePriority;
use IiMedias\ProjectBundle\Model\ProjectIssuePriorityQuery;
use IiMedias\ProjectBundle\Model\ProjectIssueStatus;
use IiMedias\ProjectBundle\Model\ProjectIssueStatusQuery;
use IiMedias\ProjectBundle\Model\ProjectIssueType as ProjectIssueTypeModel;
use IiMedias\ProjectBundle\Model\ProjectIssueTypeQuery;

class ProjectIssueType extends AbstractType
{
    /**
     * @since 1.0.0 26/07/2016 Création -- sebii
     * @access public
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (is_null($options['projectId'])) {
            throw new Exception('Le projet n\'est pas défini.');
        }
        $builder
            ->add('projectIssueType', ModelType::class, array(
                    'attr'       => array(
                        'class'       => 'form-control',
                        'placeholder' => 'Type',
                    ),
                    'class'      => ProjectIssueTypeModel::class,
                    'label'      => 'Type',
                    'label_attr' => array(
                        'class' => 'col-xs-4 col-sm-3 col-md-3 col-lg-3 control-label',
                    ),
                    'query'      => ProjectIssueTypeQuery::getCriteriaForProjectIssueTypeForm(),
                    'required'   => true,
            ))
            ->add('subject', TextType::class, array(
                    'attr'       => array(
                        'class'       => 'form-control',
                        'placeholder' => 'Sujet',
                    ),
                    'label'      => 'Sujet',
                    'label_attr' => array(
                        'class' => 'col-xs-4 col-sm-3 col-md-3 col-lg-3 control-label',
                    ),
                    'required'   => true,
            ))
            ->add('description', TextareaType::class, array(
                    'attr'       => array(
                        'class'       => 'form-control',
                        'placeholder' => 'Description',
                    ),
                    'label'      => 'Description',
                    'label_attr' => array(
                        'class' => 'col-xs-4 col-sm-3 col-md-3 col-lg-3 control-label',
                    ),
                    'required'   => true,
            ))
            ->add('projectIssueStatus', ModelType::class, array(
                    'attr'       => array(
                        'class'       => 'form-control',
                        'placeholder' => 'Statut',
                    ),
                    'class'      => ProjectIssueStatus::class,
                    'label'      => 'Statut',
                    'label_attr' => array(
                        'class' => 'col-xs-4 col-sm-3 col-md-3 col-lg-3 control-label',
                    ),
                    'query'      => ProjectIssueStatusQuery::getCriteriaForProjectIssueTypeForm(),
                    'required'   => true,
            ))
            ->add('projectIssuePriority', ModelType::class, array(
                    'attr'       => array(
                        'class'       => 'form-control',
                        'placeholder' => 'Priorité',
                    ),
                    'class'      => ProjectIssuePriority::class,
                    'label'      => 'Priorité',
                    'label_attr' => array(
                        'class' => 'col-xs-4 col-sm-3 col-md-3 col-lg-3 control-label',
                    ),
                    'query'      => ProjectIssuePriorityQuery::getCriteriaForProjectIssueTypeForm(),
                    'required'   => true,
            ))
            ->add('assignedUser', ModelType::class, array(
                    'attr'       => array(
                        'class'       => 'form-control',
                        'placeholder' => 'Assigné à',
                    ),
                    'class'      => User::class,
                    'label'      => 'Assigné à',
                    'label_attr' => array(
                        'class' => 'col-xs-4 col-sm-3 col-md-3 col-lg-3 control-label',
                    ),
                    'query'      => UserQuery::create()
                        ->useUserProjectMemberQuery()
                            ->filterByProjectId($options['projectId'])
                        ->endUse()
                        ->orderByUsername(),
                    'required'   => true,
            ))
            ->add('projectIssueParent', ModelType::class, array(
                    'attr'       => array(
                        'class'       => 'form-control',
                        'placeholder' => 'Tache parente',
                    ),
                    'class'      => ProjectIssue::class,
                    'label'      => 'Tache parente',
                    'label_attr' => array(
                        'class' => 'col-xs-4 col-sm-3 col-md-3 col-lg-3 control-label',
                    ),
                    'query'      => ProjectIssueQuery::prepareIssuesOfProjectId($options['projectId']),
                    'required'   => false,
            ))
            ->add('startAt', DateType::class, array(
                    'attr'       => array(
                        'class'       => 'form-control jQueryDate',
                        'placeholder' => 'Début',
                    ),
                    'label'      => 'Début',
                    'label_attr' => array(
                        'class' => 'col-xs-4 col-sm-3 col-md-3 col-lg-3 control-label',
                    ),
                    'required'   => true,
                    'widget' => 'single_text',
            ))
            ->add('endAt', DateType::class, array(
                    'attr'       => array(
                        'class'       => 'form-control jQueryDate',
                        'placeholder' => 'Echéance',
                    ),
                    'label'      => 'Echéance',
                    'label_attr' => array(
                        'class' => 'col-xs-4 col-sm-3 col-md-3 col-lg-3 control-label',
                    ),
                    'required'   => false,
                    'widget' => 'single_text',
            ))
            ->add('estimatedTime', NumberType::class, array(
                    'attr'       => array(
                        'class'       => 'form-control',
                        'placeholder' => 'Temps estimé',
                    ),
                    'label'      => 'Temps estimé',
                    'label_attr' => array(
                        'class' => 'col-xs-4 col-sm-3 col-md-3 col-lg-3 control-label',
                    ),
                    'required'   => false,
            ))
            ->add('completedPercentage', IntegerType::class, array(
                    'attr'          => array(
                        'class'       => 'form-control',
                        'placeholder' => '% réalisé',
                    ),
                    'label'         => '% réalisé',
                    'label_attr'    => array(
                        'class' => 'col-xs-4 col-sm-3 col-md-3 col-lg-3 control-label',
                    ),
                    'required'      => false,
                    'rounding_mode' => IntegerToLocalizedStringTransformer::ROUND_CEILING,
                    'scale'         => 0,
            ))
            ->add('submit', SubmitType::class, array(
                    'attr'  => array(
                        'class'       => 'btn btn-primary',
                        'placeholder' => 'Enregistrer',
                    ),
                    'label' => 'Enregistrer',
            ))
            ->add('continue', SubmitType::class, array(
                'attr'  => array(
                    'class' => 'btn btn-default',
                    'placeholder' => 'Enregistrer et continuer',
                ),
                'label' => 'Enregistrer et continuer',
            ))
        ;
        if ($options['newCommentActivate'] === true) {
            $builder
                ->add('newComment', TextAreaType::class, array(
                        'attr'       => array(
                            'class'       => 'form-control',
                            'placeholder' => 'Commentaire',
                        ),
                        'label'      => 'Commentaire',
                        'label_attr' => array(
                            'class' => 'col-xs-4 col-sm-3 col-md-3 col-lg-3 control-label',
                        ),
                        'required'   => true,
                ))
            ;
        }
    }

    /**
     * @since 1.0.0 26/07/2016 Création -- sebii
     * @access public
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
                'data_class'         => 'IiMedias\ProjectBundle\Model\ProjectIssue',
                'name'               => 'projectissue',
                'projectId'          => null,
                'newCommentActivate' => false,
        ));
    }
}

<?php

namespace IiMedias\ProjectBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use IiMedias\ProjectBundle\Model\Project;
use IiMedias\ProjectBundle\Model\ProjectQuery;
use IiMedias\ProjectBundle\Model\ProjectMember;
use IiMedias\ProjectBundle\Form\Type\ProjectType;

/**
 * Class ProjectController
 * @package IiMedias\ProjectBundle\Controller
 * @author Sébastien "sebii" Bloino <sebii@sebiiheckel.fr>
 * @version 1.0.0
 */
class ProjectController extends Controller
{
    /**
     * Index / Liste des projets en cours
     *
     * @access public
     * @since 1.0.0 20/07/2016 Création -- sebii
     * @Route("/admin/{_locale}/projects", name="iimedias_project_project_index", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        $projects = ProjectQuery::getAllForUser($this->getUser());
        return $this->render('IiMediasProjectBundle:Project:index.html.twig', array(
                'projects' => $projects,
        ));
    }

    /**
     * Génération du formulaire d'ajout et d'édition d'un projet
     *
     * @access public
     * @since 1.0.0 26/07/2016 Création -- sebii
     * @param Symfony\Component\HttpFoundation\Request $request Objet Requête de Symfony
     * @param string $formMode Mode du formulaire
     * @param integer $projectId Id du projet
     * @Route("/admin/{_locale}/project/add", name="iimedias_project_project_add", requirements={"_locale"="\w{2}"}, defaults={"_locale"="fr", "formMode"="add", "projectSlug"=""})
     * @Route("/admin/{_locale}/project/{projectSlug}/edit", name="iimedias_project_project_edit", requirements={"_locale"="\w{2}", "formMode"="edit", "projectSlug"="\w+"}, defaults={"_locale"="fr", "formMode"="edit"})
     * @Method({"GET", "POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function projectForm(Request $request, $formMode, $projectSlug)
    {
        switch ($formMode) {
            case 'edit':
                $extendsLayout  = 'IiMediasProjectBundle::_layoutProject.html.twig';
                $project        = ProjectQuery::getOneBySlug($projectSlug);
                $formActionPath = $this->generateUrl('iimedias_project_project_edit', array('projectSlug' => $projectSlug));
                break;
            case 'add':
            default:
                $extendsLayout  = 'IiMediasProjectBundle::_layout.html.twig';
                $project        = new Project();
                $formActionPath = $this->generateUrl('iimedias_project_project_add');
        }
        $form = $this->createForm(ProjectType::class, $project, array('action' => $formActionPath));
        if ($request->isMethod('post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                if ($formMode !== 'edit') {
                    $project
                        ->setCreatedByUserId($this->getUser()->getId())
                    ;
                }
                $project
                    ->setUpdatedByUserId($this->getUser()->getId())
                    ->save()
                ;
                $projectMember = new ProjectMember();
                $projectMember
                    ->setProject($project)
                    ->setUser($this->getUser())
                    ->setIsActive(true)
                    ->setCreatedByUser($this->getUser())
                    ->setUpdatedByUser($this->getUser())
                    ->save()
                ;
                return $this->redirect($this->generateUrl('iimedias_project_project_view', array('projectSlug' => $project->getSlug())));
            }
        }

        return $this->render('IiMediasProjectBundle:Project:projectForm.html.twig', array(
                'extendsLayout'      => $extendsLayout,
                'form'               => $form->createView(),
                'formMode'           => $formMode,
                'project'            => $project,
                'projectSlug'        => $projectSlug,
                'moduleNavActiveTab' => 'configuration',
        ));
    }

    /**
     * Dashboard d'un projet
     *
     * @access public
     * @since 1.0.0 26/07/2016 Création -- sebii
     * @param Project $project
     * @Route("/admin/{_locale}/project/{projectSlug}", name="iimedias_project_project_view", requirements={"_locale"="\w{2}", "projectSlug"="\w+"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
     * @ParamConverter("project", class="IiMedias\ProjectBundle\Model\Project", options={"mapping"={"projectSlug": "slug"}})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function view(Project $project)
    {
        return $this->render('IiMediasProjectBundle:Project:view.html.twig', array(
                'project'            => $project,
                'moduleNavActiveTab' => 'view',
        ));
    }
}

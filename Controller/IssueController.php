<?php

namespace IiMedias\ProjectBundle\Controller;

use IiMedias\ProjectBundle\Model\ProjectIssueComment;
use IiMedias\ProjectBundle\Model\ProjectIssueQuery;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use IiMedias\ProjectBundle\Model\Project;
use IiMedias\ProjectBundle\Model\ProjectIssue;
use IiMedias\ProjectBundle\Form\Type\ProjectIssueType;

class IssueController extends Controller
{
    /**
     * Index / Liste des projets en cours
     *
     * @access public
     * @since 1.0.0 20/07/2016 Création -- sebii
     * @Route("/admin/{_locale}/project/{projectSlug}/issues", name="iimedias_project_issue_index", requirements={"_locale"="\w{2}", "projectSlug"="\w+"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
     * @ParamConverter("project", class="IiMedias\ProjectBundle\Model\Project", options={"mapping"={"projectSlug": "slug"}})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function index(Project $project)
    {
        $issues = ProjectIssueQuery::getAllForProject($project);
        return $this->render('IiMediasProjectBundle:Issue:index.html.twig', array(
                'project'            => $project,
                'issues'             => $issues,
                'moduleNavActiveTab' => 'issue',
        ));
    }

    /**
     * Génération du formulaire d'ajout et d'édition d'un projet
     *
     * @access public
     * @since 1.0.0 26/07/2016 Création -- sebii
     * @param Symfony\Component\HttpFoundation\Request $request Objet Requête de Symfony
     * @param string $formMode Mode du formulaire
     * @param integer $projectId Id du projet
     * @Route("/admin/{_locale}/project/{projectSlug}/issue/add", name="iimedias_project_issue_add", requirements={"_locale"="\w{2}", "projectSlug"="\w+"}, defaults={"_locale"="fr", "formMode"="add", "issueId"=0})
     * @Route("/admin/{_locale}/project/{projectSlug}/issue/{issueId}/edit", name="iimedias_project_issue_edit", requirements={"_locale"="\w{2}", "formMode"="edit", "projectSlug"="\w+", "issueId"="\d+"}, defaults={"_locale"="fr", "formMode"="edit"})
     * @ParamConverter("project", class="IiMedias\ProjectBundle\Model\Project", options={"mapping"={"projectSlug": "slug"}})
     * @Method({"GET", "POST"})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function issueForm(Request $request, $formMode, Project $project, $issueId)
    {
        switch ($formMode) {
            case 'edit':
                $issue              = ProjectIssueQuery::getOneByProjectAndId($project, $issueId);
                $formActionPath     = $this->generateUrl('iimedias_project_issue_edit', array('projectSlug' => $project->getSlug(), 'issueId' => $issueId));
                $newCommentActivate = true;
                $moduleNavActiveTab = 'issue';
                break;
            case 'add':
            default:
                $issue              = new ProjectIssue();
                $issue
                    ->setProject($project)
                ;
                $formActionPath     = $this->generateUrl('iimedias_project_issue_add', array('projectSlug' => $project->getSlug()));
                $newCommentActivate = false;
                $moduleNavActiveTab = 'newissue';
        }
        $form = $this->createForm(ProjectIssueType::class, $issue, array('action' => $formActionPath, 'projectId' => $project->getId(), 'newCommentActivate' => $newCommentActivate));
        if ($request->isMethod('post')) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                if ($formMode !== 'edit') {
                    $issue
                        ->setCreatedByUser($this->getUser())
                    ;
                }
                if ($newCommentActivate === true) {
                    $comment = new ProjectIssueComment();
                    $comment
                        ->setProjectIssue($issue)
                        ->setComment(trim($issue->getNewComment()))
                        ->setCreatedByUser($this->getUser())
                        ->setUpdatedByUser($this->getUser())
                        ->save()
                    ;
                }
                $issue
                    ->setUpdatedByUser($this->getUser())
                    ->calculateParentAndChildrenCompletedPercentage()
                    ->save()
                ;
                return $this->redirect($this->generateUrl('iimedias_project_issue_view', array('projectSlug' => $project->getSlug(), 'issueId' => $issue->getId())));
            }
        }

        return $this->render('IiMediasProjectBundle:Issue:issueForm.html.twig', array(
                'form'               => $form->createView(),
                'formMode'           => $formMode,
                'moduleNavActiveTab' => $moduleNavActiveTab,
                'project'            => $project,
                'newCommentActivate' => $newCommentActivate,
                'issue'              => $issue,
        ));
    }

    /**
     * Dashboard d'un projet
     *
     * @access public
     * @since 1.0.0 26/07/2016 Création -- sebii
     * @param Project $project
     * @param ProjectIssue $issue
     * @Route("/admin/{_locale}/project/{projectSlug}/issue/{issueId}", name="iimedias_project_issue_view", requirements={"_locale"="\w{2}", "projectSlug"="\w+", "issueId"="\d+"}, defaults={"_locale"="fr"})
     * @Method({"GET"})
     * @ParamConverter("project", class="IiMedias\ProjectBundle\Model\Project", options={"mapping"={"projectSlug": "slug"}})
     * @ParamConverter("issue", class="IiMedias\ProjectBundle\Model\ProjectIssue", options={"mapping"={"issueId": "id"}})
     * @return Symfony\Component\HttpFoundation\Response
     */
    public function view(Project $project, ProjectIssue $issue)
    {
        return $this->render('IiMediasProjectBundle:Issue:view.html.twig', array(
                'project'            => $project,
                'issue'              => $issue,
                'moduleNavActiveTab' => 'issue',
        ));
    }
}
